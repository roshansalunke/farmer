import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

function FullDetail() {
  const { t } = useTranslation();
  const [eGrade, setEGrade] = React.useState("");
  const [schoolData, setSchoolData] = React.useState([]);
  const {
    parentSchoolData,
    parentUserSchoolData,
    getDiffElementarySchoolData,
    getDiffHighSchoolData,
  } = useSelector((state) => state.parentSchoolDetails);

  React.useEffect(() => {
    if (
      parentSchoolData &&
      parentSchoolData.code === "200" &&
      parentSchoolData.data
    ) {
      if (parentSchoolData.data.grade5.length > 0) {
        setSchoolData(parentSchoolData.data.grade5);
        setEGrade("5");
      } else if (parentSchoolData.data.grade7.length > 0) {
        setSchoolData(parentSchoolData.data.grade7);
        setEGrade("7");
      } else if (parentSchoolData.data.grade9.length > 0) {
        setSchoolData(parentSchoolData.data.grade9);
        setEGrade("9");
      } else {
        setSchoolData([]);
      }
    }
  }, [parentSchoolData]);

  const handleCheckbox = (e) => {
    let { value } = e.target;
    setEGrade(value);
    if (parentSchoolData && parentSchoolData.data) {
      if (value === "5" && parentSchoolData.data.grade5.length > 0) {
        setSchoolData(parentSchoolData.data.grade5);
      } else if (value === "7" && parentSchoolData.data.grade7.length > 0) {
        setSchoolData(parentSchoolData.data.grade7);
      } else if (value === "9" && parentSchoolData.data.grade9.length > 0) {
        setSchoolData(parentSchoolData.data.grade9);
      }
    }
  };

  return (
    <div className="right-side">
      <div className="in-left">
        <div className="title-top">
          <h2 className="sec-heading">{t("LBL_FULL_DETAIL_HEADING")}</h2>
          <h3 className="sec-caption">
            {parentUserSchoolData !== undefined
              ? parentUserSchoolData.vSchoolShortName
              : ""}
          </h3>
          <div className="full-detail-radio">
            {parentSchoolData &&
            parentSchoolData.code === "200" &&
            parentSchoolData.data &&
            parentSchoolData.data.grade5.length > 0 ? (
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="flexRadioDefault"
                  id="flexRadioDefault5"
                  value={"5"}
                  onClick={handleCheckbox}
                  checked={eGrade === "5" ? true : false}
                />
                <label className="form-check-label" for="flexRadioDefault5">
                  5
                </label>
              </div>
            ) : (
              <></>
            )}

            {parentSchoolData &&
            parentSchoolData.code === "200" &&
            parentSchoolData.data &&
            parentSchoolData.data.grade7.length > 0 ? (
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="flexRadioDefault"
                  placeholder="6"
                  id="flexRadioDefault6"
                  onClick={handleCheckbox}
                  value={"7"}
                  checked={eGrade === "7" ? true : false}
                />
                <label className="form-check-label" for="flexRadioDefault6">
                  7
                </label>
              </div>
            ) : (
              <></>
            )}

            {parentSchoolData &&
            parentSchoolData.code === "200" &&
            parentSchoolData.data &&
            parentSchoolData.data.grade9.length > 0 ? (
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="flexRadioDefault"
                  placeholder="9"
                  id="flexRadioDefault7"
                  value={"9"}
                  onClick={handleCheckbox}
                  checked={eGrade === "9" ? true : false}
                />
                <label className="form-check-label" for="flexRadioDefault7">
                  9
                </label>
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
        <div className="table-box table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>{t("LBL_FULL_DETAIL_TABLE_TH_1")}</th>
                <th>{t("LBL_FULL_DETAIL_TABLE_TH_2")}</th>
                <th>{t("LBL_FULL_DETAIL_TABLE_TH_3")}</th>
              </tr>
            </thead>
            <tbody>
              {schoolData && schoolData.length > 0
                ? schoolData
                    .filter((item) => item.eSubject === "C")
                    .map((parentData, index) => (
                      <tr key={index}>
                        <td>{parentData.vCategoryName}</td>
                        <td>{parentData.RESULT || 0} %</td>
                        <td
                          className={
                            parentData.DIFFERENCE > 0
                              ? `green-color`
                              : `pink-color`
                          }
                        >
                          {parentData.DIFFERENCE || 0} %
                        </td>
                      </tr>
                    ))
                : null}
            </tbody>
          </table>
        </div>
        <div className="table-box table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>{t("LBL_FULL_DETAIL_TABLE_TH_4")}</th>
                <th>{t("LBL_FULL_DETAIL_TABLE_TH_2")}</th>
                <th>{t("LBL_FULL_DETAIL_TABLE_TH_3")}</th>
              </tr>
            </thead>
            <tbody>
              {schoolData && schoolData.length > 0
                ? schoolData
                    .filter((item) => item.eSubject === "M")
                    .map((parentData, index) => (
                      <tr key={index}>
                        <td>{parentData.vCategoryName}</td>
                        <td>{parentData.RESULT || 0} %</td>
                        <td
                          className={
                            parentData.DIFFERENCE > 0
                              ? `green-color`
                              : `pink-color`
                          }
                        >
                          {parentData.DIFFERENCE || 0} %
                        </td>
                      </tr>
                    ))
                : null}
            </tbody>
          </table>
        </div>
      </div>

      {getDiffHighSchoolData &&
      getDiffElementarySchoolData &&
      getDiffElementarySchoolData.code === "200" &&
      Object.keys(getDiffElementarySchoolData.data).length > 0 &&
      getDiffHighSchoolData.code === "200" &&
      getDiffHighSchoolData.data &&
      getDiffHighSchoolData.data.length > 0 &&
      getDiffElementarySchoolData.data.school &&
      getDiffElementarySchoolData.data.school.school_results &&
      getDiffElementarySchoolData.data.school.school_results.length > 0 ? (
        <div className="in-right">
          <h2>{t("LBL_FULL_DETAIL_H2_TITLE")}</h2>
          <div className="card-box">
            <h3>{t("LBL_FULL_DETAIL_CARD_BOX_H3_TITLE")}</h3>
            <ul>
              {getDiffHighSchoolData.data.map((czech_diff, index) => (
                <li>
                  {/* <span>{t("LBL_FULL_DETAIL_CARD_BOX_LI_1_SPAN_1")}</span> */}
                  <span>{`HS${index + 1}`}</span>
                  <span
                    className={
                      Math.round(
                        +czech_diff.HighSchoolData.cj_proc -
                          +getDiffElementarySchoolData.data.school
                            .school_results[0].dPercentage
                      ) > 0
                        ? "green-color"
                        : "pink-color"
                    }
                  >
                    {`${
                      Math.round(
                        +czech_diff.HighSchoolData.cj_proc -
                          +getDiffElementarySchoolData.data.school
                            .school_results[0].dPercentage
                      ) || 0
                    }%`}
                    &nbsp;
                    {Math.round(
                      czech_diff.HighSchoolData.cj_proc -
                        getDiffElementarySchoolData.data.school
                          .school_results[0].dPercentage >
                        0
                    ) > 0
                      ? t("LBL_BEST")
                      : t("LBL_WORST")}
                  </span>
                </li>
              ))}
            </ul>
          </div>

          <div className="card-box">
            <h3>{t("LBL_FULL_DETAIL_CARD_BOX_2_H3_TITLE")}</h3>
            <ul>
              {getDiffHighSchoolData.data.map((math_diff, index) => (
                <li>
                  {/* <span>{t("LBL_FULL_DETAIL_CARD_BOX_LI_1_SPAN_1")}</span> */}
                  <span>{`HS${index + 1}`}</span>
                  <span
                    className={
                      Math.round(
                        +math_diff.HighSchoolData.ma_proc -
                          +getDiffElementarySchoolData.data.school
                            .school_results[1].dPercentage
                      ) > 0
                        ? "green-color"
                        : "pink-color"
                    }
                  >
                    {`${
                      Math.round(
                        math_diff.HighSchoolData.ma_proc -
                          getDiffElementarySchoolData.data.school
                            .school_results[1].dPercentage
                      ) || 0
                    }%`}
                    &nbsp;
                    {Math.round(
                      +math_diff.HighSchoolData.ma_proc -
                        +getDiffElementarySchoolData.data.school
                          .school_results[1].dPercentage >
                        0
                    ) > 0
                      ? t("LBL_BEST")
                      : t("LBL_WORST")}
                  </span>
                </li>
              ))}
            </ul>
          </div>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default FullDetail;
