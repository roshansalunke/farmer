import React from "react";
import { useTranslation } from "react-i18next";

function TestNextStep() {
  const { t } = useTranslation();

  return (
    <div className="details-right">
      <div className="image-box m-0">
        <img
          src="../assets/images/sova_new_clr 1.png"
          alt=""
          className="img-contain"
        />
      </div>
      <p>
        {t("LBL_TEST_NEXT_STEP_P_TEXT")}
        {/* text for owl, specific for this page, because here we are starting with
        different point of view */}
      </p>
    </div>
  );
}

export default TestNextStep;
