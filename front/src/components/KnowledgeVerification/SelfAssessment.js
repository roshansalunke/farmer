import React from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { Toast } from "primereact/toast";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { confirmDialog } from "primereact/confirmdialog";
import { GetUserByCookie } from "../../store/action/User";
import { STORE_SCHOOL_DATA } from "../../store/constants/region";

function SelfAssessment() {
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("cz");
  const { schoolData } = useSelector((state) => state.region);
  const { userByCookieData, userByTokenData } = useSelector(
    (state) => state.user
  );
  const toast = React.useRef(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const initialFormValues = {
    vParentEmail: "",
    vTermAndCondition: false,
    eGrade: "",
    eSubject: "",
  };

  const [initialValues, setInitialValues] = React.useState(initialFormValues);

  const testSingUpSchema = Yup.object().shape({
    vParentEmail: Yup.string()
      .required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE"))
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE")),
    vTermAndCondition: Yup.boolean().oneOf(
      [true],
      t("LBL_KNOWLEDGE_VERIFICATION_TERMS_AND_COND_ERR_MESSAGE")
    ),
    eGrade: Yup.string().required(
      t("LBL_KNOWLEDGE_VERIFICATION_GRADE_ERR_MESSAGE")
    ),
    eSubject: Yup.string().required(
      t("LBL_KNOWLEDGE_VERIFICATION_LANG_TYPE_ERR_MESSAGE")
    ),
  });

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };

  console.log(initialValues);

  React.useEffect(() => {
    if (
      sessionStorage.getItem("token") &&
      userByTokenData &&
      userByTokenData.code === "200"
    ) {
      let values = {};
      if (userByTokenData.data.iSchoolId) {
        let data = userByTokenData.data;
        values = {
          iCityId: data.iCityId,
          iDistrictId: data.iDistrictId,
          iRedIZOId: data.iRedIZOId,
          iRegionId: data.iRegionId,
          iSchoolId: data.iSchoolId,
        };
        dispatch({ type: STORE_SCHOOL_DATA, payload: values });
        setInitialValues({
          eGrade: userByTokenData.data.eGrade,
          vParentEmail: userByTokenData.data.user_relation.user
            ? userByTokenData.data.user_relation.user.vEmail
            : "",
          vTermAndCondition: false,
          eSubject: "",
        });
      }
    }
  }, [userByTokenData, dispatch]);

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  React.useEffect(() => {
    if (
      userByCookieData &&
      userByCookieData.code === "200" &&
      userByCookieData.data &&
      Object.keys(userByCookieData.data).length > 0 &&
      !sessionStorage.getItem("token")
    ) {
      setInitialValues({
        vParentEmail: userByCookieData.data.vParentEmail,
        vTermAndCondition: false,
        eSubject: "",
        eGrade: "",
      });
    }
  }, [userByCookieData]);

  // get cookie
  const getCookie = React.useCallback((cookieName) => {
    const cookies = document.cookie.split(";");

    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();

      // Check if the cookie starts with the specified name
      if (cookie.startsWith(cookieName + "=")) {
        return cookie.substring(cookieName.length + 1);
      }
    }

    // Return null if the cookie is not found
    return null;
  }, []);

  // Get user data for cookie
  React.useEffect(() => {
    const vCookieId = getCookie("user-id");
    if (vCookieId && vCookieId.length > 0) {
      dispatch(
        GetUserByCookie({
          vCookieId: vCookieId,
        })
      );
    }
  }, [getCookie, dispatch]);

  const handleSubmit = async (values) => {
    if (schoolData && Object.keys(schoolData).length > 2) {
      let vCookieId = getCookie("user-id");
      if (!vCookieId) {
        vCookieId = Math.random().toString(36).slice(2, 50);
      }
      const reqParameters = {
        ...values,
        ...schoolData,
        vCookieId: vCookieId,
        eSchoolType: "Elementary School",
        vLangCode: lang,
      };
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/userCreate`,
          reqParameters,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (res && res.status === 200) {
          const { data } = res;
          if (data && data.code === "200") {
            let date = new Date();
            date.setTime(date.getTime() + 1 * 24 * 60 * 60 * 1000);
            const myDate = date.toUTCString();
            date.setTime(date.getTime() + 365 * 24 * 60 * 60 * 1000);
            const expId = date.toUTCString();
            document.cookie =
              "user-id" +
              "=" +
              vCookieId +
              ";" +
              "expires=" +
              expId +
              ";path=/;" +
              "domain" +
              process.env.REACT_APP_URL;

            document.cookie =
              "exam" +
              "=" +
              reqParameters.eSubject +
              ";" +
              "expires=" +
              myDate +
              ";path=/;" +
              "domain" +
              process.env.REACT_APP_URL;

            document.cookie =
              "grade" +
              "=" +
              reqParameters.eGrade +
              ";" +
              "expires=" +
              myDate +
              ";path=/;" +
              "domain" +
              process.env.REACT_APP_URL;

            document.cookie =
              "schoolId" +
              "=" +
              schoolData.iSchoolId +
              ";" +
              "expires=" +
              myDate +
              ";path=/;" +
              "domain" +
              process.env.REACT_APP_URL;

            confirmDialog({
              message: "Are you sure you want to proceed?",
              header: "Confirmation",
              icon: "pi pi-exclamation-triangle",
              accept: () => {
                navigate(`/${lang}/test`, {
                  state: {
                    iUserKnowledgeId: data.data.iUserKnowledgeId,
                    eGrade: reqParameters.eGrade,
                    eSubject: reqParameters.eSubject,
                    vCookieId: data.data.vCookieId,
                    iSchoolId: schoolData.iSchoolId,
                  },
                });
              },
              reject: () => {},
            });
          } else if (data && data.code === "400") {
            warnShow(data.message);
          }
        }
      } catch (err) {
        return;
      }
    } else {
      warnShow(t("LBL_KNOWLEDGE_VERIFICATION_WARN_MESSAGE"));
    }
  };

  return (
    <>
      {/* <ConfirmDialog /> */}
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={testSingUpSchema}
        onSubmit={handleSubmit}
      >
        {({ values, errors, touched }) => (
          <>
            <Form>
              <Toast ref={toast} />
              <div className="self-asset-sec">
                <div className="details-left">
                  <h1 className="sec-heading">
                    {t("LBL_KNOWLEDGE_VERIFICATION_HEADING")}
                  </h1>
                  {userByTokenData &&
                  userByTokenData.code === "200" &&
                  userByTokenData.data.iSchoolId ? (
                    <></>
                  ) : (
                    <button
                      className="btn pink-btn"
                      type="button"
                      data-bs-toggle="offcanvas"
                      data-bs-target="#offcanvasRightselectschool"
                      aria-controls="offcanvasRightsingup"
                    >
                      {t("LBL_KNOWLEDGE_VERIFICATION_CHOOSE_SCHOOL_TEXT")}
                      {/* Choose elementary school */}
                    </button>
                  )}

                  {userByTokenData &&
                  userByTokenData.code === "200" &&
                  userByTokenData.data.user_relation &&
                  Object.keys(userByTokenData.data.user_relation).length > 0 &&
                  userByTokenData.data.user_relation.user.vEmail ? (
                    <></>
                  ) : (
                    <div className="form-group email-box">
                      <label className="pink-color">
                        {t("LBL_KNOWLEDGE_VERIFICATION_EMAIL_LABLE")}
                      </label>

                      <Field
                        name="vParentEmail"
                        disabled={
                          values.vParentEmail &&
                          values.vParentEmail.length > 0 &&
                          userByCookieData &&
                          userByCookieData.code === "200" &&
                          userByCookieData.data &&
                          Object.keys(userByCookieData.data).length > 0
                            ? true
                            : false
                        }
                        type="email"
                      />
                      {errors.vParentEmail && touched.vParentEmail ? (
                        <div className="text-danger">{errors.vParentEmail}</div>
                      ) : null}
                    </div>
                  )}

                  <div className="form-check accept-req">
                    <Field
                      type="checkbox"
                      className="form-check-input"
                      name="vTermAndCondition"
                      id="vTermAndCondition"
                    />

                    <label
                      className="form-check-label"
                      htmlFor="vTermAndCondition"
                    >
                      {t("LBL_KNOWLEDGE_VERIFICATION_TERM_ACCEPT_TEXT")}{" "}
                      <span className="pink-color">
                        {t("LBL_KNOWLEDGE_VERIFICATION_TERM_TERMS_TEXT")}
                      </span>{" "}
                      {t("LBL_KNOWLEDGE_VERIFICATION_TERM_AND_TEXT")}&nbsp;
                      <span className="pink-color">
                        {t("LBL_KNOWLEDGE_VERIFICATION_TERM_CONDITION_TEXT")}
                      </span>
                    </label>
                    {errors.vTermAndCondition && touched.vTermAndCondition ? (
                      <div className="text-danger">
                        {errors.vTermAndCondition}
                      </div>
                    ) : null}
                  </div>

                  {userByTokenData &&
                  userByTokenData.code === "200" &&
                  userByTokenData.data.iSchoolId ? (
                    <></>
                  ) : (
                    <>
                      <div className="grade-select-box">
                        <div className="form-check">
                          <Field
                            className="form-check-input"
                            name="eGrade"
                            id="5"
                            type="radio"
                            value="5"
                            disabled={
                              userByCookieData &&
                              userByCookieData.code === "200" &&
                              userByCookieData.data &&
                              +userByCookieData.data.Test_Count % 2 !== 0 &&
                              Object.keys(userByCookieData.data).length > 0
                                ? userByCookieData.data.eGrade &&
                                  userByCookieData.data.eGrade === "5"
                                  ? false
                                  : true
                                : false
                            }
                          />
                          <label className="form-check-label" htmlFor="5">
                            5. {t("LBL_KNOWLEDGE_VERIFICATION_GRADE_LABLE")}
                          </label>
                        </div>
                        <div className="form-check">
                          <Field
                            className="form-check-input"
                            name="eGrade"
                            id="7"
                            type="radio"
                            value="7"
                            disabled={
                              userByCookieData &&
                              userByCookieData.code === "200" &&
                              userByCookieData.data &&
                              +userByCookieData.data.Test_Count % 2 !== 0 &&
                              Object.keys(userByCookieData.data).length > 0
                                ? userByCookieData.data.eGrade &&
                                  userByCookieData.data.eGrade === "7"
                                  ? false
                                  : true
                                : false
                            }
                          />

                          <label className="form-check-label" htmlFor="7">
                            7. {t("LBL_KNOWLEDGE_VERIFICATION_GRADE_LABLE")}
                          </label>
                        </div>
                        <div className="form-check">
                          <Field
                            className="form-check-input"
                            name="eGrade"
                            id="9"
                            type="radio"
                            value="9"
                            disabled={
                              userByCookieData &&
                              userByCookieData.code === "200" &&
                              userByCookieData.data &&
                              +userByCookieData.data.Test_Count % 2 !== 0 &&
                              Object.keys(userByCookieData.data).length > 0
                                ? userByCookieData.data.eGrade &&
                                  userByCookieData.data.eGrade === "9"
                                  ? false
                                  : true
                                : false
                            }
                          />

                          <label className="form-check-label" htmlFor="9">
                            9. {t("LBL_KNOWLEDGE_VERIFICATION_GRADE_LABLE")}
                          </label>
                        </div>
                      </div>
                      {errors.eGrade && touched.eGrade ? (
                        <div className="text-danger">{errors.eGrade}</div>
                      ) : null}
                    </>
                  )}

                  <div className="opt-select-box">
                    <div className="form-check">
                      <Field
                        className="form-check-input"
                        name="eSubject"
                        id="C"
                        type="radio"
                        value="C"
                        disabled={
                          userByCookieData &&
                          userByCookieData.code === "200" &&
                          userByCookieData.data &&
                          +userByCookieData.data.Test_Count % 2 !== 0 &&
                          Object.keys(userByCookieData.data).length > 0
                            ? userByCookieData.data.eSubject &&
                              userByCookieData.data.eSubject === "M"
                              ? false
                              : true
                            : false
                        }
                      />
                      <label className="form-check-label" htmlFor="C">
                        <span>
                          {t("LBL_KNOWLEDGE_VERIFICATION_CZECH_TEXT")}
                        </span>
                        <div className="image-box">
                          <img
                            src="/../assets/images/fitness-kniha-ikona-300x300 2.png"
                            alt=""
                            className="img-contain"
                          />
                        </div>
                      </label>
                    </div>
                    <div className="form-check">
                      <Field
                        className="form-check-input"
                        name="eSubject"
                        id="M"
                        type="radio"
                        value="M"
                        disabled={
                          userByCookieData &&
                          userByCookieData.code === "200" &&
                          userByCookieData.data &&
                          +userByCookieData.data.Test_Count % 2 !== 0 &&
                          Object.keys(userByCookieData.data).length > 0
                            ? userByCookieData.data.eSubject &&
                              userByCookieData.data.eSubject === "C"
                              ? false
                              : true
                            : false
                        }
                      />
                      <label className="form-check-label" htmlFor="M">
                        <span>{t("LBL_KNOWLEDGE_VERIFICATION_MATH_TEXT")}</span>
                        <div className="image-box">
                          <img
                            src="/../assets/images/pngegg (kopie) 2.png"
                            alt=""
                            className="img-contain"
                          />
                        </div>
                      </label>
                    </div>
                  </div>
                  {errors.eSubject && touched.eSubject ? (
                    <div className="text-danger">{errors.eSubject}</div>
                  ) : null}
                  <p className="caption">
                    {t("LBL_KNOWLEDGE_VERIFICATION_CAPTION")}
                  </p>
                </div>
                <div className="details-right">
                  <div className="image-box">
                    <img
                      src="/../assets/images/sova_new_clr 1.png"
                      alt=""
                      className="img-contain"
                    />
                  </div>
                  <p>{t("LBL_KNOWLEDGE_VERIFICATION_DESCRIPTION_TEXT")}</p>
                  <button type="submit" className="btn yellow-btn">
                    {t("LBL_KNOWLEDGE_VERIFICATION_SUBMIT_BUTTON")}
                  </button>
                </div>
              </div>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}

export default SelfAssessment;
