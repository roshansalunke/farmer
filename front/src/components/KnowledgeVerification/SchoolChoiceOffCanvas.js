import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

import {
  RegionList,
  DistrictByIdList,
  CityByIdList,
  StoreSchoolData,
  getSchoolList,
} from "../../store/action/Region";
import { useLocation } from "react-router-dom";

function SchoolChoiceOffCanvas() {
  const { regionList, districtByIdList, citListData, schoolListByCityIdData } =
    useSelector((state) => state.region);
  const { t } = useTranslation();
  const location = useLocation();

  const { userByCookieData } = useSelector((state) => state.user);
  const initialFormValues = {
    iRegionId: "",
    iDistrictId: "",
    iCityId: "",
    iSchoolId: "",
  };
  const [initialValues, setInitialValues] = React.useState(initialFormValues);

  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: Yup.object({
      iRegionId: Yup.string().required(t("LBL_REGION_ERR_MESSGE")),
      iDistrictId: Yup.string().required(t("LBL_DISTRICT_ERR_MESSGE")),
      iCityId: Yup.string().required(t("LBL_CITY_ERR_MESSGE")),
      iSchoolId: Yup.string().required(t("LBL_SCHOOL_ERR_MESSGE")),
    }),
    enableReinitialize: true,
    onSubmit: (values, { resetForm }) => {
      if (
        schoolListByCityIdData &&
        schoolListByCityIdData.data &&
        schoolListByCityIdData.data.length > 0
      ) {
        let RedIZOId = schoolListByCityIdData.data.find(
          (item) => +item.iSchoolId === +values.iSchoolId
        )?.iRedIZOId;
        if (RedIZOId) {
          values = {
            ...values,
            iRedIZOId: RedIZOId,
          };
        }
      }
      
      if (
        citListData !== undefined &&
        citListData.code === "200" &&
        citListData.data.length > 0
      ) {
        let cityId = citListData.data.find(
          (data) => data.vCityTitle === values.iCityId
        )?.iCityId;
        if (cityId) {
          values = {
            ...values,
            eAccountType: "Elementary School",

            iCityId: cityId,
          };
        }
      }
      dispatch(StoreSchoolData(values));
      document.getElementById("closeoffcanvas").click();
      if (location.pathname.includes("parent-school-detail") || location.pathname.includes("parents-student-detail") || location.pathname.includes("settings")) {
        document.getElementById("right_child_offcanvass_id").click();
      }

      resetForm();
    },
  });

  React.useEffect(() => {
    if (!regionList) {
      dispatch(RegionList());
    }
  }, [regionList, dispatch]);

  React.useEffect(() => {
    if (
      userByCookieData &&
      userByCookieData.code === "200" &&
      userByCookieData.data &&
      Object.keys(userByCookieData.data).length > 0
    ) {
      dispatch(
        DistrictByIdList({ iRegionId: userByCookieData.data.iRegionId })
      );
      dispatch(
        CityByIdList({ iDistrictId: userByCookieData.data.iDistrictId })
      );
      dispatch(
        getSchoolList({
          vCity: userByCookieData.data.city.vCityTitle,
          eSchoolType: "Elementary School",
        })
      );
      setInitialValues({
        iRegionId: userByCookieData.data.iRegionId,
        iDistrictId: userByCookieData.data.iDistrictId,
        iCityId: userByCookieData.data.city.vCityTitle,
        iSchoolId: userByCookieData.data.iSchoolId,
      });
      dispatch(
        StoreSchoolData({
          iRegionId: userByCookieData.data.iRegionId,
          iDistrictId: userByCookieData.data.iDistrictId,
          iCityId: userByCookieData.data.iCityId,
          iSchoolId: userByCookieData.data.iSchoolId,
        })
      );
    }
  }, [userByCookieData, dispatch]);

  const handleRegionChange = (e) => {
    const regionId = e.target.value;
    if (regionId && regionId !== "") {
      dispatch(DistrictByIdList({ iRegionId: regionId }));
    }
    formik.handleChange(e);
  };

  const handleDistrictChange = (e) => {
    const districtId = e.target.value;
    if (districtId && districtId !== "") {
      dispatch(CityByIdList({ iDistrictId: districtId }));
    }
    formik.handleChange(e);
  };
  const handleCityChange = (e) => {
    const cityId = e.target.value;
    if (cityId && cityId !== "") {
      dispatch(
        getSchoolList({ vCity: cityId, eSchoolType: "Elementary School" })
      );
    }
    formik.handleChange(e);
  };

  // const handleSchoolChange = (e) => {
  //   // const cityId = e.target.value;
  //   // if (cityId && cityId !== "") {
  //   //   dispatch(getSchoolList({ iCityId: cityId }));
  //   // }
  //   formik.handleChange(e);
  // };

  const handleFormReset = () => {
    formik.resetForm();
  };

  return (
    <div
      className="offcanvas offcanvas-end login-canvas choose-school-canvas"
      // data-bs-backdrop="static"
      tabIndex="-1"
      id="offcanvasRightselectschool"
      aria-labelledby="offcanvasRightselectschoolLabel"
    >
      <form onSubmit={formik.handleSubmit}>
        <div className="offcanvas-header">
          <div id="offcanvasRightselectschoolLabel" className="canvas-title">
            {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_CHOICE_TEXT")}
          </div>
          <button
            type="button"
            className="btn-close text-reset"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
            id="closeoffcanvas"
            onClick={handleFormReset}
          ></button>
        </div>
        <div className="offcanvas-body">
          <select
            className="form-select"
            id="iRegionId"
            name="iRegionId"
            value={formik.values.iRegionId}
            onChange={handleRegionChange}
            disabled={
              userByCookieData &&
              userByCookieData.code === "200" &&
              userByCookieData.data &&
              Object.keys(userByCookieData.data).length > 0
                ? userByCookieData.data.iRegionId &&
                  userByCookieData.data.iRegionId > 0
                  ? true
                  : false
                : false
            }
          >
            <option value="">
              {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_1")}
            </option>
            {regionList !== undefined &&
            regionList.code === "200" &&
            regionList.data.length > 0 ? (
              regionList.data.map((reg, index) => (
                <option key={index} value={reg.iRegionId}>
                  {reg.vRegionTitle}
                </option>
              ))
            ) : (
              <></>
            )}
          </select>
          {formik.errors.iRegionId && formik.touched.iRegionId ? (
            <div className="text-danger">{formik.errors.iRegionId}</div>
          ) : null}

          <select
            className="form-select"
            id="iDistrictId"
            name="iDistrictId"
            value={formik.values.iDistrictId}
            onChange={handleDistrictChange}
            disabled={
              userByCookieData &&
              userByCookieData.code === "200" &&
              userByCookieData.data &&
              Object.keys(userByCookieData.data).length > 0
                ? userByCookieData.data.iDistrictId &&
                  userByCookieData.data.iDistrictId > 0
                  ? true
                  : false
                : false
            }
          >
            <option value="">
              {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_2")}{" "}
            </option>
            {districtByIdList !== undefined &&
            districtByIdList.code === "200" &&
            districtByIdList.data.length > 0 ? (
              districtByIdList.data.map((reg, index) => (
                <option key={index} value={reg.iDistrictId}>
                  {reg.vDistrictTitle}
                </option>
              ))
            ) : (
              <></>
            )}
          </select>
          {formik.errors.iDistrictId && formik.touched.iDistrictId ? (
            <div className="text-danger">{formik.errors.iDistrictId}</div>
          ) : null}

          <select
            className="form-select"
            id="iCityId"
            name="iCityId"
            value={formik.values.iCityId}
            onChange={handleCityChange}
            disabled={
              userByCookieData &&
              userByCookieData.code === "200" &&
              userByCookieData.data &&
              Object.keys(userByCookieData.data).length > 0
                ? userByCookieData.data.iCityId &&
                  userByCookieData.data.iCityId > 0
                  ? true
                  : false
                : false
            }
          >
            <option value={""}>
              {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_3")}
            </option>
            {citListData !== undefined &&
            citListData.code === "200" &&
            citListData.data.length > 0 ? (
              citListData.data.map((reg, index) => (
                <option key={index} value={reg.vCityTitle}>
                  {reg.vCityTitle}
                </option>
              ))
            ) : (
              <></>
            )}
          </select>
          {formik.errors.iCityId && formik.touched.iCityId ? (
            <div className="text-danger">{formik.errors.iCityId}</div>
          ) : null}

          {/* <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="flexRadioselelctschool"
              id="flexRadioselelctschool1"
            />
            <label
              className="form-check-label"
              htmlFor="flexRadioselelctschool1"
            >
              ZŠ Komenského
            </label>
          </div> */}
          <select
            className="form-select"
            id="iSchoolId"
            name="iSchoolId"
            value={formik.values.iSchoolId}
            onChange={(e) => formik.handleChange(e)}
            disabled={
              userByCookieData &&
              userByCookieData.code === "200" &&
              userByCookieData.data &&
              Object.keys(userByCookieData.data).length > 0
                ? userByCookieData.data.iSchoolId &&
                  userByCookieData.data.iSchoolId > 0
                  ? true
                  : false
                : false
            }
          >
            <option value="">
              {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_4")}{" "}
            </option>
            {schoolListByCityIdData !== undefined &&
            schoolListByCityIdData.code === "200" &&
            schoolListByCityIdData.data.length > 0 ? (
              schoolListByCityIdData.data.map((school, index) => (
                <option key={index} value={school.iSchoolId}>
                  {school.vSchoolFullName}
                </option>
              ))
            ) : (
              <></>
            )}
          </select>
          {formik.errors.iSchoolId && formik.touched.iSchoolId ? (
            <div className="text-danger">{formik.errors.iSchoolId}</div>
          ) : null}
          <button type="submit" className="btn green-btn">
            {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_SUBMIT_TEXT")}
          </button>
        </div>
      </form>
    </div>
  );
}

export default SchoolChoiceOffCanvas;
