import { Swiper, SwiperSlide } from "swiper/react";
import React from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Navigation, Thumbs } from "swiper/modules";
import Latex from "react-latex";

// Import Swiper styles
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import { GetAllSubjectLessionQuestions } from "../../store/action/Test";
import { Toast } from "primereact/toast";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";

export default function StudentSubjectTest() {
  const navigate = useNavigate();
  const [swiper, setSwiper] = React.useState(null);
  const { t } = useTranslation();
  const { subjectLessionQuestionData } = useSelector((state) => state.test);
  const [test, setTest] = React.useState({});
  const [thumbsSwiper, setThumbsSwiper] = React.useState(null);
  // const [minutes, setMinutes] = React.useState(45);
  // const [seconds, setSeconds] = React.useState(0);
  const [questionList, setQuestionList] = React.useState([]);
  const [questionId, setQuestionId] = React.useState([]);
  const [wrongAnsList, setWrongAnsList] = React.useState([]);
  const [selectedUserAns, setSelectedUserAns] = React.useState("");
  const [summaryVal, setSummaryVal] = React.useState("");
  const { state } = useLocation();
  const toast = React.useRef(null);
  const [lessionId, setLessionId] = React.useState([]);
  const [lessionPointData, setLessionPointData] = React.useState([]);
  const dispatch = useDispatch();
  const [lang, setLang] = React.useState("cz");
  const { userByTokenData } = useSelector((state) => state.user);

  React.useEffect(() => {
    if (!sessionStorage.getItem("token")) {
      navigate(`/`);
      return;
    }
  }, [navigate]);

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  const nexto = async (qId, data, len) => {
    if (selectedUserAns === "") {
      swiper.slideNext();
      let div = document.getElementById("count-number" + qId);
      div.style.backgroundColor = "red";
    }
    if (
      (selectedUserAns !== "" || summaryVal !== "") &&
      !questionId.includes(qId) &&
      selectedUserAns === data.vSummaryAnswers
    ) {
      // setCorrectAns((pre) => ++pre);
      setQuestionId((pre) => [...pre, qId]);
    }
    if (
      (qId && questionList.includes(qId) && state) ||
      selectedUserAns !== "" ||
      summaryVal !== ""
    ) {
      if (
        data.vSummaryAnswers !== selectedUserAns &&
        !lessionId.includes(qId)
      ) {
        setLessionId((pre) => [...pre, qId]);
        if (selectedUserAns !== "" || summaryVal !== "") {
          setWrongAnsList((pre) => [...pre, data.iTaskId]);
        }
        try {
          let res = await axios.post(
            `${process.env.REACT_APP_API_URL}/api/math-lesson-solution/get-all-math-lesson-solution-data`,
            { iTaskId: data.iTaskId, eSubject: state.eSubject },
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          if (res.data.code === "200") {
            if (
              subjectLessionQuestionData &&
              subjectLessionQuestionData.code === "200" &&
              subjectLessionQuestionData.data &&
              subjectLessionQuestionData.data.length > 0
            ) {
              setTest(subjectLessionQuestionData);
            }
            setLessionPointData((pre) => [
              ...pre,
              { iTaskId: qId, data: res.data.data },
            ]);
          }
        } catch (err) {
          console.log(err);
        }
      } else {
        if (selectedUserAns !== "" || summaryVal !== "") {
          swiper.slideNext();
        }
      }

      if (!lessionId.includes(qId)) {
        let lessonData = {};
        if (userByTokenData && userByTokenData.code === "200") {
          lessonData = {
            iUserId: userByTokenData.data.iUserId,
            iLessonId: data.iLessonId,
            vUserAnswers: selectedUserAns,
            vSummaryAnswers: data.vSummaryAnswers,
          };
          if (state && state.eSubject === "M") {
            lessonData = {
              ...lessonData,
              iMathLessonTestId: data.iMathLessonTestId,
            };
          } else {
            lessonData = {
              ...lessonData,
              iCZLessonTestId: data.iCZLessonTestId,
            };
          }
        }
        try {
          await axios.post(
            `${process.env.REACT_APP_API_URL}/api/user-lesson/createUserLessonTestData`,
            lessonData,
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
        } catch (err) {
          console.log(err);
        }
      }
      let div = document.getElementById("count-number" + qId);
      if (selectedUserAns !== "" || summaryVal !== "") {
        if (data.vSummaryAnswers !== selectedUserAns) {
          div.style.backgroundColor = "red";
        } else {
          div.style.backgroundColor = "#BACF1B";
        }
      }
    } else {
      let div = document.getElementById("count-number" + qId);
      div.style.backgroundColor = "red";
    }
    setSummaryVal("");
    setSelectedUserAns("");
    if (test && test.code === "200" && len === test.data.length - 1) {
      handleFinalSubmit();
    }
  };

  const handleFinalSubmit = React.useCallback(() => {
    // if (minutes === 0) {
    //   navigate(
    //     `/${lang}/student-infinite-trees/${sessionStorage.getItem("token")}`,
    //     {
    //       state: state,
    //     }
    //   );
    // }
    navigate(
      `/${lang}/student-infinite-trees/${sessionStorage.getItem("token")}`,
      {
        state: state,
      }
    );
  }, [state, lang, navigate]);

  React.useEffect(() => {
    if (state && sessionStorage.getItem("token")) {
      dispatch(
        GetAllSubjectLessionQuestions({
          iLessonId: state.iLessonId,
          eSubject: state.eSubject,
        })
      );
    }
  }, [dispatch, state]);

  React.useEffect(() => {
    if (
      subjectLessionQuestionData &&
      subjectLessionQuestionData.code === "200" &&
      subjectLessionQuestionData.data &&
      subjectLessionQuestionData.data.length > 0
    ) {
      setTest(subjectLessionQuestionData);
    } else {
      setTest({});
    }
  }, [subjectLessionQuestionData]);

  // React.useEffect(() => {
  //   const interval = setInterval(() => {
  //     if (seconds > 0) {
  //       setSeconds((pre) => pre - 1);
  //     }

  //     if (seconds === 0) {
  //       if (minutes === 0) {
  //         handleFinalSubmit();
  //         clearInterval(interval);
  //       } else {
  //         setSeconds(59);
  //         setMinutes((pre) => pre - 1);
  //       }
  //     }
  //   }, 1000);

  //   return () => {
  //     clearInterval(interval);
  //   };
  // }, [seconds, handleFinalSubmit, minutes]);

  const handleRadionChange = (e, qId) => {
    if (e.target.name === "vUserAnswer") {
      setSummaryVal(e.target.value);
      if (!questionList.includes(qId)) {
        setQuestionList((pre) => [...pre, qId]);
      }
      if (e.target.value === "") {
        setQuestionList((pre) => [...pre.filter((val) => +val !== +qId)]);
      }
      return;
    }
    if (!questionList.includes(qId)) {
      setQuestionList((pre) => [...pre, qId]);
    }
    setSelectedUserAns(e.target.value);
  };

  const handleSkipQuestion = (index, data, taskId) => {
    setSummaryVal("");
    setSelectedUserAns("");

    // set red color for skip question
    for (let i = 0; i <= index; i++) {
      if (data && +data[i].iTaskId === +taskId) {
        let div = document.getElementById("count-number" + taskId);
        var computedStyle = window.getComputedStyle(div);
        var bgColor = computedStyle.backgroundColor;
        if (bgColor !== "rgb(186, 207, 27)") {
          div.style.backgroundColor = "#f8bd01";
        }
      } else if (!questionList.includes(data[i].iTaskId)) {
        let div = document.getElementById("count-number" + data[i].iTaskId);
        let computedStyle = window.getComputedStyle(div);
        let bgColor = computedStyle.backgroundColor;
        if (bgColor !== "rgb(186, 207, 27)") {
          div.style.backgroundColor = "red";
        }
      }
    }

    // stop override color on attemp question
    for (let i = 0; i < data.length; i++) {
      if (+data[i].iTaskId === +taskId) continue;
      let myElement = document.getElementById("count-number" + data[i].iTaskId);
      if (myElement) {
        let computedStyle = window.getComputedStyle(myElement);
        let backgroundColor = computedStyle.backgroundColor;
        if (backgroundColor === "rgb(186, 207, 27)") {
          continue;
        } else if (backgroundColor === "rgb(248, 189, 1)") {
          myElement.style.backgroundColor = "red";
        }
      }
    }
  };

  return (
    <>
      <Toast ref={toast} />
      <div className="white-card plr-50 pt-50 pb-50 test-lang-sec">
        <div className="slider-box">
          {/* <div className="time">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="22"
              height="22"
              viewBox="0 0 22 22"
              fill="none"
            >
              <g clipPath="url(#clip0_381_1918)">
                <path
                  d="M11 0C4.93475 0 0 4.93449 0 11C0 17.0655 4.93475 22 11 22C17.0655 22 22 17.0655 22 11C22 4.93449 17.0653 0 11 0ZM11 20.3942C5.82012 20.3942 1.60583 16.1799 1.60583 11C1.60583 5.82012 5.82012 1.60583 11 1.60583C16.1799 1.60583 20.3942 5.82007 20.3942 10.9997C20.3942 16.1799 16.1799 20.3942 11 20.3942Z"
                  fill="#6A7A99"
                />
                <path
                  d="M14.7471 11H11.2678V6.18243C11.2678 5.73895 10.9083 5.37952 10.4648 5.37952C10.0214 5.37952 9.66193 5.73895 9.66193 6.18243V11.8029C9.66193 12.2464 10.0214 12.6058 10.4648 12.6058H14.7471C15.1905 12.6058 15.55 12.2464 15.55 11.8029C15.55 11.3594 15.1905 11 14.7471 11Z"
                  fill="#6A7A99"
                />
              </g>
              <defs>
                <clipPath id="clip0_381_1918">
                  <rect width="22" height="22" fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span>
              {minutes} : {seconds < 10 ? `0${seconds}` : `${seconds}`}
            </span>
          </div> */}
          <div className="top-line-box">
            <div>
              <h1>
                {state && state.eSubject === "M"
                  ? t("LBL_TEST_QUESTION_HEADING_MATH")
                  : t("LBL_TEST_QUESTION_HEADING_CZECH")}{" "}
                {state && `(${state.vLessonTitle})`}
              </h1>
            </div>
            <div className="count-slider">
              <Swiper
                onSwiper={setThumbsSwiper}
                spaceBetween={10}
                slidesPerView={5}
                // simulateTouch={false}
                navigation={true}
                className="mySwiper-user"
                freeMode={false}
                watchSlidesProgress={true}
                modules={[Navigation, Thumbs]}
                breakpoints={{
                  0: {
                    slidesPerView: 3,
                  },
                  420: {
                    slidesPerView: 4,
                  },
                  767: {
                    slidesPerView: 4,
                  },
                  1199: {
                    slidesPerView: 6,
                  },
                }}
              >
                {test &&
                  test.code === "200" &&
                  test.data &&
                  test.data.length > 0 &&
                  test.data.map((val, index) => (
                    <SwiperSlide key={index}>
                      <div
                        onClick={() =>
                          handleSkipQuestion(index, test.data, val.iTaskId)
                        }
                        style={{
                          backgroundColor: index === 0 ? "#F8BD01" : "",
                        }}
                        className={"count-number"}
                        id={"count-number" + val.iTaskId}
                      >
                        <p>{index + 1}</p>
                      </div>
                    </SwiperSlide>
                  ))}
              </Swiper>
            </div>
            <div className="right-side">
              <Link
                className=""
                to={`/${lang}/student-infinite-trees/${sessionStorage.getItem(
                  "token"
                )}`}
                state={{
                  eSubject: state
                    ? state.eSubject
                    : sessionStorage.getItem("eSubject"),
                }}
              >
                {t("LBL_BACK_TO_ROAD_MAP")}
              </Link>
            </div>
          </div>

          <Swiper
            spaceBetween={10}
            navigation={false}
            allowTouchMove={false}
            onSwiper={(s) => {
              setSwiper(s);
            }}
            thumbs={{
              swiper:
                thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null,
            }}
            modules={[Navigation, Thumbs]}
            className="mySwiper-quote"
          >
            {test &&
              test.code === "200" &&
              test.data &&
              test.data.length > 0 &&
              test.data.map((test, index) => (
                <SwiperSlide key={index}>
                  <div
                    className={
                      test.vChoiceA && test.vChoiceA.length > 0
                        ? "question-slider radio-center"
                        : test.vYes === null
                        ? "question-slider text-answer-box"
                        : "question-slider"
                    }
                  >
                    <div className="question-area">
                      <div className="intro-box">
                        {test.vDefault1Column1 ||
                        test.vDefault1Column2 ||
                        test.vDefault1Column3 ? (
                          <h3>{t("LBL_TEST_CAPTION_1")}</h3>
                        ) : !test.vChoiceA && test.vYes === null ? (
                          <h3 className="font-24-semibold">
                            {t("LBL_TEST_CAPTION_2")}
                          </h3>
                        ) : (
                          ""
                        )}
                        {test.vDefault1Txt1 ? (
                          <>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: test.vDefault1Txt1.replaceAll(
                                  "<br>",
                                  ""
                                ),
                              }}
                            />
                          </>
                        ) : (
                          ""
                        )}
                        {test.vychozi1_text1 ? (
                          <>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: test.vychozi1_text1.replaceAll(
                                  "<br>",
                                  ""
                                ),
                              }}
                            />
                          </>
                        ) : (
                          ""
                        )}
                        {test.vExplanationImage &&
                        test.vExplanationImage.split("/").pop() !== "null" ? (
                          <div className="image-box-q">
                            <img
                              src={test.vExplanationImage}
                              className="image-contain"
                              alt="test"
                            />
                          </div>
                        ) : (
                          ""
                        )}
                        {test.vDefaultTextImage &&
                        test.vDefaultTextImage.split("/").pop() !== "null" ? (
                          <div className="image-box-q">
                            <img
                              src={test.vDefaultTextImage}
                              className="image-contain"
                              alt="test"
                            />
                          </div>
                        ) : (
                          ""
                        )}

                        {test.vDefault1Txt2 ? (
                          <>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: test.vDefault1Txt2.replaceAll(
                                  "<br>",
                                  ""
                                ),
                              }}
                            />
                          </>
                        ) : (
                          ""
                        )}
                        <ul>
                          {test.vDefault1Column1 && (
                            <li style={{ listStyleType: "none" }}>
                              {/* <div>{testvDefault1Column1}</div> */}
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: test.vDefault1Column1.replaceAll(
                                    "<br>",
                                    ""
                                  ),
                                }}
                              />

                              {test.vDefault2Txt1 ? (
                                <>
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: test.vDefault2Txt1.replaceAll(
                                        "<br>",
                                        ""
                                      ),
                                    }}
                                  />
                                </>
                              ) : (
                                ""
                              )}
                            </li>
                          )}
                          {test.vDefault1Column2 && (
                            <li style={{ listStyleType: "none" }}>
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: test.vDefault1Column2.replaceAll(
                                    "<br>",
                                    ""
                                  ),
                                }}
                              />
                              {test.vDefault2Txt2 ? (
                                <>
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: test.vDefault2Txt2.replaceAll(
                                        "<br>",
                                        ""
                                      ),
                                    }}
                                  />
                                </>
                              ) : (
                                ""
                              )}
                            </li>
                          )}
                          {test.vDefault1Column3 && (
                            <li style={{ listStyleType: "none" }}>
                              {test.vDefault1Column3 || ""}
                              <div>
                                {test.vDefault2Txt3
                                  ? `3.1 ${test.vDefault2Txt3}`
                                  : ""}
                              </div>
                            </li>
                          )}
                        </ul>
                      </div>
                      {!test.vChoiceA && test.vYes === null ? (
                        <></>
                      ) : (
                        <div className="question-box">
                          <h3>{t("LBL_TEST_QUESTION_TITLE")}</h3>
                          {test.vTaskAbove ? (
                            <p
                              dangerouslySetInnerHTML={{
                                __html: test.vTaskAbove.replaceAll("<br>", " "),
                              }}
                            />
                          ) : (
                            <></>
                          )}
                          <>
                            <Latex>{test.vTask}</Latex>
                          </>
                          {/* <p
                            dangerouslySetInnerHTML={{
                              __html:
                                test.iMathLessonTestId +
                                " " +
                                test.vTask.replaceAll("<br>", ""),
                            }}
                          /> */}
                          {test.vTaskUnder ? (
                            <p
                              dangerouslySetInnerHTML={{
                                __html: test.vTaskUnder.replaceAll("<br>", ""),
                              }}
                            />
                          ) : (
                            <></>
                          )}

                          {/* <p>{testvTaskUnder}</p> */}
                        </div>
                      )}
                      {!test.vChoiceA && test.vYes === null ? (
                        <>
                          <p className="font-24-semibold">{test.vTask}</p>
                          <div className="form-floating">
                            <textarea
                              className="form-control"
                              placeholder={t(
                                "LBL_TEST_PLACEHOLDER_QUESTION_TEXT"
                              )}
                              name="vUserAnswer"
                              id="floatingTextarea2"
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                            ></textarea>
                            <label htmlFor="floatingTextarea2">
                              {t("LBL_TEST_PLACEHOLDER_QUESTION_TEXT")}
                            </label>
                            {/* <p className="text-danger">
                              {errorMsg ? "Answer is Required." : ""}
                            </p> */}
                          </div>
                        </>
                      ) : (
                        <></>
                      )}
                    </div>
                    {test.vChoiceA && test.vChoiceA.length > 0 ? (
                      <div className="radio-box" key={index}>
                        {test.vChoiceA ? (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              // name="multi-maths"
                              name={`multi-maths${index}`}
                              id={test.vChoiceA}
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "A"
                              }
                              value={"A"}
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={test.vChoiceA}
                            >
                              <Latex>{test.vChoiceA}</Latex>
                              {/* {test.vChoiceA.replaceAll("<br>", "")} */}
                            </label>
                          </div>
                        ) : (
                          ""
                        )}
                        {test.vChoiceB ? (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              // name="multi-maths"
                              id={test.vChoiceB}
                              name={`multi-maths${index}`}
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "B"
                              }
                              value={"B"}
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={test.vChoiceB}
                            >
                              <Latex>{test.vChoiceB}</Latex>
                              {/* {test.vChoiceB.replaceAll("<br>", "")} */}
                            </label>
                          </div>
                        ) : (
                          ""
                        )}
                        {test.vChoiceC ? (
                          <div className="form-check" key={index}>
                            <input
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "C"
                              }
                              value={"C"}
                              className="form-check-input"
                              type="radio"
                              name={`multi-maths${index}`}
                              id={test.vChoiceC}
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={test.vChoiceC}
                            >
                              <Latex>{test.vChoiceC}</Latex>

                              {/* {test.vChoiceC.replaceAll("<br>", "")} */}
                            </label>
                          </div>
                        ) : (
                          ""
                        )}
                        {test.vChoiceD ? (
                          <div className="form-check">
                            <input
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "D"
                              }
                              className="form-check-input"
                              type="radio"
                              name={`multi-maths${index}`}
                              id={test.vChoiceD}
                              value={"D"}
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={test.vChoiceD}
                            >
                              <Latex>{test.vChoiceD}</Latex>
                              {/* {test.vChoiceD.replaceAll("<br>", "")} */}
                            </label>
                          </div>
                        ) : (
                          ""
                        )}

                        {test.vChoiceE ? (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              name={`multi-maths${index}`}
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "E"
                              }
                              id={test.vChoiceE}
                              value={"E"}
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={test.vChoiceE}
                            >
                              <Latex>{test.vChoiceE}</Latex>

                              {/* {test.vChoiceE.replaceAll("<br>", "")} */}
                            </label>
                          </div>
                        ) : (
                          ""
                        )}
                        {test.vChoiceF ? (
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              name={`multi-maths${index}`}
                              id={test.vChoiceF}
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "F"
                              }
                              value="F"
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={test.vChoiceF}
                            >
                              <Latex>{test.vChoiceF}</Latex>
                              {/* {test.vChoiceF.replaceAll("<br>", "")} */}
                            </label>
                          </div>
                        ) : (
                          ""
                        )}
                        {/* <p className="text-danger">
                          {errorMsg ? "Answer is Required." : ""}
                        </p> */}
                      </div>
                    ) : (
                      <></>
                    )}
                    <div className="answer-box">
                      {test.vNo && test.vYes ? (
                        <div className="radio-box">
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              name={`flexRadioDefault${index}`}
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "Ano"
                              }
                              type="radio"
                              id={`flexRadioDefault1${index}`}
                              value={"Ano"}
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={`flexRadioDefault1${index}`}
                            >
                              {t("LBL_YES")}
                            </label>
                          </div>
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              name={`flexRadioDefault${index}`}
                              id={`flexRadioDefault2${index}`}
                              defaultChecked={
                                test.isAnswered && test.vUserAnswers === "Ne"
                              }
                              // key={index}
                              value={"Ne"}
                              onChange={(e) =>
                                handleRadionChange(e, test.iTaskId)
                              }
                              disabled={
                                wrongAnsList.includes(parseInt(test.iTaskId))
                                  ? true
                                  : false
                              }
                            />
                            <label
                              className="form-check-label"
                              htmlFor={`flexRadioDefault2${index}`}
                            >
                              {t("LBL_NO")}
                            </label>
                          </div>
                          {/* <p className="text-danger">
                            {errorMsg ? "Answer is Required." : ""}
                          </p> */}
                        </div>
                      ) : (
                        <></>
                      )}
                      {index === subjectLessionQuestionData.data.length - 1 ? (
                        <button
                          // key={index}
                          className="green-btn btn"
                          type="button"
                          // onClick={handleFinalSubmit}
                          onClick={() => {
                            handleSkipQuestion(
                              index,
                              subjectLessionQuestionData.data,
                              test.iTaskId
                            );
                            nexto(test.iTaskId, test, index);
                          }}
                        >
                          {t("LBL_TEST_SUBMIT_QUESTION_TEXT")}
                          {/* Submit Exam */}
                        </button>
                      ) : (
                        <button
                          className="yellow-btn btn"
                          onClick={() => {
                            handleSkipQuestion(
                              index,
                              subjectLessionQuestionData.data,
                              test.iTaskId
                            );
                            nexto(test.iTaskId, test, index);
                          }}
                        >
                          {t("LBL_TEST_NEXT_QUESTION_TEXT")}
                          {/* Next question */}
                        </button>
                      )}
                    </div>
                    {lessionPointData &&
                    lessionPointData.length > 0 &&
                    lessionId &&
                    lessionId.includes(test.iTaskId) ? (
                      <ul className="true-answer">
                        {lessionPointData.length > 0 &&
                          lessionPointData.map((val, idx) => (
                            <>
                              {val.iTaskId === test.iTaskId && (
                                <>
                                  <h5>{`Answer ${test.vSummaryAnswers}`}</h5>

                                  {lessionPointData
                                    .at(idx)
                                    ?.data.map((point, key) => (
                                      <li key={key}>
                                        {"-> "}
                                        <Latex>{point.vExplanation}</Latex>
                                      </li>
                                    ))}
                                </>
                              )}
                            </>
                          ))}
                      </ul>
                    ) : (
                      <></>
                    )}
                  </div>
                </SwiperSlide>
              ))}
          </Swiper>
        </div>
      </div>
    </>
  );
}
