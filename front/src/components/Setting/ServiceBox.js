import React from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { Toast } from "primereact/toast";
import {
  GetParentChildList,
  GetRemainCreditCount,
  GetUserShcoolList,
  SetEduPrgs,
  SetSchools,
} from "../../store/action/shoppingCartActions";
import { GetParentSchoolDetails } from "../../store/action/ParentSchoolDetails";
import {
  STORE_PARENT_CHILD_DATA,
  STORE_SCHOOL_TYPE_DATA,
  STORE_USER_SCHOOL_DATA,
} from "../../store/constants/parentSchoolDetails";

function ServiceBox() {
  const [lang, setLang] = React.useState("cz");
  const { t } = useTranslation();
  const navigate = useNavigate();
  const toast = React.useRef(null);
  const [activeAccountCount, setActiveAccountCount] = React.useState(0);
  const { userByTokenData } = useSelector((state) => state.user);
  const { userSchoolData, getCreditRemainCount, getParentChildData } =
    useSelector((state) => state.shoppingCart);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (
      !getParentChildData &&
      userByTokenData &&
      userByTokenData.code === "200"
    )
      dispatch(GetParentChildList({ iParentId: userByTokenData.data.iUserId }));
  }, [dispatch, userByTokenData, getParentChildData]);

  React.useEffect(() => {
    if (
      getParentChildData &&
      getParentChildData.code === "200" &&
      getParentChildData.data &&
      getParentChildData.data.length > 0
    ) {
      let c = 0;
      for (let c of getParentChildData.data) {
        if (c.eStudentStatus === "Accepted") {
          c = c + 1;
        }
      }
      setActiveAccountCount(c);
    }
  }, [getParentChildData]);

  React.useEffect(() => {
    if (!userSchoolData && userByTokenData && userByTokenData.code === "200")
      dispatch(GetUserShcoolList({ iUserId: userByTokenData.data.iUserId }));
  }, [userSchoolData, dispatch, userByTokenData]);

  React.useEffect(() => {
    if (
      !getCreditRemainCount &&
      userByTokenData &&
      userByTokenData.code === "200"
    ) {
      dispatch(GetRemainCreditCount({ iUserId: userByTokenData.data.iUserId }));
    }
  }, [userByTokenData, getCreditRemainCount, dispatch]);

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  const handleSchool = (school, index) => {
    if (school) {
      dispatch(GetParentSchoolDetails({ iRedIZOId: school.iRedIZOId }));
      let data = {
        iRedIZOId: school.iRedIZOId,
        vSchoolShortName: school.school.vSchoolFullName,
        id: index,
      };
      dispatch({ type: STORE_USER_SCHOOL_DATA, payload: data });
    }
    dispatch({
      type: STORE_PARENT_CHILD_DATA,
      payload: { childId: -1 },
    });
    navigate(
      `/${lang}/parent-school-detail/${sessionStorage.getItem("token")}`
    );
  };

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };

  const handleChooseSchool = () => {
    warnShow(t("LBL_CREDIT_COINS_WARN_MESSAGE"));
  };

  const handleSchoolType = (schoolName) => {
    dispatch({ type: STORE_SCHOOL_TYPE_DATA, payload: schoolName });
  };

  const handleTotalSchoolChange = React.useCallback(() => {
    dispatch(SetSchools(true));
  }, [dispatch]);

  const handleEducationProgramChange = React.useCallback(() => {
    dispatch(SetEduPrgs(true));
  }, [dispatch]);

  return (
    <div className="s-service-box">
      <Toast ref={toast} />
      <h1 className="sec-heading">{t("LBL_SETTING_PAGE_H1_TITLE")}</h1>
      {/* <h1 className="sec-heading">Settings of services</h1> */}
      <div className="boxes">
        <div className="topbar">
          <p>
            {t("LBL_SETTING_PAGE_TOPBAR_P1_1")}
            {`: ${activeAccountCount}`}
          </p>
          <p>
            {t("LBL_SETTING_PAGE_TOPBAR_P1_2")} :&nbsp;
            {getCreditRemainCount &&
            getCreditRemainCount.code === "200" &&
            getCreditRemainCount.data &&
            getCreditRemainCount.data.iRemainEducationProgram
              ? getCreditRemainCount.data.iRemainEducationProgram
              : 0}
          </p>
          {/* <p>Settings of active accouts (3 accounts)</p> */}
          {/* <p>remaining: 0</p> */}
          <div
            className="plus-box"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasconnectaccount"
            aria-controls="offcanvasRightsingup"
            id="right_child_offcanvass_id"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
            >
              <ellipse
                cx="20"
                cy="19.5833"
                rx="20"
                ry="19.5833"
                fill="#E61C63"
              />
              <path d="M4 19.5833H19.3636H36" stroke="white" strokeWidth="2" />
              <path d="M20 35.25V3.91667" stroke="white" strokeWidth="2" />
            </svg>
            <span>{t("LBL_SETTING_PAGE_TOPBAR_SPAN_1")}</span>
            {/* <span>Connect account</span> */}
          </div>
          <div className="plus-box" onClick={handleEducationProgramChange}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
            >
              <ellipse
                cx="20"
                cy="19.5833"
                rx="20"
                ry="19.5833"
                fill="#E61C63"
              />
              <path d="M4 19.5833H19.3636H36" stroke="white" strokeWidth="2" />
              <path d="M20 35.25V3.91667" stroke="white" strokeWidth="2" />
            </svg>
            <span>{t("LBL_SETTING_PAGE_TOPBAR_SPAN_2")}</span>
            {/* <span>Add to cart</span> */}
          </div>
        </div>
        <div className="topbar-caption">
          {/* {t("LBL_SETTING_PAGE_TOPBAR_CAPTION")} */}
        </div>
        {/* <div className="topbar-caption">Tady bude doprovodný text</div> */}
        <div className="table-box table-responsive">
          <table className="table">
            <tbody>
              {getParentChildData &&
                getParentChildData.code === "200" &&
                getParentChildData.data &&
                getParentChildData.data.length > 0 &&
                getParentChildData.data.map((child, index) => (
                  <tr key={index}>
                    <td>{`${child.vFirstName} ${child.vLastName}`}</td>
                    <td className="green-color">
                      <div className="dots"></div>
                      <span>
                        {t("LBL_SETTING_PAGE_TABLE_BOX_1_TD_2_COMMON")}
                      </span>
                    </td>
                    <td
                      onClick={() =>
                        navigate(
                          `/${lang}/parents-student-detail/${sessionStorage.getItem(
                            "token"
                          )}`
                        )
                      }
                    >
                      {t("LBL_SETTING_PAGE_TABLE_BOX_1_TD_3_COMMON")}
                    </td>
                    {/* <td>{t("LBL_SETTING_PAGE_TABLE_BOX_1_TD_4_COMMON")}</td> */}
                    {/* <td>Cody Fisher</td>
                      <td className="green-color">
                        <div className="dots"></div>
                        <span>education program</span>
                      </td>
                      <td>Details</td>
                      <td>Set up</td> */}
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>

      <div className="boxes custom-box">
        <div className="box-top d-flex justify-content-between mb-4">
          <p className="blue-color">
            {t("LBL_SETTING_PAGE_BOXES_DETAILS_OF_SCHOOL")}
          </p>
          <p className="text-center pink-color">
            {t("LBL_SETTING_PAGE_TOPBAR_P1_2")}:&nbsp;
            {getCreditRemainCount &&
            getCreditRemainCount.code === "200" &&
            getCreditRemainCount.data &&
            Object.keys(getCreditRemainCount.data).length > 0
              ? getCreditRemainCount.data.iRemainDetailOfSchool
              : 0}
          </p>
        </div>
        <div className="test mb-3">
          <div className="topbar">
            <p>
              {t("LBL_SETTING_PAGE_BOXES_TOPBAR_P_1")}&nbsp;
              {userSchoolData &&
                userSchoolData.code === "200" &&
                userSchoolData.data &&
                userSchoolData.data.filter(
                  (sch) => sch.eSchoolType !== "High School"
                )?.length}
              &nbsp;of&nbsp;
              {getCreditRemainCount &&
                getCreditRemainCount.code === "200" &&
                getCreditRemainCount.data &&
                Object.keys(getCreditRemainCount.data).length > 0 &&
                getCreditRemainCount.data.iRemainDetailOfSchool}
            </p>
            {/* <p>Details of schools choosen 2 of 2</p> */}

            {(getCreditRemainCount &&
              getCreditRemainCount.code === "200" &&
              getCreditRemainCount.data &&
              +getCreditRemainCount.data.iRemainDetailOfSchool === 0) ||
            getCreditRemainCount === undefined ? (
              <>
                <div
                  className="plus-box"
                  type="button"
                  onClick={handleChooseSchool}
                  // data-bs-toggle="offcanvas"
                  // data-bs-target="#offcanvasRightselectschoolsetting"
                  // aria-controls="offcanvasRightsingup"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="40"
                    height="40"
                    viewBox="0 0 40 40"
                    fill="none"
                  >
                    <ellipse
                      cx="20"
                      cy="19.5833"
                      rx="20"
                      ry="19.5833"
                      fill="#E61C63"
                    />
                    <path
                      d="M4 19.5833H19.3636H36"
                      stroke="white"
                      strokeWidth="2"
                    />
                    <path
                      d="M20 35.25V3.91667"
                      stroke="white"
                      strokeWidth="2"
                    />
                  </svg>
                  <span>{t("LBL_SETTING_PAGE_BOXES_TOPBAR_SPAN_1")}</span>
                </div>
              </>
            ) : (
              <div
                className="plus-box"
                type="button"
                data-bs-toggle="offcanvas"
                data-bs-target="#offcanvasRightselectschoolsetting"
                aria-controls="offcanvasRightsingup"
                onClick={() => handleSchoolType("Elementary School")}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="40"
                  height="40"
                  viewBox="0 0 40 40"
                  fill="none"
                >
                  <ellipse
                    cx="20"
                    cy="19.5833"
                    rx="20"
                    ry="19.5833"
                    fill="#E61C63"
                  />
                  <path
                    d="M4 19.5833H19.3636H36"
                    stroke="white"
                    strokeWidth="2"
                  />
                  <path d="M20 35.25V3.91667" stroke="white" strokeWidth="2" />
                </svg>

                <span>{t("LBL_SETTING_PAGE_BOXES_TOPBAR_SPAN_1")}</span>
                {/* <span>Choose a school</span> */}
              </div>
            )}

            <div className="plus-box" onClick={handleTotalSchoolChange}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="40"
                viewBox="0 0 40 40"
                fill="none"
              >
                <ellipse
                  cx="20"
                  cy="19.5833"
                  rx="20"
                  ry="19.5833"
                  fill="#E61C63"
                />
                <path
                  d="M4 19.5833H19.3636H36"
                  stroke="white"
                  strokeWidth="2"
                />
                <path d="M20 35.25V3.91667" stroke="white" strokeWidth="2" />
              </svg>
              <span>{t("LBL_SETTING_PAGE_BOXES_TOPBAR_SPAN_2")}</span>
              {/* <span>Add to cart</span> */}
            </div>
          </div>
          <div className="table-box table-responsive">
            <table className="table">
              <tbody>
                {userSchoolData &&
                  userSchoolData.code === "200" &&
                  userSchoolData.data &&
                  userSchoolData.data.length > 0 &&
                  userSchoolData.data
                    .filter((high) => high.eSchoolType !== "High School")
                    ?.map((school, index) => (
                      <tr key={index}>
                        <td>{school.school.vSchoolFullName}</td>
                        <td className="green-color">
                          <div className="dots"></div>
                          <span>{t("LBL_SETTING_PAGE_TABLE_BOX_2_TD_2")}</span>
                        </td>
                        <td onClick={() => handleSchool(school, index)}>
                          {t("LBL_SETTING_PAGE_TABLE_BOX_1_TD_3_COMMON")}
                        </td>
                      </tr>
                    ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ServiceBox;
