import { useTranslation } from "react-i18next";
import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import ChildAccountForm from "./ChildAccountForm";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Toast } from "primereact/toast";
import { Dialog } from "primereact/dialog";
import { GetParentChildList } from "../../store/action/shoppingCartActions";

function AccountConnectSetting() {
  const { t } = useTranslation();
  const [ref, setRef] = React.useState(true);
  const { userByTokenData } = useSelector((state) => state.user);
  const toast = React.useRef(null);
  const [visible, setVisible] = React.useState(false);
  const [successMsg, setSuccessMsg] = React.useState("");
  const dispatch = useDispatch();

  const initialFormValues = {
    vEmail: "",
  };

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };

  const checkEmailExistChildAndParent = async (values) => {
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/user/checkEmailChildAndParent`,
        { ...values, vLangCode: sessionStorage.getItem("langType") || "CZ" },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (res.data.code === "404") {
        warnShow(res.data.message);
        return false;
      } else if (res.data.code === "200") {
        if (userByTokenData && userByTokenData.code === "200") {
          values = {
            ...values,
            iParentId: userByTokenData.data.iUserId,
            vLangCode: sessionStorage.getItem("langType") || "CZ",
          };
        }
        const childRes = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/connectChild`,
          values,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (childRes && childRes.data.code === "201") {
          // successShow(childRes.data.message);
          if (userByTokenData && userByTokenData.code === "200") {
            dispatch(
              GetParentChildList({ iParentId: userByTokenData.data.iUserId })
            );
          }
          setSuccessMsg(childRes.data.message);
          setVisible(true);
          return true;
        } else if (childRes && childRes.data.code === "409") {
          warnShow(childRes.data.message);
          return false;
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  const formik = useFormik({
    initialValues: initialFormValues,
    validationSchema: Yup.object({
      vEmail: Yup.string()
        .required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE"))
        .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE")),
    }),
    enableReinitialize: true,
    onSubmit: async (values, { resetForm }) => {
      let valid = await checkEmailExistChildAndParent({
        vEmail: values.vEmail,
        eAccountType: "Student",
      });
      if (valid) {
        resetForm();
      }
    },
  });

  const handleFormReset = () => {
    formik.resetForm();
    setRef((pre) => !pre);
  };

  return (
    <>
      <Toast ref={toast} />
      <div
        className="offcanvas offcanvas-start login-canvas choose-school-canvas ac-connect-canvas"
        data-bs-backdrop="static"
        tabIndex="-1"
        id="offcanvasconnectaccount"
        aria-labelledby="offcanvasconnectaccountLabel"
        key={ref}
      >
        <div className="offcanvas-header">
          <div id="offcanvasconnectaccountLabel" className="canvas-title">
            {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_CANVAS_TITLE")}
            {/* Account connection */}
          </div>
          <button
            type="button"
            className="btn-close text-reset"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
            // id="closeoffcanvas"
            id="close_right_offcanvas"
            onClick={() => handleFormReset()}
          ></button>
        </div>
        <div className="offcanvas-body">
          <form onSubmit={formik.handleSubmit}>
            <div className="form-group">
              <input
                name="vEmail"
                id="vEmail"
                className="form-control"
                placeholder={t("LBL_KNOWLEDGE_VERIFICATION_EMAIL_LABLE")}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.vEmail}
                type="text"
              />
              {formik.errors.vEmail && formik.touched.vEmail ? (
                <div className="text-danger">{formik.errors.vEmail}</div>
              ) : null}
            </div>
            <button type="submit" className="yellow-btn btn">
              {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_BUTTON_1")}
            </button>
          </form>

          <div className="or-text text-center">
            {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_DIV_1")}
          </div>
          <div className="canvas-title mb-3">
            {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_DIV_2")}
          </div>
          {/* <button className="yellow-btn btn">Send request</button> */}
          {/* <div className="or-text text-center">or</div>
          <div className="canvas-title mb-3">Setting up a child's account</div> */}
          <ChildAccountForm />
          {/* <button className="btn blue-btn mb-4">Choose a primary school</button>
          <button className="btn yellow-btn">Save and send child data</button> */}
        </div>
      </div>
      {/* add modal */}
      <Dialog
        visible={visible}
        draggable={false}
        resizable={false}
        modal
        // footer={footerContent}
        style={{ width: "30rem" }}
        onHide={() => {
          setVisible(false);
        }}
      >
        <h3 className="sec-heading" style={{ fontSize: "24px" }}>
          {successMsg}
        </h3>
        <div className="col-12 d-flex justify-content-center">
          <button
            onClick={() => {
              setVisible(false);
              document.getElementById("closeoffcanvas").click();
            }}
            type="submit"
            className="text-center btn yellow-btn"
          >
            {t("LBL_SETTING_CONFIRM_BUTTON_TEXT")}
          </button>
        </div>
      </Dialog>
    </>
  );
}

export default AccountConnectSetting;
