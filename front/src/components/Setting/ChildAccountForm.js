import React from "react";
import { useTranslation } from "react-i18next";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { Toast } from "primereact/toast";
import { useDispatch, useSelector } from "react-redux";
import { GetParentChildList as childList } from "../../store/action/shoppingCartActions";
import { GetParentChildList } from "../../store/action/ParentSchoolDetails";
import { Dialog } from "primereact/dialog";
import { STORE_SCHOOL_DATA } from "../../store/constants/region";

function ChildAccountForm() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [visible, setVisible] = React.useState(false);
  const toast = React.useRef(null);
  const [emailExitsErrMsg, setEmailExitsErrMsg] = React.useState("");
  const [successMsg, setSuccessMsg] = React.useState("");
  const { userByTokenData } = useSelector((state) => state.user);
  const { schoolData } = useSelector((state) => state.region);

  const initialFormValues = {
    vEmail: "",
    vFirstName: "",
    vLastName: "",
    vMobileNumber: "",
    eGrade: "",
    eCzechGrade: "",
    eMathGrade: "",
  };

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };
  const formik = useFormik({
    initialValues: initialFormValues,
    validationSchema: Yup.object({
      vEmail: Yup.string()
        .required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE"))
        .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE")),
      vFirstName: Yup.string().required(t("LBL_SINGUP_NAME_ERROR_MESSAGE")),
      vLastName: Yup.string().required(t("LBL_SINGUP_SURNAME_ERROR_MESSAGE")),
      eGrade: Yup.string().required(t("Grade is required.")),
      eCzechGrade: Yup.string().required(t("CzechGrade is required.")),
      eMathGrade: Yup.string().required(t("MathGrade is required.")),
      vMobileNumber: Yup.number().required(
        t("LBL_SIGNUP_PHONE_NO_ERROR_MESSAGE")
      ),
    }),
    enableReinitialize: true,
    onSubmit: async (values, { resetForm }) => {
      if (userByTokenData && userByTokenData.code === "200") {
        values = {
          ...values,
          eAccountType: "Student",
          vLangCode: sessionStorage.getItem("langType") || "CZ",
          iParentId: userByTokenData.data.iUserId,
          ...schoolData,
        };
      }
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/registerForStudent`,
          values,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        const { data } = res;
        if (data && data.code === "201") {
          setSuccessMsg(data.message);
          dispatch({ type: STORE_SCHOOL_DATA, payload: undefined });
          setVisible(true);
          if (userByTokenData && userByTokenData.code === "200") {
            dispatch(
              GetParentChildList({ iUserId: userByTokenData.data.iUserId })
            );
            dispatch(childList({ iParentId: userByTokenData.data.iUserId }));
          }
          resetForm();
          setEmailExitsErrMsg("");
          // setTimeout(() => {
          //   document.getElementById("closeoffcanvas").click();
          //   resetForm();
          //   setEmailExitsErrMsg("");
          // }, 2000);
        } else if (data && data.code === "200") {
          setEmailExitsErrMsg(data.message);
          warnShow(data.message);
          return;
        }
      } catch (err) {
        setEmailExitsErrMsg(err.response.data.message);
        return;
      }
    },
  });

  return (
    <>
      <Toast ref={toast} />
      <form onSubmit={formik.handleSubmit}>
        <div className="form-group">
          <input
            name="vEmail"
            id="vEmail"
            className="form-control"
            placeholder={t("LBL_KNOWLEDGE_VERIFICATION_EMAIL_LABLE")}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.vEmail}
            type="text"
          />
          {(formik.errors.vEmail && formik.touched.vEmail) ||
          emailExitsErrMsg !== "" ? (
            <div className="text-danger">
              {emailExitsErrMsg !== ""
                ? emailExitsErrMsg
                : formik.errors.vEmail}
            </div>
          ) : null}
        </div>
        <div className="form-group">
          <input
            className="form-control"
            placeholder={t("LBL_SINGUP_OFFCANVAS_NAME_LABEL")}
            type="text"
            name="vFirstName"
            id="vFirstName"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.vFirstName}
          />
          {formik.errors.vFirstName && formik.touched.vFirstName ? (
            <div className="text-danger">{formik.errors.vFirstName}</div>
          ) : null}
        </div>
        <div className="form-group">
          <input
            className="form-control"
            placeholder={t("LBL_SINGUP_OFFCANVAS_SURNAME_LABEL")}
            type="text"
            name="vLastName"
            id="vLastName"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.vLastName}
          />
          {formik.errors.vLastName && formik.touched.vLastName ? (
            <div className="text-danger">{formik.errors.vLastName}</div>
          ) : null}
        </div>
        <div className="form-group">
          <input
            className="form-control"
            placeholder={t("LBL_SINGUP_OFFCANVAS_PHONE_NO_LABEL")}
            type="number"
            name="vMobileNumber"
            id="vMobileNumber"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.vMobileNumber}
          />
          {formik.errors.vMobileNumber && formik.touched.vMobileNumber ? (
            <div className="text-danger">{formik.errors.vMobileNumber}</div>
          ) : null}
        </div>
        <select
          name="eGrade"
          id="eGrade"
          className="form-select"
          aria-label="Default select example"
          value={formik.values.eGrade}
          onChange={(e) => formik.handleChange(e)}
        >
          <option value="">
            {t("LBL_KNOWLEDGE_VERIFICATION_GRADE_LABLE")} - 5./ 7./ 9.{" "}
          </option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
        </select>
        {formik.errors.eGrade && formik.touched.eGrade ? (
          <div className="text-danger">{formik.errors.eGrade}</div>
        ) : null}
        <div className="canvas-title mb-3">
          {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_DIV_3")}
        </div>
        <div className="select-box">
          <label>
            {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_SELECT_BOX_LABEL_1")}
          </label>
          <select
            onChange={(e) => formik.handleChange(e)}
            name="eCzechGrade"
            id="eCzechGrade"
            value={formik.values.eCzechGrade}
            className="form-select"
            aria-label="Default select example"
          >
            <option defaultChecked>
              {t("LBL_KNOWLEDGE_VERIFICATION_GRADE_LABLE")} 1 - 5
            </option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
          {formik.errors.eCzechGrade && formik.touched.eCzechGrade ? (
            <div className="text-danger">{formik.errors.eCzechGrade}</div>
          ) : null}
        </div>
        <div className="select-box">
          <label>
            {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_SELECT_BOX_LABEL_2")}
          </label>
          <select
            name="eMathGrade"
            id="eMathGrade"
            value={formik.values.eMathGrade}
            className="form-select"
            aria-label="Default select example"
            onChange={(e) => formik.handleChange(e)}
          >
            <option defaultChecked>
              {t("LBL_KNOWLEDGE_VERIFICATION_GRADE_LABLE")} 1 - 5
            </option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
          {formik.errors.eMathGrade && formik.touched.eMathGrade ? (
            <div className="text-danger">{formik.errors.eMathGrade}</div>
          ) : null}
        </div>
        <button
          type="button"
          // onClick={() =>
          //   document.getElementById("close_right_offcanvas").click()
          // }
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasRightselectschool"
          aria-controls="offcanvasRightsingup"
          className="btn blue-btn mb-4"
        >
          {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_BUTTON_2_COMMON")}
        </button>
        <button
          disabled={
            schoolData === undefined || Object.keys(schoolData).length === 0
              ? true
              : false
          }
          type="submit"
          className="btn yellow-btn"
        >
          {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_BUTTON_3")}
        </button>
      </form>

      {/* add modal */}
      <Dialog
        visible={visible}
        draggable={false}
        resizable={false}
        modal
        // footer={footerContent}
        style={{ width: "30rem" }}
        onHide={() => {
          setVisible(false);
        }}
      >
        <h3 className="sec-heading" style={{ fontSize: "24px" }}>
          {successMsg}
        </h3>
        <div className="col-12 d-flex justify-content-center">
          <button
            onClick={() => {
              setVisible(false);
              document.getElementById("close_right_offcanvas").click();
            }}
            type="submit"
            className="text-center btn yellow-btn"
          >
            {t("LBL_SETTING_CONFIRM_BUTTON_TEXT")}
          </button>
        </div>
      </Dialog>
    </>
  );
}

export default ChildAccountForm;
