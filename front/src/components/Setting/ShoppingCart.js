import React from "react";
import { useTranslation } from "react-i18next";
import { Toast } from "primereact/toast";
import { loadStripe } from "@stripe/stripe-js";
// import Stripe from "../../config/stripe/Stripe";
import { useDispatch, useSelector } from "react-redux";
import {
  GetRemainCreditCount,
  SetEduPrgs,
  SetPayableAmount,
  SetSchools,
  // SetPayableAmount,
} from "../../store/action/shoppingCartActions";
import axios from "axios";
const stripePromise = loadStripe(
  "pk_live_51OWJilHo8SaRmVONlCjE43njou2tHEgGnMViD0jU4wbLaLq5AwPRqcSUX0lK5dXKXeRjdEXVFVwTm3GvTJwhQQQs00V1D3ya6Y"
);

function ShoppingCart() {
  const toast = React.useRef(null);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  // const { amount, choiceOfSchools, educationPrograms } = useParams();
  const [totalSchools, setTotalSchools] = React.useState(0);
  const [paymentSuccess, setPaymentSuccess] = React.useState({});
  const [educationProgram, setEducationProgram] = React.useState(0);
  const [visibleStripeDialog, setVisibleStripeDialog] = React.useState(false);
  const { totalAmount, totalEduPrgsData, totalSchoolsData } = useSelector(
    (state) => state.shoppingCart
  );
  const { userByTokenData } = useSelector((state) => state.user);
  const [lang, setLang] = React.useState("cz");

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  React.useEffect(() => {
    if (totalSchoolsData) {
      dispatch(SetSchools(false));
      if (totalSchools === 0) {
        setTotalSchools(1);
      } else {
        setTotalSchools(+totalSchools + 1);
      }
    }
  }, [dispatch, totalSchools, totalSchoolsData]);

  React.useEffect(() => {
    if (totalEduPrgsData) {
      dispatch(SetEduPrgs(false));
      if (educationProgram === 0) {
        setEducationProgram(1);
      } else {
        setEducationProgram(+educationProgram + 1);
      }
    }
  }, [dispatch, educationProgram, totalEduPrgsData]);

  React.useEffect(() => {
    if (
      totalAmount &&
      Object.keys(totalAmount).length > 0 &&
      totalAmount.visible &&
      visibleStripeDialog
    ) {
      if (
        totalAmount.data &&
        totalAmount.data.code === "200" &&
        totalAmount.data.data &&
        Object.keys(totalAmount.data.data).length > 0
      ) {
        setPaymentSuccess({
          ...totalAmount.data.data,
          amount: totalAmount.data.amount,
        });
      }
      setVisibleStripeDialog(false);
    }
  }, [totalAmount, visibleStripeDialog]);

  const handleTotalSchoolChange = React.useCallback(
    (e) => {
      if (e.target.value >= 0) {
        setTotalSchools(e.target.value);
      } else {
        toast.current.show({
          severity: "warn",
          
          detail: t("LBL_SETTING_PAGE_ERR_MESSAGE_FOR_COINS"),
          life: 1000,
        });
      }
    },
    [t]
  );

  const handleEducationProgramChange = React.useCallback(
    (e) => {
      if (e.target.value >= 0) {
        setEducationProgram(e.target.value);
      } else {
        toast.current.show({
          severity: "warn",
          
          detail: t("LBL_SETTING_PAGE_ERR_MESSAGE_FOR_COINS"),
          life: 1000,
        });
      }
    },
    [t]
  );

  const handleBuy = React.useCallback(async () => {
    const amount =
      parseInt(totalSchools * 50) + parseInt(educationProgram * 2000);
    if (amount <= 0) {
      toast.current.show({
        severity: "warn",
        
        detail: t("LBL_SETTING_PAGE_ERR_MESSAGE_FOR_AMOUNT"),
        life: 1000,
      });
    } else {
      const stripe = await stripePromise;
      sessionStorage.setItem("amount", amount);
      sessionStorage.setItem("totalSchools", totalSchools);
      sessionStorage.setItem("educationProgram", educationProgram);
      sessionStorage.setItem("iUserId", userByTokenData?.data?.iUserId);
      dispatch(
        SetPayableAmount({
          amount: amount,
          totalSchools: totalSchools,
          educationProgram: educationProgram,
        })
      );

      // setVisibleStripeDialog(true);
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/stripe/create-checkout-session`,
          {
            vLangCode: lang,
            amount: amount,
            success_url: `${
              process.env.REACT_APP_URL
            }/${lang}/payment-sucess/${sessionStorage.getItem("token")}`,
            cancel_url: `${
              process.env.REACT_APP_URL
            }/${lang}/payment-declined/${sessionStorage.getItem("token")}`,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        if (
          res.status === 200 &&
          res.data &&
          res.data.data &&
          Object.keys(res.data.data).length > 0 &&
          res.data.data.url
        ) {
          const result = await stripe.redirectToCheckout({
            sessionId: res.data.data.id,
          });

          if (result.error) {
            console.error(result.error);
          }
        }

        // if (res.status === 404) {
        //   navigate("/logout");
        // }
      } catch (err) {
        if (
          err &&
          err.response &&
          err.response.status &&
          err.response.status === 404
        ) {
          console.log(err);
        }
      }
      if (userByTokenData && userByTokenData.code === "200") {
        dispatch(
          GetRemainCreditCount({ iUserId: userByTokenData.data.iUserId })
        );
      }
    }
  }, [dispatch, userByTokenData, lang, totalSchools, t, educationProgram]);

  return (
    <div className="s-shopping-cart-box">
      <Toast ref={toast} />
      <h2 className="sec-heading">{t("LBL_SETTING_PAGE_CART_BOX_H2")}</h2>
      {/* <h2 className="sec-heading">Shopping cart</h2> */}
      <div className="cart-box">
        <div className="box">
          <div className="product-title">
            <h3>{t("LBL_SETTING_PAGE_CART_BOX_H3")}</h3>
            <p>{t("LBL_SETTING_PAGE_CART_BOX_P_1")}</p>
            {/* <h3>Details of schools</h3> */}
            {/* <p>
            Dokoupení druhé a další školy pro zobrazení detailních
            výsledků.{" "}
          </p> */}
          </div>
          <input
            type="number"
            className="form-control"
            id="total-school"
            name="points"
            step="1"
            value={totalSchools}
            onChange={(e) => handleTotalSchoolChange(e)}
          />
          <p className="price">{t("LBL_SETTING_PAGE_CART_BOX_P_2")}</p>
          {/* <p className="price">50 Kč</p> */}
        </div>
        <div className="box">
          <div className="product-title">
            <h3>{t("LBL_SETTING_PAGE_CART_BOX_PRODUCT_TITLE_H3")}</h3>
            <p>{t("LBL_SETTING_PAGE_CART_BOX_PRODUCT_TITLE_P_1")}</p>
            {/* <h3>Education program</h3>
          <p>Kompletní přípravný kurz na jednotnou přijímací zkoušku</p> */}
          </div>
          <input
            type="number"
            className="form-control"
            id="education-program"
            name="points"
            step="1"
            value={educationProgram}
            onChange={(e) => handleEducationProgramChange(e)}
          />
          <p className="price">
            {t("LBL_SETTING_PAGE_CART_BOX_PRODUCT_TITLE_P_2")}
          </p>
          {/* <p className="price">2 000 Kč</p> */}
        </div>
      </div>
      <div className="buy-box">
        <div className="total-box">
          <div className="form-group">
            <label> {t("LBL_SETTING_PAGE_BUY_BOX_LABEL_1")}</label>
            {/* <label>Discount coupon:</label> */}
            <input type="text" className="form-control" />
          </div>
          <div className="form-group mb-0">
            <label>{t("LBL_SETTING_PAGE_BUY_BOX_LABEL_2")}</label>
            {/* <label>Total amount:</label> */}
            <p className="form-control">
              {parseInt(totalSchools * 50) + parseInt(educationProgram * 2000)}
              {t("LBL_SETTING_PAGE_BUY_BOX_P")}
            </p>
            {/* <p className="form-control">Kč</p> */}
          </div>
        </div>
        <button className="yellow-btn btn" onClick={handleBuy}>
          {t("LBL_SETTING_PAGE_BUY_BOX_BUTTON_TEXT")}
        </button>
        {/* <button className="yellow-btn btn">Buy</button> */}
      </div>
      <div className="text-image">
        <p>
          {t("LBL_SETTING_PAGE_TEXT_IMAGE_P")}
          {/* If all available positions are used, it is possible to add more
        to the shopping cart. */}
        </p>
        <div className="image-box">
          <img
            src="/../assets/images/sova_new_clr 1.png"
            alt=""
            className="img-contain"
          />
        </div>
      </div>
      <div
        className="modal fade payment-detail-modal"
        id="paymentdetails2"
        aria-hidden="true"
        aria-labelledby="paymentdetailsLabel2"
        tabIndex="-1"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h2>{t("LBL_SETTING_PAGE_MODAL_HEADING_1")}</h2>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            {paymentSuccess && Object.keys(paymentSuccess).length > 0 ? (
              <div className="modal-body">
                <div className="details-box">
                  {paymentSuccess.iTransactionId ? (
                    <div className="box">
                      <div className="left">
                        <h3>{t("LBL_SETTING_PAGE_MODAL_HEADING_2")} : </h3>
                      </div>
                      <div className="right">
                        <p>{paymentSuccess.iTransactionId}</p>
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}
                  {paymentSuccess.iCreditDetailOfSchool ? (
                    <div className="box">
                      <div className="left">
                        <h3>{t("LBL_SETTING_PAGE_MODAL_HEADING_3")} : </h3>
                      </div>
                      <div className="right">
                        <p>{paymentSuccess.iCreditDetailOfSchool}</p>
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}
                  {paymentSuccess.iCreditEductionProgram ? (
                    <div className="box">
                      <div className="left">
                        <h3>{t("LBL_SETTING_PAGE_MODAL_HEADING_4")} : </h3>
                      </div>
                      <div className="right">
                        <p>{paymentSuccess.iCreditEductionProgram}</p>
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}

                  {paymentSuccess.amount ? (
                    <div className="box">
                      <div className="left">
                        <h3>{t("LBL_SETTING_PAGE_MODAL_HEADING_5")} : </h3>
                      </div>
                      <div className="right">
                        <p>{paymentSuccess.amount}</p>
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            ) : (
              <></>
            )}
            <div className="modal-footer">
              <button
                className="btn blue-btn"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                {t("LBL_SETTING_PAGE_MODAL_CANDEL_BUTTON")}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ShoppingCart;
