import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import {
  GetCZLesson,
  GetMathLesson,
  ResetTopics,
} from "../../store/action/StudentTopicLessonAction";
import axios from "axios";
import Latex from "react-latex";

function Topic() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { state } = useLocation();
  const { getMathLessonData, getCZLessonData } = useSelector(
    (state) => state.studentTopicLesson
  );
  const [explanation, setExplanation] = React.useState({});
  const [definition, setDefinition] = React.useState({});
  const [examples, setExamples] = React.useState({});
  const [lang, setLang] = React.useState("cz");
  const { userByTokenData } = useSelector((state) => state.user);

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  React.useEffect(() => {
    if (state && Object.keys(state).length > 0 && state.eSubject) {
      if (state.eSubject === "M") {
        dispatch(
          GetMathLesson({
            eSubject: state.eSubject,
            iLessonMathId: state.iLessonMathId,
          })
        );
      } else if (state.eSubject === "C") {
        dispatch(
          GetCZLesson({
            eSubject: state.eSubject,
            iLessonCZId: state.iLessonCZId,
          })
        );
      }
    }
  }, [dispatch, state]);

  React.useEffect(() => {
    if (
      state &&
      state.eSubject === "M" &&
      getMathLessonData &&
      getMathLessonData.code === "200" &&
      getMathLessonData.data &&
      getMathLessonData.data.length > 0 &&
      Object.keys(explanation).length === 0 &&
      Object.keys(definition).length === 0 &&
      Object.keys(examples).length === 0
    ) {
      const resLesson = getMathLessonData.data;
      setExplanation(resLesson.find((item) => item.vType === "explanation"));
      setDefinition(resLesson.find((item) => item.vType === "definition"));
      setExamples(resLesson.find((item) => item.vType === "examples"));
    }
  }, [state, explanation, definition, examples, getMathLessonData]);

  React.useEffect(() => {
    if (
      state &&
      state.eSubject === "C" &&
      getCZLessonData &&
      getCZLessonData.code === "200" &&
      getCZLessonData.data &&
      getCZLessonData.data.length > 0 &&
      Object.keys(explanation).length === 0 &&
      Object.keys(definition).length === 0 &&
      Object.keys(examples).length === 0
    ) {
      const resLesson = getCZLessonData.data;
      setExplanation(resLesson.find((item) => item.vType === "explanation"));
      setDefinition(resLesson.find((item) => item.vType === "definition"));
      setExamples(resLesson.find((item) => item.vType === "examples"));
    }
  }, [state, explanation, definition, examples, getCZLessonData]);

  const handleContinue = React.useCallback(() => {
    // api call
    let values = {};
    if (userByTokenData && userByTokenData.code === "200") {
      values = {
        ...values,
        iUserId: userByTokenData.data.iUserId,
      };
    }
    if (state && state.eSubject === "M") {
      values = {
        ...values,
        vLessonNumber: state.vLessonNumber,
        iLessonMathId: state.iLessonMathId,
        vLangCode: lang,
      };
    } else {
      values = {
        ...values,
        vLessonNumber: state.vLessonNumber,
        iLessonCZId: state.iLessonCZId,
        vLangCode: lang,
      };
    }

    try {
      (async function () {
        await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user-lesson/createUserLessonData`,
          values,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
      })();
    } catch (err) {
      console.log("Error ", err);
    }
    dispatch(ResetTopics());
    setExplanation({});
    setDefinition({});
    setExamples({});
    setTimeout(() => {
      navigate(
        `/${lang}/student-infinite-trees/${sessionStorage.getItem("token")}`,
        {
          state: state,
        }
      );
    }, 200);
  }, [navigate, userByTokenData, lang, state, dispatch]);

  return (
    <div className="white-card plr-50 pt-50 pb-50 custom-min-h intro-topic">
      <div className="inner-box row g-4">
        <Link
          to={`/${lang}/student-infinite-trees/${sessionStorage.getItem(
            "token"
          )}`}
          state={{
            eSubject: state
              ? state.eSubject
              : sessionStorage.getItem("eSubject"),
          }}
        >
          {t("LBL_BACK_TO_ROAD_MAP")}
        </Link>
        <div className="col-md-7">
          <div className="details-left">
            {definition && Object.keys(definition).length > 0 ? (
              <h1 className="sec-heading text-start">{definition.tText} </h1>
            ) : (
              <></>
            )}
            {explanation && Object.keys(explanation).length > 0 ? (
              state.eSubject === "C" ? (
                <p
                  dangerouslySetInnerHTML={{
                    __html: explanation.tText,
                  }}
                />
              ) : (
                <p>
                  <Latex>{explanation.tText}</Latex>
                </p>
              )
            ) : (
              <></>
            )}
            {examples && Object.keys(examples).length > 0 ? (
              state.eSubject === "C" ? (
                <p
                  dangerouslySetInnerHTML={{
                    __html: examples.tText.replaceAll("\n", "<br />"),
                  }}
                />
              ) : (
                <Latex displayMode={true}>{examples.tText}</Latex>
              )
            ) : (
              <></>
            )}
            {/* <div className="video-box">
              <video width="400" controls>
                <source src="mov_bbb.mp4" type="video/mp4" />
                <source src="mov_bbb.ogg" type="video/ogg" />
              </video>
            </div>
            <div className="image-box">
              <img
                src="/../assets/images/topic-chart-image.png"
                className="img-contain"
                alt=""
              />
            </div> */}
          </div>
        </div>
        <div className="col-md-5">
          <div className="details-right text-center">
            <div className="image-box">
              <img
                src="/../assets/images/sova_new_clr 1.png"
                alt=""
                className="img-contain"
              />
            </div>
            <button className="btn yellow-btn" onClick={handleContinue}>
              {t("LBL_TOPIC_CONTINUE_BUTTON")}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Topic;
