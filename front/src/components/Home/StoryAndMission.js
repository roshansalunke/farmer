import React from "react";
import { useTranslation } from "react-i18next";

function StoryAndMission() {
  const { t } = useTranslation();

  return (
    <div className="our-story pt-50 pb-50 story-padding">
      <div className="image-box">
        <img src="../assets/images/AIM 1.png" alt="" className="img-contain" />
      </div>
      {/* <h2 className="title">Our story and mission</h2> */}
      <h2 className="title">{t("LBL_HOME_STORE_AND_MISSION_TITLE")}</h2>
      <p className="descri">
      {t("LBL_HOME_STORE_AND_MISSION_DESCRI")}
        {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non est
        a ex elementum dignissim. In faucibus, arcu et luctus condimentum,
        mauris orci bibendum turpis, a interdum nibh nunc ac arcu. */}
      </p>
    </div>
  );
}

export default StoryAndMission;
