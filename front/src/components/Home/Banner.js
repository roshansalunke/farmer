import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
function Banner() {
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("cz");

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  return (
    <div className="home-banner">
      <div className="image-box">
        <img
          src="/../assets/images/sova_new_clr 1.png"
          alt=""
          className="img-contain"
        />
      </div>
      <h1>
        {t("LBL_HOME_HEADING")}
        {/* Joyful way how to learn <span className="yellow-color">Czech</span> and <span className="pink-color">Math</span> */}
      </h1>
      <p>
        {t("LBL_HOME_P_TAG")}
        {/* Build skills with our education platform, get points and compete with
        your school, friends and other regions our country! */}
      </p>
      <div className="action-btn">
        <Link
          to={
            sessionStorage.getItem("token") &&
            sessionStorage.getItem("token").length > 0 &&
            sessionStorage.getItem("token") !== null
              ? `/${lang}/knowledge-verification/${sessionStorage.getItem(
                  "token"
                )}`
              : `/${lang}/knowledge-verification-l`
          }
          className="green-btn btn"
        >
          {/* Join for free */}
          {t("LBL_HOME_JOIN_BUTTON_TEXT")}
        </Link>
        {sessionStorage.getItem("token") ? (
          <></>
        ) : (
          <Link
            to={"/"}
            className="blue-btn btn"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasRightsingup"
            aria-controls="offcanvasRightsingup"
          >
            {t("LBL_HOME_SIGNUP_BUTTON_TEXT")}
            {/* Sign up */}
          </Link>
        )}
      </div>
    </div>
  );
}

export default Banner;
