import React from "react";
import { useTranslation } from "react-i18next";

function Improvement() {
  const { t } = useTranslation();

  return (
    <div className="table-box table-responsive">
      <table className="table">
        <thead>
          <tr>
            <th colSpan={2}>
              <span className="table-title">
                {t("LBL_IMPROVEMENT_RATE_TABLE_TH")}
              </span>
            </th>
            {/* <th colSpan={2}><span className="table-title">Improvement</span></th> */}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{t("LBL_IMPROVEMENT_RATE_TABLE_TD_1")}</td>
            {/* <td>{t("")}Cody Fisher</td> */}
            <td className="pink-color">-3%</td>
          </tr>
          <tr>
            <td>{t("LBL_IMPROVEMENT_RATE_TABLE_TD_2")}</td>
            {/* <td>Courtney Fisher</td> */}
            <td className="green-color">+1%</td>
          </tr>
          <tr>
            <td>{t("LBL_IMPROVEMENT_RATE_TABLE_TD_3")}</td>
            {/* <td>Daisy Fisher</td> */}
            <td className="green-color">+6%</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Improvement;
