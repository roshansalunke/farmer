import React from "react";
import { useTranslation } from "react-i18next";

function Time() {
  const { t } = useTranslation();

  return (
    <div className="table-box table-responsive">
      <table className="table">
        <thead>
          <tr>
            <th>
              <span className="table-title">{t("LBL_TIME_TABLE_TITLE")}</span>
              {/* <span className="table-title">Time</span> */}
            </th>
            <th>{t("LBL_TIME_TABLE_TH_1")}</th>
            <th>{t("LBL_TIME_TABLE_TH_2")}</th>
            <th>{t("LBL_TIME_TABLE_TH_3")}</th>
            {/* <th>week (minutes)</th>
            <th>month (minutes)</th>
            <th>total (minutes)</th> */}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{t("LBL_TIME_TABLE_TD_TEXT_1")}</td>
            {/* <td>Cody Fisher</td> */}
            <td className="green-color">53</td>
            <td>104</td>
            <td>308</td>
          </tr>
          <tr>
            <td>{t("LBL_TIME_TABLE_TD_TEXT_2")}</td>
            {/* <td>Courtney Fisher</td> */}
            <td className="pink-color">25</td>
            <td>86</td>
            <td>236</td>
          </tr>
          <tr>
            <td>{t("LBL_TIME_TABLE_TD_TEXT_3")}</td>
            <td className="green-color">84</td>
            <td>227</td>
            <td>703</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Time;
