import React from "react";
import { useTranslation } from "react-i18next";

function SuccessRate() {
  const { t } = useTranslation();

  return (
    <div className="table-box table-responsive">
      <table className="table">
        <thead>
          <tr>
            <th colSpan={2}>
              <span className="table-title">{t("LBL_SUCCESS_RATE_TABLE_TH")}</span>
            </th>
            {/* <th colSpan={2}><span className="table-title">Success rate</span></th> */}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{t("LBL_SUCCESS_RATE_TABLE_TD_1")}</td>
            {/* <td>Cody Fisher</td> */}
            <td className="pink-color">48%</td>
          </tr>
          <tr>
            <td>{t("LBL_SUCCESS_RATE_TABLE_TD_2")}</td>
            {/* <td>Courtney Fisher</td> */}
            <td className="green-color">31%</td>
          </tr>
          <tr>
            <td>{t("LBL_SUCCESS_RATE_TABLE_TD_3")}</td>
            {/* <td>Daisy Fisher</td> */}
            <td className="green-color">61%</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default SuccessRate;
