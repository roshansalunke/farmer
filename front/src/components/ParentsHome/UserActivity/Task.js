import React from "react";
import { useTranslation } from "react-i18next";

function Task() {
  const { t } = useTranslation();

  return (
    <div className="table-box table-responsive">
      <table className="table">
        <thead>
          <tr>
            <th>
              <span className="table-title">{t("LBL_TASK_TABLE_TH_1")}</span>
            </th>
            <th>{t("LBL_TASK_TABLE_TH_2")}</th>
            <th>{t("LBL_TASK_TABLE_TH_3")}</th>
            {/* <th><span className="table-title">Task</span></th> */}
            {/* <th>Czech language</th>
            <th>Mathematics</th> */}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{t("LBL_TASK_TABLE_TD_1")}</td>
            {/* <td>Cody Fisher</td> */}
            <td>1.8 chapter</td>
            <td>3.2 chapter</td>
          </tr>
          <tr>
            <td>{t("LBL_TASK_TABLE_TD_2")}</td>
            {/* <td>Courtney Fisher</td> */}
            <td>2.0 chapter</td>
            <td>1.1 chapter</td>
          </tr>
          <tr>
            <td>{t("LBL_TASK_TABLE_TD_3")}</td>
            {/* <td>Daisy Fisher</td> */}
            <td>3.1 chapter</td>
            <td>3.3 chapter</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Task;
