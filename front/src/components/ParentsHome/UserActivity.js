import React from "react";
import Time from "./UserActivity/Time";
import Task from "./UserActivity/Task";
import SuccessRate from "./UserActivity/SuccessRate";
import Improvement from "./UserActivity/Improvement";
import { useTranslation } from "react-i18next";

function UserActivity() {
  const { t } = useTranslation();

  return (
    <div className="right-side">
      <div className="details">
        <h2 className="sec-heading">{t("LBL_USER_ACTIVITY_HEADING")}</h2>
        {/* <h2 className="sec-heading">User activity</h2> */}
        <div className="user-activity-box">
          <div className="left">
            <Time />
            <Task />
          </div>
          <div className="right">
            <SuccessRate />
            <Improvement />
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserActivity;
