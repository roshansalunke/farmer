import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { GetParentChildList } from "../../../store/action/shoppingCartActions";
import { useDispatch, useSelector } from "react-redux";
import { GetElementaryAndHighSchoolList } from "../../../store/action/ParentSchoolDetails";
import {
  STORE_PARENT_CHILD_DATA,
  STORE_USER_SCHOOL_DATA,
} from "../../../store/constants/parentSchoolDetails";
import { Toast } from "primereact/toast";

function SubordinateAccounts() {
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("en");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [childIndex, setChildIndex] = React.useState(0);
  const { userByTokenData } = useSelector((state) => state.user);
  const { getParentChildData, getCreditRemainCount } = useSelector(
    (state) => state.shoppingCart
  );
  const { parentChildData } = useSelector((state) => state.parentSchoolDetails);
  const [isNoCredit, setIsNoCredit] = React.useState(false);
  const toast = React.useRef(null);

  React.useEffect(() => {
    if (
      !getParentChildData &&
      userByTokenData &&
      userByTokenData.code === "200"
    )
      dispatch(GetParentChildList({ iParentId: userByTokenData.data.iUserId }));
  }, [dispatch, userByTokenData, getParentChildData]);

  React.useEffect(() => {
    let lng = "en";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  React.useEffect(() => {
    if (!parentChildData) {
      if (
        getParentChildData &&
        getParentChildData.code === "200" &&
        getParentChildData.data &&
        getParentChildData.data.length > 0
      ) {
        dispatch({ type: STORE_PARENT_CHILD_DATA, payload: { childId: 0 } });
        navigate(
          `/${lang}/parents-student-detail/${sessionStorage.getItem("token")}`,
          {
            state: {
              iUserId: getParentChildData.data[0].iUserId,
              user: getParentChildData.data[0].user,
            },
          }
        );
      }
    }
  }, [dispatch, navigate, getParentChildData, lang, parentChildData]);

  React.useEffect(() => {
    if (parentChildData) {
      setChildIndex(parentChildData.childId);
    }
  }, [dispatch, parentChildData]);

  React.useEffect(() => {
    if (
      (getCreditRemainCount &&
        getCreditRemainCount.code === "200" &&
        getCreditRemainCount.data &&
        (+getCreditRemainCount.data.iRemainEducationProgram === 0 ||
          getCreditRemainCount.data.iRemainEducationProgram === null)) ||
      getCreditRemainCount === undefined
    ) {
      setIsNoCredit(true);
    } else {
      setIsNoCredit(false);
    }
  }, [getCreditRemainCount]);
  const handleStudents = () => {
    if (isNoCredit) {
      toast.current.show({
        severity: "warn",
        // summary: "Warning",
        detail: "Please Create credit First.",
        life: 1000,
      });
    }
  };

  return (
    <div className="box">
      <Toast ref={toast} />
      <div className="title-top">
        <div className="inner-top">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30"
            height="25"
            viewBox="0 0 30 25"
            fill="none"
          >
            <path
              d="M20.625 0C18.7125 0 17.0625 1.09238 16.05 2.74657C17.7375 4.43196 18.75 6.74157 18.75 9.39451C18.75 10.2996 18.6375 11.1111 18.4125 11.9226C19.125 12.2659 19.8375 12.5156 20.625 12.5156C23.7375 12.5156 26.25 9.70662 26.25 6.27341C26.25 2.8402 23.7375 0.031211 20.625 0.031211V0ZM9.375 3.1211C6.2625 3.1211 3.75 5.93009 3.75 9.3633C3.75 12.7965 6.2625 15.6055 9.375 15.6055C12.4875 15.6055 15 12.7965 15 9.3633C15 5.93009 12.4875 3.1211 9.375 3.1211ZM27.1875 12.9838C25.575 14.5755 23.3625 15.5431 20.85 15.6055C21.8625 16.7915 22.5 18.2272 22.5 19.7878V21.8477H30V16.6667C30 15.0437 28.8375 13.6392 27.1875 12.9526V12.9838ZM2.8125 16.1049C1.1625 16.7915 0 18.196 0 19.819V25H18.75V19.819C18.75 18.196 17.5875 16.7915 15.9375 16.1049C14.25 17.7591 11.9625 18.7266 9.375 18.7266C6.7875 18.7266 4.5 17.7278 2.8125 16.1049Z"
              fill="#5182B2"
            />
          </svg>
          <h2>{t("LBL_SUB_ORDINATE_ACCOUNT_TITLE")}</h2>
          {/* <h2>Subordinate accounts</h2> */}
        </div>
        {userByTokenData &&
        userByTokenData.code === "200" &&
        userByTokenData.data &&
        userByTokenData.data.eAccountType === "Parent" ? (
          <div
            className="plus-box"
            id="right_child_offcanvass_id"
            data-bs-toggle={isNoCredit ? "" : "offcanvas"}
            data-bs-target={isNoCredit ? "" : "#offcanvasconnectaccount"}
            aria-controls={isNoCredit ? "" : "offcanvasRightsingup"}
            onClick={() => handleStudents()}
            // onClick={() =>
            //   dispatch({ type: STORE_SCHOOL_DATA, payload: undefined })
            // }
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
            >
              <circle cx="10" cy="10" r="10" fill="#E61C63" />
              <path d="M2 10H9.68178H18" stroke="white" strokeWidth="2" />
              <path d="M10 18V2" stroke="white" strokeWidth="2" />
            </svg>
          </div>
        ) : (
          <></>
        )}
      </div>
      <ul>
        {getParentChildData &&
          getParentChildData.code === "200" &&
          getParentChildData.data &&
          getParentChildData.data.length > 0 &&
          getParentChildData.data.map((child, index) => (
            <li key={index}>
              <span
                onClick={() =>
                  navigate(
                    `/${lang}/parents-student-detail/${sessionStorage.getItem(
                      "token"
                    )}`
                  )
                }
                id={`child${index}`}
                style={{ color: index === childIndex ? "red" : "" }}
              >
                {`${child.vFirstName} ${child.vLastName}`}
              </span>
              <Link
                to={`/${lang}/parents-student-detail/${sessionStorage.getItem(
                  "token"
                )}`}
                state={{ iUserId: child.iUserId, user: child.user }}
                onClick={() => {
                  setChildIndex(index);
                  dispatch(
                    GetElementaryAndHighSchoolList({ iUserId: child.iUserId })
                  );
                  dispatch({
                    type: STORE_PARENT_CHILD_DATA,
                    payload: { childId: index },
                  });
                  dispatch({
                    type: STORE_USER_SCHOOL_DATA,
                    payload: {},
                  });
                }}
              >
                {t("LBL_SUB_ORDINATE_ACCOUNT_LINK_TEXT")}
              </Link>
            </li>
            )
          )}
      </ul>
    </div>
  );
}

export default SubordinateAccounts;
