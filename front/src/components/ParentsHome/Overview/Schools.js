import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import {
  GetRemainCreditCount,
  GetUserShcoolList,
} from "../../../store/action/shoppingCartActions";
import { useDispatch, useSelector } from "react-redux";
import {
  GET_DIFFERENCE_ELEMENTARY_SCHOOL_SUCCESS,
  GET_DIFFERENCE_HIGH_SCHOOL_SUCCESS,
  STORE_PARENT_CHILD_DATA,
  STORE_SCHOOL_TYPE_DATA,
  STORE_USER_SCHOOL_DATA,
} from "../../../store/constants/parentSchoolDetails";
import {
  GetElementarySchoolDifference,
  GetHighSchoolDifference,
  GetParentSchoolDetails,
} from "../../../store/action/ParentSchoolDetails";
import { Toast } from "primereact/toast";

function Schools() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const toast = React.useRef(null);
  const { userByTokenData } = useSelector((state) => state.user);
  const { userSchoolData, getCreditRemainCount } = useSelector(
    (state) => state.shoppingCart
  );
  const { parentUserSchoolData, schoolGradeData } = useSelector(
    (state) => state.parentSchoolDetails
  );
  const [lang, setLang] = React.useState("cz");
  const [isNoCredit, setIsNoCredit] = React.useState(false);

  React.useEffect(() => {
    if (
      (getCreditRemainCount &&
        getCreditRemainCount.code === "200" &&
        getCreditRemainCount.data &&
        +getCreditRemainCount.data.iRemainDetailOfSchool === 0) ||
      getCreditRemainCount === undefined
    ) {
      setIsNoCredit(true);
    } else {
      setIsNoCredit(false);
    }
  }, [getCreditRemainCount]);

  React.useEffect(() => {
    if (
      !getCreditRemainCount &&
      userByTokenData &&
      userByTokenData.code === "200"
    ) {
      dispatch(GetRemainCreditCount({ iUserId: userByTokenData.data.iUserId }));
    }
  }, [userByTokenData, getCreditRemainCount, dispatch]);

  React.useEffect(() => {
    if (!userSchoolData && userByTokenData && userByTokenData.code === "200")
      dispatch(GetUserShcoolList({ iUserId: userByTokenData.data.iUserId }));
  }, [userSchoolData, dispatch, userByTokenData]);

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  const handleUserSchool = (school, index) => {
    if (school) {
      dispatch(
        GetParentSchoolDetails({
          iRedIZOId: school.iRedIZOId,
          eGrade: schoolGradeData || "5",
        })
      );

      if (userByTokenData && userByTokenData.code === "200") {
        dispatch({
          type: GET_DIFFERENCE_ELEMENTARY_SCHOOL_SUCCESS,
          payload: {},
        });
        dispatch({
          type: GET_DIFFERENCE_HIGH_SCHOOL_SUCCESS,
          payload: {},
        });
        dispatch(
          GetElementarySchoolDifference({
            iSchoolId: school.iSchoolId,
            iUserId: userByTokenData.data.iUserId,
          })
        );
        dispatch(
          GetHighSchoolDifference({ iUserId: userByTokenData.data.iUserId })
        );
      }

      let data = {
        iRedIZOId: school.iRedIZOId,
        vSchoolShortName: school.school.vSchoolFullName,
        id: index,
      };
      dispatch({
        type: STORE_USER_SCHOOL_DATA,
        payload: data,
      });
    }
    dispatch({
      type: STORE_PARENT_CHILD_DATA,
      payload: { childId: -1 },
    });
    if (location.pathname.includes("parents-student-detail")) {
      navigate(
        `/${lang}/parent-school-detail/${sessionStorage.getItem("token")}`
      );
    }
  };

  const handleSchoolType = (schoolName) => {
    if (isNoCredit) {
      toast.current.show({
        severity: "warn",
        //
        detail: t("LBL_CREDIT_COINS_WARN_MESSAGE"),
        life: 1000,
      });
    } else {
      dispatch({ type: STORE_SCHOOL_TYPE_DATA, payload: schoolName });
    }
  };

  return (
    <div className="box">
      <Toast ref={toast} />
      <div className="title-top">
        <div className="inner-top">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="27"
            height="30"
            viewBox="0 0 27 30"
            fill="none"
          >
            <path
              d="M3.75 0C3.4875 0 3.2625 0.0375 3.0375 0.1125C1.575 0.4125 0.4125 1.575 0.1125 3.0375C0 3.2625 0 3.4875 0 3.75V24.375C0 27.4875 2.5125 30 5.625 30H26.25V26.25H5.625C4.575 26.25 3.75 25.425 3.75 24.375C3.75 23.325 4.575 22.5 5.625 22.5H26.25V1.875C26.25 0.825 25.425 0 24.375 0H22.5V11.25L18.75 7.5L15 11.25V0H3.75Z"
              fill="#5182B2"
            />
          </svg>
          <h2>{t("LBL_SCHOOL_TITLE")}</h2>
          {/* <h2>Schools</h2> */}
        </div>
        {userByTokenData &&
        userByTokenData.code === "200" &&
        userByTokenData.data &&
        userByTokenData.data.eAccountType === "Parent" ? (
          <div
            className=""
            type="button"
            data-bs-toggle={isNoCredit ? "" : "offcanvas"}
            data-bs-target={
              isNoCredit ? "" : "#offcanvasRightselectschoolsetting"
            }
            aria-controls={isNoCredit ? "" : "offcanvasRightsingup"}
            onClick={() => handleSchoolType("Elementary School")}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
            >
              <circle cx="10" cy="10" r="10" fill="#E61C63" />
              <path d="M2 10H9.68178H18" stroke="white" strokeWidth="2" />
              <path d="M10 18V2" stroke="white" strokeWidth="2" />
            </svg>
          </div>
        ) : (
          <></>
        )}
      </div>
      <ul>
        {userSchoolData &&
          userSchoolData.code === "200" &&
          userSchoolData.data &&
          userSchoolData.data.length > 0 &&
          userSchoolData.data
            .filter((high) => high.eSchoolType !== "High School")
            ?.map((school, index) => (
              <li key={index}>
                <span
                  style={{
                    color:
                      parentUserSchoolData !== undefined &&
                      parentUserSchoolData.id === index
                        ? "red"
                        : "",
                  }}
                  onClick={() => handleUserSchool(school, index)}
                >
                  {school.school.vSchoolFullName}
                </span>
                <span onClick={() => handleUserSchool(school, index)}>
                  {t("LBL_SCHOOL_LINK")}
                </span>
              </li>
            ))}
      </ul>

      <br />

      {/* high school list */}

      <div className="title-top">
        <div className="inner-top">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="27"
            height="30"
            viewBox="0 0 27 30"
            fill="none"
          >
            <path
              d="M3.75 0C3.4875 0 3.2625 0.0375 3.0375 0.1125C1.575 0.4125 0.4125 1.575 0.1125 3.0375C0 3.2625 0 3.4875 0 3.75V24.375C0 27.4875 2.5125 30 5.625 30H26.25V26.25H5.625C4.575 26.25 3.75 25.425 3.75 24.375C3.75 23.325 4.575 22.5 5.625 22.5H26.25V1.875C26.25 0.825 25.425 0 24.375 0H22.5V11.25L18.75 7.5L15 11.25V0H3.75Z"
              fill="#5182B2"
            />
          </svg>
          <h2>{t("LBL_HIGH_SCHOOL_TITLE")}</h2>
          {/* <h2>{schoolTypeData}</h2> */}
        </div>
        {userByTokenData &&
        userByTokenData.code === "200" &&
        userByTokenData.data &&
        userByTokenData.data.eAccountType === "Parent" ? (
          <div
            className=""
            type="button"
            data-bs-toggle={isNoCredit ? "" : "offcanvas"}
            data-bs-target={
              isNoCredit ? "" : "#offcanvasRightselectschoolsetting"
            }
            aria-controls={isNoCredit ? "" : "offcanvasRightsingup"}
            onClick={() => handleSchoolType("High School")}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
            >
              <circle cx="10" cy="10" r="10" fill="#E61C63" />
              <path d="M2 10H9.68178H18" stroke="white" strokeWidth="2" />
              <path d="M10 18V2" stroke="white" strokeWidth="2" />
            </svg>
          </div>
        ) : (
          <></>
        )}
      </div>
      <ul>
        {userSchoolData &&
          userSchoolData.code === "200" &&
          userSchoolData.data &&
          userSchoolData.data.length > 0 &&
          userSchoolData.data
            .filter((high) => high.eSchoolType === "High School")
            ?.map((school, index) => (
              <li key={index}>
                {/* <span onClick={() => handleUserSchool(school, index)}> */}
                <span>{`HS${index + 1}-> ${school.highschool.nazev || ""} - ${
                  school.highschool.smo_name || ""
                }`}</span>
              </li>
            ))}
      </ul>
      {/* end hight school list */}
    </div>
  );
}

export default Schools;
