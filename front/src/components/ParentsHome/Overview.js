import React from "react";
import SubordinateAccounts from "./Overview/SubordinateAccounts";
import Schools from "./Overview/Schools";
import { useTranslation } from "react-i18next";

function Overview() {
  const { t } = useTranslation();

  return (
    <div className="left-side">
      <h1 className="sec-heading">{t("LBL_OVERVIEW_HEADING")}</h1>
      <div className="inner-box">
        <SubordinateAccounts />
        <Schools />
      </div>
    </div>
  );
}

export default Overview;
