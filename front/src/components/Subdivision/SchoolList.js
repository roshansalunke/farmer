import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

function SchoolList() {
  const { t } = useTranslation();
  const { getDistrictNameData } = useSelector((state) => state.region);
  const { totalNumberOfSchoolData } = useSelector(
    (state) => state.schoolStatistic
  );

  return (
    <div className="box">
      <h2>
        {t("LBL_SCHOOL_LIST_TITLE")}&nbsp;
        {/* Number of schools in region */}
        {getDistrictNameData !== undefined ? getDistrictNameData : ""}:
      </h2>
      {totalNumberOfSchoolData !== undefined &&
      totalNumberOfSchoolData.code === "200" &&
      totalNumberOfSchoolData.data &&
      Object.keys(totalNumberOfSchoolData.data).length > 0 ? (
        <ul>
          <li>
            <span className="green-color">
              {totalNumberOfSchoolData.data.Elementary_School_Count}
            </span>
            {t("LBL_SCHOOL_LIST_1")}
            {/* Elementary Schools */}
          </li>
          <li>
            <span className="blue-color">
              {totalNumberOfSchoolData.data.EightYearGymnasium_School_Count}
            </span>
            {t("LBL_SCHOOL_LIST_2")}

            {/* eight-year gymnasium */}
          </li>
          <li>
            <span className="blue-color">
              {totalNumberOfSchoolData.data.SixYearGymnasium_School_Count}
            </span>
            {t("LBL_SCHOOL_LIST_3")}

            {/* six-year gymnasium */}
          </li>
          <li>
            <span className="blue-color">
              {totalNumberOfSchoolData.data.HighSchoolWithGraduation_Count}
            </span>
            {t("LBL_SCHOOL_LIST_4")}

            {/* high school with graduation */}
          </li>
          <li>
            <span className="yellow-color">
              {totalNumberOfSchoolData.data.HighSchoolWithOutGraduation_Count}
            </span>
            {t("LBL_SCHOOL_LIST_5")}

            {/* high school without graduation */}
          </li>
        </ul>
      ) : (
        <></>
      )}
    </div>
  );
}

export default SchoolList;
