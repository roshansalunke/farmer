import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

function TopSchoolList() {
  const { t } = useTranslation();
  const { schoolStatData, schoolEasiestAndHardData } = useSelector(
    (state) => state.schoolStatistic
  );
  const { getDistrictNameData } = useSelector((state) => state.region);

  return (
    <div className="box">
      {(schoolStatData !== undefined &&
        schoolStatData.code === "200" &&
        schoolStatData.data &&
        schoolStatData.data !== null) ||
      (schoolEasiestAndHardData !== undefined &&
        schoolEasiestAndHardData.code === "200" &&
        schoolEasiestAndHardData.data &&
        schoolEasiestAndHardData.data !== null) ? (
        <div className="box">
          <h2>
            {t("LBL_TOP_SCHOOL_LIST_TITLE")}&nbsp;
            {getDistrictNameData !== undefined ? getDistrictNameData : ""}:
            {/* TOP numbers of region Nymburk: */}
          </h2>
          <ul>
            {schoolStatData &&
            schoolStatData.code === "200" &&
            schoolStatData.data &&
            Object.keys(schoolStatData.data).length > 0 ? (
              <>
                <li>
                  <span className="perc green-color">
                    {schoolStatData.data.best_school_percentage
                      ? parseFloat(
                          schoolStatData.data.best_school_percentage
                        ).toFixed(2)
                      : "0"}
                    {"%"}
                  </span>
                  {t("LBL_TOP_SCHOOL_LIST_1")} (
                  {schoolStatData.data.best_school_name || ""})
                </li>
                <li>
                  <span className="perc pink-color">
                    {parseFloat(
                      schoolStatData.data.worst_school_percentage
                    ).toFixed(2) || "0"}
                    {"%"}
                  </span>
                  {t("LBL_TOP_SCHOOL_LIST_2")}

                  {/* Worst school */}
                </li>
              </>
            ) : (
              <></>
            )}
            {schoolEasiestAndHardData &&
            schoolEasiestAndHardData.code === "200" &&
            schoolEasiestAndHardData.data &&
            Object.keys(schoolEasiestAndHardData.data).length > 0 ? (
              <>
                <li>
                  <span className="perc green-color">
                    {schoolEasiestAndHardData.data.Easiest_Topic_Percentage
                      ? parseFloat(
                          schoolEasiestAndHardData.data.Easiest_Topic_Percentage
                        ).toFixed(2)
                      : "0"}
                    %
                  </span>
                  {t("LBL_TOP_SCHOOL_LIST_3")} (
                  {schoolEasiestAndHardData.data.Easiest_Topic_Name || ""})
                </li>
                <li>
                  <span className="perc pink-color">
                    {schoolEasiestAndHardData.data.Hardest_Topic_Percentage
                      ? parseFloat(
                          schoolEasiestAndHardData.data.Hardest_Topic_Percentage
                        ).toFixed(2)
                      : "0"}
                    %
                  </span>
                  {t("LBL_TOP_SCHOOL_LIST_4")} (
                  {schoolEasiestAndHardData.data.Hardest_Topic_Name || ""})
                </li>
              </>
            ) : (
              <></>
            )}
          </ul>
          <div className="w-100 text-center">
            <Link
              to={""}
              data-bs-toggle="offcanvas"
              data-bs-target="#offcanvasRightlogin"
              aria-controls="offcanvasRightlogin"
              className="btn pink-btn"
            >
              {t("LBL_TOP_SCHOOL_LIST_5")}
              {/* Show me my school */}
            </Link>
          </div>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default TopSchoolList;
