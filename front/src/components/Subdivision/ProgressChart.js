import React from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

function ProgressChart() {
  const { t } = useTranslation();
  const { getDistrictSchoolAvgData } = useSelector(
    (state) => state.schoolStatistic
  );
  const { getDistrictNameData } = useSelector((state) => state.region);

  return (
    <div className="progress-sec pt-50  max-1300">
      {getDistrictSchoolAvgData &&
      getDistrictSchoolAvgData.code === "200" &&
      getDistrictSchoolAvgData.data &&
      Object.keys(getDistrictSchoolAvgData.data).length > 0 ? (
        <>
          <div className="box">
            <CircularProgressbar
              value={getDistrictSchoolAvgData.data.District_Percentage}
              maxValue={1}
              text={`${
                parseFloat(
                  getDistrictSchoolAvgData.data.District_Percentage
                ).toFixed(2) || 0
              }%`}
              className={
                getDistrictSchoolAvgData.data.District_Percentage -
                  getDistrictSchoolAvgData.data.Country_Percentage >
                0
                  ? `green-color`
                  : `pink-color`
              }
            />
            <p>
              {t("LBL_PROGRESS_CHAR_P_TEXT")}&nbsp;
              {/* Overall results:&nbsp; */}
              {getDistrictNameData !== undefined ? getDistrictNameData : ""}
              &nbsp;{t("LBL_PROGRESS_CHAR_P_HAS_TEXT")}&nbsp;
              {Math.abs(
                parseFloat(getDistrictSchoolAvgData.data.District_Percentage) -
                  parseFloat(getDistrictSchoolAvgData.data.Country_Percentage)
              ).toFixed(2)}
              % &nbsp;
              {getDistrictSchoolAvgData.data.District_Percentage -
                getDistrictSchoolAvgData.data.Country_Percentage >
              0 ? (
                <span className="green-color">
                  {t("LBL_PROGRESS_CHARR_BETTER_TEXT")}
                  &nbsp;
                </span>
              ) : (
                <span className="pink-color">
                  {t("LBL_PROGRESS_CHARR_WORSE_TEXT")}
                  &nbsp;
                </span>
              )}
              {t("LBL_PROGRESS_CHAR_AFTER_SPAN_TEXT")}&nbsp;(
              {`${
                parseFloat(
                  getDistrictSchoolAvgData.data.Country_Percentage
                ).toFixed(2) || 0
              }%`}
              )
            </p>
          </div>
          <div className="box">
            <CircularProgressbar
              value={getDistrictSchoolAvgData.data.District_C_Percentage}
              maxValue={1}
              text={`${
                parseFloat(
                  getDistrictSchoolAvgData.data.District_C_Percentage
                ).toFixed(2) || 0
              }%`}
              className={
                getDistrictSchoolAvgData.data.District_C_Percentage -
                  getDistrictSchoolAvgData.data.Country_C_Percentage >
                0
                  ? `green-color`
                  : `pink-color`
              }
            />
            <p>
              {t("LBL_PROGRESS_CHAR_P_TEXT")}&nbsp;
              {/* Overall results : */}
              {getDistrictNameData !== undefined ? getDistrictNameData : ""}
              &nbsp;{t("LBL_PROGRESS_CHAR_P_HAS_TEXT")}&nbsp;
              {Math.abs(
                getDistrictSchoolAvgData.data.District_C_Percentage -
                  getDistrictSchoolAvgData.data.Country_C_Percentage
              ).toFixed(2)}
              % &nbsp;
              {getDistrictSchoolAvgData.data.District_C_Percentage -
                getDistrictSchoolAvgData.data.Country_C_Percentage >
              0 ? (
                <span className="green-color">
                  {t("LBL_PROGRESS_CHARR_BETTER_TEXT")}&nbsp;
                  {/* better&nbsp; */}
                </span>
              ) : (
                <span className="pink-color">
                  {t("LBL_PROGRESS_CHARR_WORSE_TEXT")}&nbsp;
                  {/* worse */}
                </span>
              )}
              {t("LBL_PROGRESS_CHAR_AFTER_SPAN_TEXT")}&nbsp;(
              {`${
                getDistrictSchoolAvgData.data.Country_C_Percentage.toFixed(2) ||
                0
              }%`}
              ) {t("LBL_PROGRESS_CHART_IN_CZECH")}
            </p>
          </div>
          <div className="box">
            <CircularProgressbar
              value={getDistrictSchoolAvgData.data.District_M_Percentage}
              maxValue={1}
              text={`${
                parseFloat(
                  getDistrictSchoolAvgData.data.District_M_Percentage
                ).toFixed(2) || 0
              }%`}
              styles={buildStyles({ path: { strokeWidth: "12px" } })}
              className={
                getDistrictSchoolAvgData.data.District_M_Percentage -
                  getDistrictSchoolAvgData.data.Country_M_Percentage >
                0
                  ? `green-color`
                  : `pink-color`
              }
            />
            <p>
              {t("LBL_PROGRESS_CHAR_P_TEXT")}
              {getDistrictNameData !== undefined ? getDistrictNameData : ""}
              &nbsp;{t("LBL_PROGRESS_CHAR_P_HAS_TEXT")}&nbsp;
              {Math.abs(
                getDistrictSchoolAvgData.data.District_M_Percentage -
                  getDistrictSchoolAvgData.data.Country_M_Percentage
              ).toFixed(2)}
              % &nbsp;
              {getDistrictSchoolAvgData.data.District_M_Percentage -
                getDistrictSchoolAvgData.data.Country_M_Percentage >
              0 ? (
                <span className="green-color">
                  {t("LBL_PROGRESS_CHARR_BETTER_TEXT")}
                  &nbsp;
                </span>
              ) : (
                <span className="pink-color">
                  {t("LBL_PROGRESS_CHARR_WORSE_TEXT")}
                  &nbsp;
                </span>
              )}
              {t("LBL_PROGRESS_CHAR_AFTER_SPAN_TEXT")}&nbsp;
              (
              {`${
                getDistrictSchoolAvgData.data.Country_M_Percentage.toFixed(2) ||
                0
              }%`}
              ) {t("LBL_PROGRESS_CHART_IN_MATH")}
            </p>
            {/* <p>
          Math results: Nymburk has 28%
          <span className="green-color">better</span> results than the national
          average (64%)
        </p> */}
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  );
}

export default ProgressChart;
