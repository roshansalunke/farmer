import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

import { DistrictList, StoreDistrictName } from "../../store/action/Region";
import {
  GetNumberOfSchoolStats,
  getBestAndWorstSchoolStat,
  getDistrictSchoolAvgStat,
  getEasiestAndHardStat,
} from "../../store/action/SchoolStatistics";
import { useTranslation } from "react-i18next";

function SelectSubDivision() {
  const { t } = useTranslation();
  const { districtList } = useSelector((state) => state.region);
  const dispatch = useDispatch();
  const [districtId, setDistrictId] = React.useState(null);
  const { state } = useLocation();

  React.useEffect(() => {
    if (!districtList) {
      dispatch(DistrictList());
    }
  }, [districtList, dispatch]);

  React.useEffect(() => {
    if (state && state !== null && state.districtId) {
      if (
        districtList !== undefined &&
        districtList.code === "200" &&
        districtList.data.length > 0
      ) {
        let distName = districtList.data.find(
          (val) => +val.iDistrictId === +state.districtId
        ).vDistrictTitle;
        dispatch(StoreDistrictName(distName));

        dispatch(
          getBestAndWorstSchoolStat({
            iDistrictId: state.districtId,
            vDistrict: distName,
          })
        );
        dispatch(
          getEasiestAndHardStat({
            iDistrictId: state.districtId,
            vDistrict: distName,
          })
        );
        dispatch(
          getDistrictSchoolAvgStat({
            iDistrictId: state.districtId,
            vDistrict: distName,
          })
        );
        dispatch(
          GetNumberOfSchoolStats({
            iDistrictId: state.districtId,
            vDistrict: distName,
          })
        );
      }
    }
  }, [dispatch, districtList, state]);

  const handleChange = (e) => {
    let districtId = e.target.value;
    if (districtId) {
      setDistrictId(districtId);
      if (
        districtId !== null &&
        districtList !== undefined &&
        districtList.code === "200" &&
        districtList.data.length > 0
      ) {
        let districtName = districtList.data.find(
          (val) => +val.iDistrictId === +districtId
        ).vDistrictTitle;
        dispatch(StoreDistrictName(districtName));

        dispatch(
          getBestAndWorstSchoolStat({
            iDistrictId: districtId,
            vDistrict: districtName,
          })
        );
        dispatch(
          getEasiestAndHardStat({
            iDistrictId: districtId,
            vDistrict: districtName,
          })
        );
        dispatch(
          getDistrictSchoolAvgStat({
            iDistrictId: districtId,
            vDistrict: districtName,
          })
        );
        dispatch(
          GetNumberOfSchoolStats({
            iDistrictId: districtId,
            vDistrict: districtName,
          })
        );
      }
    }
  };

  return (
    <h1 className="sec-heading">
      {t("LBL_SCHOOL_LIST_TITLE")}&nbsp;
      <span className="pink-color text-decoration-underline ">
        <select
          className="form-select pink-color"
          aria-label="Default select example"
          onChange={handleChange}
          value={
            districtId !== null
              ? districtId
              : state &&
                districtList !== undefined &&
                districtList.code === "200" &&
                districtList.data.length > 0 &&
                districtList.data.find(
                  (val) => +val.iDistrictId === +state.districtId
                ).iDistrictId
          }
        >
          <option>{t("LBL_SELECT_DISTRICT_OPTION_TITLE")} </option>
          {districtList !== undefined &&
          districtList.code === "200" &&
          districtList.data.length > 0 ? (
            districtList.data.map((reg, index) => (
              <option key={index} value={reg.iDistrictId}>
                {reg.vDistrictTitle}
              </option>
            ))
          ) : (
            <>District Data Not Found</>
          )}
        </select>
      </span>
    </h1>
  );
}

export default SelectSubDivision;
