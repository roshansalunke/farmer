import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Circle from "../../assets/SVG/Circle";
import Calculator from "../../assets/SVG/Calculator";
import Trophy from "../../assets/SVG/Trophy";
import { useLocation, useNavigate } from "react-router-dom";
import {
  GetAllAttemptTestView,
  GetAllCZLesson,
  GetAllMathLesson,
  GetAllViewLesson,
} from "../../store/action/StudentTopicLessonAction";
import { useTranslation } from "react-i18next";

function Lesson() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { state } = useLocation();
  const {
    getAllMathLessonData,
    getAllCZLessonData,
    getAllViewLessonData,
    getAttemptTestData,
  } = useSelector((state) => state.studentTopicLesson);
  const [lesson, setLesson] = React.useState([]);
  const [viewLessonId, setViewLessonId] = React.useState([]);
  const [attemptTestId, setAttemptTestId] = React.useState([]);
  const [position, setPosition] = React.useState(0);
  const { userByTokenData } = useSelector((state) => state.user);

  React.useEffect(() => {
    if (state && Object.keys(state).length > 0 && state.eSubject) {
      if (state.eSubject === "M" && !getAllMathLessonData) {
        dispatch(
          GetAllMathLesson({
            eSubject: state.eSubject,
            iUserId: sessionStorage.getItem("iUserId"),
          })
        );
      } else if (state.eSubject === "C" && !getAllCZLessonData) {
        dispatch(
          GetAllCZLesson({
            eSubject: state.eSubject,
            iUserId: sessionStorage.getItem("iUserId"),
          })
        );
      }
    }
  }, [dispatch, state, getAllMathLessonData, getAllCZLessonData]);

  React.useEffect(() => {
    if (
      state &&
      state.eSubject &&
      state.eSubject === "M" &&
      getAllMathLessonData &&
      getAllMathLessonData.code === "200" &&
      getAllMathLessonData.data &&
      getAllMathLessonData.data.length > 0 &&
      lesson.length <= 0
    ) {
      const resLesson = getAllMathLessonData.data;
      let mainTopics = resLesson.filter((item) => item.eCermat === "");
      if (mainTopics.length > 0) {
        let isShowBonusQuestion = true;
        for (let i = 0; i < mainTopics.length; i++) {
          const regex = `^${mainTopics[i].vLessonNumber}\\..*`;
          isShowBonusQuestion = true;
          const subTopics = resLesson.filter((item) =>
            item.vLessonNumber.match(regex)
          );

          for (let result of subTopics) {
            if (parseInt(result.user_result) < 80) {
              isShowBonusQuestion = false;
              break;
            }
          }

          mainTopics[i] = {
            ...mainTopics[i],
            showBonusQuestion: isShowBonusQuestion,
            subTopics: [mainTopics[i], ...subTopics],
          };
        }
      }
      setLesson(mainTopics);
    }
  }, [state, lesson, getAllMathLessonData]);

  React.useEffect(() => {
    if (
      state &&
      state.eSubject === "C" &&
      getAllCZLessonData &&
      getAllCZLessonData.code === "200" &&
      getAllCZLessonData.data &&
      getAllCZLessonData.data.length > 0 &&
      lesson.length <= 0
    ) {
      const resLesson = getAllCZLessonData.data;
      let mainTopics = resLesson.filter((item) => item.eCermat === "");
      if (mainTopics.length > 0) {
        let isShowBonusQuestion = true;
        for (let i = 0; i < mainTopics.length; i++) {
          isShowBonusQuestion = true;
          const regex = `^${mainTopics[i].vLessonNumber}\\..*`;

          const subTopics = resLesson.filter((item) =>
            item.vLessonNumber.match(regex)
          );
          for (let result of subTopics) {
            if (parseInt(result.user_result) < 80) {
              isShowBonusQuestion = false;
              break;
            }
          }

          mainTopics[i] = {
            ...mainTopics[i],
            showBonusQuestion: isShowBonusQuestion,
            subTopics: [mainTopics[i], ...subTopics],
          };
        }
      }
      setLesson(mainTopics);
    }
  }, [state, lesson, getAllCZLessonData]);

  React.useEffect(() => {
    if (
      userByTokenData &&
      userByTokenData.code === "200" &&
      state &&
      state.eSubject
    ) {
      dispatch(
        GetAllViewLesson({
          iUserId: userByTokenData.data.iUserId,
          eSubject: state.eSubject,
        })
      );
    }
  }, [dispatch, userByTokenData, state]);

  React.useEffect(() => {
    if (
      userByTokenData &&
      userByTokenData.code === "200" &&
      state &&
      state.eSubject
    ) {
      dispatch(
        GetAllAttemptTestView({
          iUserId: userByTokenData.data.iUserId,
          eSubject: state.eSubject,
        })
      );
    }
  }, [dispatch, userByTokenData, state]);

  React.useEffect(() => {
    if (
      getAllViewLessonData &&
      getAllViewLessonData.code === "200" &&
      getAllViewLessonData.data &&
      getAllViewLessonData.data.length > 0
    ) {
      let data = getAllViewLessonData.data;
      let lessonData = [];
      if (state && state.eSubject === "C") {
        for (let sub of data) {
          lessonData.push(sub.iLessonCZId);
        }
      } else if (state && state.eSubject === "M") {
        for (let sub of data) {
          lessonData.push(sub.iLessonMathId);
        }
      }
      setViewLessonId(lessonData);
    }
  }, [state, getAllViewLessonData]);

  React.useEffect(() => {
    if (
      getAttemptTestData &&
      getAttemptTestData.code === "200" &&
      getAttemptTestData.data &&
      getAttemptTestData.data.length > 0
    ) {
      let data = getAttemptTestData.data;
      let lessonData = [];
      if (state && state.eSubject === "C") {
        for (let sub of data) {
          lessonData.push(sub.iLessonId);
        }
      } else if (state && state.eSubject === "M") {
        for (let sub of data) {
          lessonData.push(sub.iLessonId);
        }
      }
      setAttemptTestId(lessonData);
    }
  }, [state, getAttemptTestData]);

  const handleTest = (iLessonCZId, iLessonMathId, vLessonTitle) => {
    navigate(
      `/${
        sessionStorage.getItem("langType")?.toLowerCase() || "cz"
      }/student-test`,
      {
        state: {
          ...state,
          iLessonId: iLessonCZId || iLessonMathId,
          vLessonTitle: vLessonTitle,
        },
      }
    );
  };

  React.useEffect(() => {
    function handleScroll() {
      const y = window.pageYOffset;
      setPosition(y);
    }

    handleScroll(); // initial call to get position of the element on mount
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  // React.useEffect(() => {
  //   const alertUser = () => {
  //     sessionStorage.setItem("scrollId", 0);
  //   };
  //   window.addEventListener("beforeunload", alertUser);
  //   return () => {
  //     window.removeEventListener("beforeunload", alertUser);
  //   };
  // }, []);

  React.useEffect(() => {
    window.scrollTo(0, sessionStorage.getItem("scrollId"));
  }, [viewLessonId]);

  return (
    <div className="white-card plr-50 pt-50 pb-50">
      <h1 className="sec-heading">
        {state && state.eSubject && state.eSubject === "C"
          ? t("LBL_TASK_TABLE_TH_2")
          : t("LBL_TASK_TABLE_TH_3")}
      </h1>
      {lesson && lesson.length > 0 ? (
        lesson.map((topic, index) => (
          <div
            key={index}
            className={index === 0 ? "" : "traning-video-road-map"}
          >
            <div className="up-heading-status">
              <h2>
                {t("LBL_LESSON_SCHOOL_TITLE")} {index + 1}: {topic.vLessonTitle}
              </h2>
            </div>
            <div className={"road-map-box"}>
              {topic.subTopics && topic.subTopics.length > 0 ? (
                topic.subTopics.map((chapterSub, chapterSubKey) => (
                  <React.Fragment key={chapterSubKey}>
                    <div
                      onClick={() => {
                        navigate(
                          `/${
                            sessionStorage.getItem("langType")?.toLowerCase() ||
                            "cz"
                          }/student-intro-topic/${sessionStorage.getItem(
                            "token"
                          )}`,
                          {
                            state: {
                              ...state,
                              iLessonCZId: chapterSub.iLessonCZId,
                              iLessonMathId: chapterSub.iLessonMathId,
                              vLessonNumber: chapterSub.vLessonNumber,
                              vLessonTitle: chapterSub.vLessonTitle,
                            },
                          }
                        );
                        sessionStorage.setItem("scrollId", position);
                      }}
                      className={
                        viewLessonId.includes(
                          parseInt(
                            chapterSub.iLessonMathId || chapterSub.iLessonCZId
                          )
                        )
                          ? "left-box box cursor-pointer"
                          : chapterSubKey === 0
                          ? "first-box box cursor-pointer"
                          : "right-box box cursor-pointer"
                        // chapterSubKey === 0 ? "first-box box" : "right-box box"
                      }
                    >
                      <h2>{chapterSub.vLessonTitle}</h2>
                      <div className="icon-box">
                        <Circle />
                      </div>
                    </div>
                    {chapterSub.lesson_count &&
                    parseInt(chapterSub.lesson_count) > 2 ? (
                      <div
                        onClick={() => {
                          sessionStorage.setItem("scrollId", position);
                          handleTest(
                            chapterSub.iLessonCZId,
                            chapterSub.iLessonMathId,
                            chapterSub.vLessonTitle
                          );
                        }}
                        className={
                          parseInt(chapterSub.user_result) < 80 &&
                          attemptTestId.includes(
                            parseInt(
                              chapterSub.iLessonMathId || chapterSub.iLessonCZId
                            )
                          )
                            ? "left-box box cursor-pointer bg-danger text-light"
                            : attemptTestId.includes(
                                parseInt(
                                  chapterSub.iLessonMathId ||
                                    chapterSub.iLessonCZId
                                )
                              )
                            ? "left-box box cursor-pointer"
                            : chapterSubKey === 0
                            ? "first-box box cursor-pointer"
                            : "right-box box cursor-pointer"
                          // chapterSubKey === 0 ? "first-box box" : "left-box box"
                        }
                      >
                        <h2
                          style={{
                            color:
                              parseInt(chapterSub.user_result) < 80 &&
                              attemptTestId.includes(
                                parseInt(
                                  chapterSub.iLessonMathId ||
                                    chapterSub.iLessonCZId
                                )
                              )
                                ? "white"
                                : "",
                          }}
                        >
                          {chapterSub.vLessonTitle}
                        </h2>
                        <div className="icon-box">
                          <Calculator />
                        </div>
                      </div>
                    ) : (
                      <></>
                    )}
                  </React.Fragment>
                ))
              ) : (
                <></>
              )}
              {topic.showBonusQuestion ? (
                <>
                  <div className="verification-box">
                    <h2>
                      {t("LBL_LESSON_VERIFICATION_BOX_TEXT")}{" "}
                      {topic.vLessonTitle}
                    </h2>
                  </div>
                  <div className="bonus-task">
                    <h2>{t("LBL_LESSON_BONUS_TASK_TEXT")}</h2>
                    <Trophy />
                  </div>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        ))
      ) : (
        <></>
      )}
    </div>
  );
}

export default Lesson;
