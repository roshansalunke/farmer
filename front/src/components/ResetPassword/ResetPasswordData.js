import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Toast } from "primereact/toast";
import axios from "axios";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
// import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

function ResetPasswordData() {
  const navigate = useNavigate();
  const toast = React.useRef(null);
  const { t } = useTranslation();
  const { authToken } = useParams();
  const initialFormValues = {
    vPassword: "",
    vConfirmPassword: "",
  };
  // console.log(authToken);

  const formik = useFormik({
    initialValues: initialFormValues,
    validationSchema: Yup.object({
      vPassword: Yup.string().required(t("LBL_SIGNUP_PASSWORD_ERROR_MESSAGE")),
      vConfirmPassword: Yup.string()
        .oneOf(
          [Yup.ref("vPassword"), null],
          t("LBL_SIGNUP_PASSWORD_COMPARE_ERROR_MESSAGE")
        )
        .required(t("LBL_SIGNUP_CONFIRM_PASSWORD_ERROR_MESSAGE")),
    }),
    enableReinitialize: true,
    onSubmit: async (values, { resetForm }) => {
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/reset-password`,
          {
            ...values,
            vLangCode: sessionStorage.getItem("langType") || "CZ",
            vAuthCode: authToken,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        const { data } = res;
        // console.log(data);
        if (data && data.code === "201") {
        } else if (data && data.code === "200") {
          toast.current.show({
            severity: "success",

            detail: data.message,
            life: 2000,
          });
          setTimeout(() => {
            navigate("/success");
          }, 2000);
        }
      } catch (err) {
        toast.current.show({
          severity: "warn",
          summary: "warn",
          detail: err.response.data.message,
          life: 2000,
        });
      }
    },
  });

  return (
    <>
      <Toast ref={toast} />
      <div className="white-card plr-50 pt-50 pb-50 custom-min-h lesson-done reset-password">
        <div className="row align-items-center g-md-4 gy-5">
          <div className="col-md-6">
            <div className="details-left">
              <h3 className="sec-heading">{t("LBL_RESET_PASSWROD_HEADING")}</h3>

              <form onSubmit={formik.handleSubmit}>
                <div className="row g-4">
                  <div className="col-12">
                    <div className="form-group">
                      <input
                        name="vPassword"
                        id="vPassword"
                        className="form-control"
                        placeholder={t("LBL_LOGIN_OFFCANVAS_PASSWORD_LABEL")}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.vPassword}
                        type="password"
                      />
                      {formik.errors.vPassword && formik.touched.vPassword ? (
                        <div className="text-danger">
                          {formik.errors.vPassword}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <input
                        name="vConfirmPassword"
                        id="vConfirmPassword"
                        className="form-control"
                        placeholder={t(
                          "LBL_LOGIN_OFFCANVAS_CONFIRM_PASSWORD_LABEL"
                        )}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.vConfirmPassword}
                        type="password"
                      />
                      {formik.errors.vConfirmPassword &&
                      formik.touched.vConfirmPassword ? (
                        <div className="text-danger">
                          {formik.errors.vConfirmPassword}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                  <div className="col-12">
                    <button type="submit" className="btn yellow-btn">
                      {t("LBL_FORGOT_PASSWORD_SUBMIT_BUTTON")}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div className="col-md-6">
            <div className="details-right">
              <div className="image-box">
                <img
                  src="/../assets/images/sova_new_clr 1.png"
                  alt=""
                  className="img-contain"
                />
              </div>
              <p className="dark-blue-color">
                {t("LBL_FORGOT_PASSWORD_P_DESCRIPTION")}
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ResetPasswordData;
