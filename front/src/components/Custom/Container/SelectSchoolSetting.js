import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import {
  RegionList,
  DistrictByIdList,
  CityByIdList,
  StoreSchoolData,
  getSchoolList,
} from "../../../store/action/Region";
import { ConfirmDialog } from "primereact/confirmdialog";
import { Toast } from "primereact/toast";

import axios from "axios";
import {
  GetRemainCreditCount,
  GetUserShcoolList,
} from "../../../store/action/shoppingCartActions";
import { Dialog } from "primereact/dialog";

function SelectSchoolSetting() {
  const { t } = useTranslation();
  const [visible, setVisible] = React.useState(false);
  const [successMsg, setSuccessMsg] = React.useState("");
  const { regionList, districtByIdList, citListData, schoolListByCityIdData } =
    useSelector((state) => state.region);
  const { userByCookieData, userByTokenData } = useSelector(
    (state) => state.user
  );
  const { schoolTypeData } = useSelector((state) => state.parentSchoolDetails);
  const toast = React.useRef(null);

  const initialFormValues = {
    iRegionId: "",
    iDistrictId: "",
    iCityId: "",
    iSchoolId: "",
  };
  const [initialValues, setInitialValues] = React.useState(initialFormValues);

  const dispatch = useDispatch();

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: Yup.object({
      iRegionId: Yup.string().required(t("LBL_REGION_ERR_MESSGE")),
      iDistrictId: Yup.string().required(t("LBL_DISTRICT_ERR_MESSGE")),
      iCityId: Yup.string().required(t("LBL_CITY_ERR_MESSGE")),
      iSchoolId: Yup.string().required(t("LBL_SCHOOL_ERR_MESSGE")),
    }),
    enableReinitialize: true,
    onSubmit: (values, { resetForm }) => {
      let redIzId =
        schoolListByCityIdData &&
        schoolListByCityIdData.code === "200" &&
        schoolListByCityIdData.data &&
        schoolListByCityIdData.data.find(
          (val) => +val.iSchoolId === +values.iSchoolId
        )?.iRedIZOId;

      const schoolData = {
        iUserId: userByTokenData && userByTokenData.data.iUserId,
        iSchoolId: values.iSchoolId,
        iRedIZOId: redIzId,
        eSchoolType: schoolTypeData,
        vLangCode: sessionStorage.getItem("langType") || "CZ",
      };
      try {
        (async function () {
          const res = await axios.post(
            `${process.env.REACT_APP_API_URL}/api/user-school/createUserSchoolData`,
            schoolData,
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          await axios.post(
            `${process.env.REACT_APP_API_URL}/api/stripe/remaining-credit-update`,
            {
              iUserId: schoolData.iUserId,
              iRemainDetailOfSchool: 1,
              vLangCode:
                sessionStorage.getItem("langType") || "cz",
            },
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          const { data } = res;
          if (data.code === "200") {
            if (userByTokenData && userByTokenData.code === "200") {
              dispatch(
                GetUserShcoolList({ iUserId: userByTokenData.data.iUserId })
              );
              dispatch(
                GetRemainCreditCount({ iUserId: userByTokenData.data.iUserId })
              );
            }
            setSuccessMsg(data.message);
            setVisible(true);
            resetForm();
          }
        })();
      } catch (err) {
        warnShow(err.response.data.message);
        return;
      }
    },
  });

  React.useEffect(() => {
    if (!regionList) {
      dispatch(RegionList());
    }
  }, [regionList, dispatch]);

  React.useEffect(() => {
    if (
      userByCookieData &&
      userByCookieData.code === "200" &&
      userByCookieData.data &&
      Object.keys(userByCookieData.data).length > 0
    ) {
      dispatch(
        DistrictByIdList({ iRegionId: userByCookieData.data.iRegionId })
      );
      dispatch(
        CityByIdList({ iDistrictId: userByCookieData.data.iDistrictId })
      );
      dispatch(getSchoolList({ iCityId: userByCookieData.data.iCityId }));
      setInitialValues({
        iRegionId: userByCookieData.data.iRegionId,
        iDistrictId: userByCookieData.data.iDistrictId,
        iCityId: userByCookieData.data.iCityId,
        iSchoolId: userByCookieData.data.iSchoolId,
      });
      dispatch(
        StoreSchoolData({
          iRegionId: userByCookieData.data.iRegionId,
          iDistrictId: userByCookieData.data.iDistrictId,
          iCityId: userByCookieData.data.iCityId,
          iSchoolId: userByCookieData.data.iSchoolId,
        })
      );
    }
  }, [userByCookieData, dispatch]);

  const handleRegionChange = (e) => {
    const regionId = e.target.value;
    if (regionId && regionId !== "") {
      dispatch(DistrictByIdList({ iRegionId: regionId }));
    }
    formik.handleChange(e);
  };

  const handleDistrictChange = (e) => {
    const districtId = e.target.value;
    dispatch(CityByIdList({ iDistrictId: districtId }));
    formik.handleChange(e);
  };
  const handleCityChange = (e) => {
    const cityId = e.target.value;
    dispatch(getSchoolList({ vCity: cityId, eSchoolType: schoolTypeData }));
    formik.handleChange(e);
  };

  const handleFormReset = () => {
    formik.resetForm();
  };

  return (
    <>
      <Toast ref={toast} />
      <ConfirmDialog />
      <div
        className="offcanvas offcanvas-end login-canvas choose-school-canvas"
        data-bs-backdrop="static"
        tabIndex="-1"
        id="offcanvasRightselectschoolsetting"
        aria-labelledby="offcanvasRightselectschoolsettingLabel"
      >
        <form onSubmit={formik.handleSubmit}>
          <div className="offcanvas-header">
            <div
              id="offcanvasRightselectschoolsettingLabel"
              className="canvas-title"
            >
              {/* {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_BUTTON_2_COMMON")}&nbsp; */}
              {schoolTypeData !== undefined && schoolTypeData === "High School"
                ? t("LBL_HIGH_SCHOOL_TITLE")
                : t("LBL_SCHOOL_TITLE")}
            </div>
            <button
              type="button"
              className="btn-close text-reset"
              data-bs-dismiss="offcanvas"
              id="closeoffcanvasid"
              aria-label="Close"
              onClick={() => handleFormReset()}
            ></button>
          </div>
          <div className="offcanvas-body">
            <div className="form-group">
              <select
                className="form-select m-0"
                id="iRegionId"
                name="iRegionId"
                value={formik.values.iRegionId}
                onChange={handleRegionChange}
                disabled={
                  userByCookieData &&
                  userByCookieData.code === "200" &&
                  userByCookieData.data &&
                  Object.keys(userByCookieData.data).length > 0
                    ? userByCookieData.data.iRegionId &&
                      userByCookieData.data.iRegionId > 0
                      ? true
                      : false
                    : false
                }
              >
                <option value="">
                  {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_1")}
                </option>
                {regionList !== undefined &&
                regionList.code === "200" &&
                regionList.data.length > 0 ? (
                  regionList.data.map((reg, index) => (
                    <option key={index} value={reg.iRegionId}>
                      {reg.vRegionTitle}
                    </option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              {formik.errors.iRegionId && formik.touched.iRegionId ? (
                <div className="text-danger">{formik.errors.iRegionId}</div>
              ) : null}
            </div>
            <div className="form-group">
              <select
                className="form-select m-0"
                id="iDistrictId"
                name="iDistrictId"
                value={formik.values.iDistrictId}
                onChange={handleDistrictChange}
                disabled={
                  userByCookieData &&
                  userByCookieData.code === "200" &&
                  userByCookieData.data &&
                  Object.keys(userByCookieData.data).length > 0
                    ? userByCookieData.data.iDistrictId &&
                      userByCookieData.data.iDistrictId > 0
                      ? true
                      : false
                    : false
                }
              >
                <option value="">
                  {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_2")}
                </option>
                {districtByIdList !== undefined &&
                districtByIdList.code === "200" &&
                districtByIdList.data.length > 0 ? (
                  districtByIdList.data.map((reg, index) => (
                    <option key={index} value={reg.iDistrictId}>
                      {reg.vDistrictTitle}
                    </option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              {formik.errors.iDistrictId && formik.touched.iDistrictId ? (
                <div className="text-danger">{formik.errors.iDistrictId}</div>
              ) : null}
            </div>
            <div className="form-group">
              <select
                className="form-select m-0"
                id="iCityId"
                name="iCityId"
                value={formik.values.iCityId}
                onChange={handleCityChange}
                disabled={
                  userByCookieData &&
                  userByCookieData.code === "200" &&
                  userByCookieData.data &&
                  Object.keys(userByCookieData.data).length > 0
                    ? userByCookieData.data.iCityId &&
                      userByCookieData.data.iCityId > 0
                      ? true
                      : false
                    : false
                }
              >
                <option value={""}>
                  {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_3")}
                </option>
                {citListData !== undefined &&
                citListData.code === "200" &&
                citListData.data.length > 0 ? (
                  citListData.data.map((reg, index) => (
                    <option key={index} value={reg.vCityTitle}>
                      {reg.vCityTitle}
                    </option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              {formik.errors.iCityId && formik.touched.iCityId ? (
                <div className="text-danger">{formik.errors.iCityId}</div>
              ) : null}
            </div>
            {/* <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="flexRadioselelctschool"
              id="flexRadioselelctschool1"
            />
            <label
              className="form-check-label"
              htmlFor="flexRadioselelctschool1"
            >
              ZŠ Komenského
            </label>
          </div> */}
          <div className="form-group">
            <select
              className="form-select m-0"
              id="iSchoolId"
              name="iSchoolId"
              value={formik.values.iSchoolId}
              onChange={(e) => formik.handleChange(e)}
              disabled={
                userByCookieData &&
                userByCookieData.code === "200" &&
                userByCookieData.data &&
                Object.keys(userByCookieData.data).length > 0
                  ? userByCookieData.data.iSchoolId &&
                    userByCookieData.data.iSchoolId > 0
                    ? true
                    : false
                  : false
              }
            >
              <option value="">
                {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_LEBEL_4")}
              </option>
              {schoolListByCityIdData !== undefined &&
              schoolListByCityIdData.code === "200" &&
              schoolListByCityIdData.data.length > 0 ? (
                schoolListByCityIdData.data.map((school, index) =>
                  schoolTypeData === "High School" ? (
                    <option key={index} value={school.iHighSchoolId}>
                      {`${school.nazev} - ${school.SMO}`}
                    </option>
                  ) : (
                    <option key={index} value={school.iSchoolId}>
                      {school.vSchoolFullName}
                    </option>
                  )
                )
              ) : (
                <></>
              )}
            </select>
            {formik.errors.iSchoolId && formik.touched.iSchoolId ? (
              <div className="text-danger">{formik.errors.iSchoolId}</div>
            ) : null}
            </div>
            <button type="submit" className="btn green-btn">
              {t("LBL_KNOWLEDGE_VERIFICATION_SCHOOL_SUBMIT_TEXT")}
            </button>
          </div>
        </form>
      </div>
      {/* add modal */}
      <Dialog
        visible={visible}
        draggable={false}
        resizable={false}
        modal
        // footer={footerContent}
        style={{ width: "30rem" }}
        onHide={() => {
          setVisible(false);
        }}
      >
        <h3 className="sec-heading" style={{ fontSize: "24px" }}>
          {successMsg}
        </h3>
        <div className="col-12 d-flex justify-content-center">
          <button
            onClick={() => {
              document.getElementById("closeoffcanvasid").click();
              setVisible(false);
            }}
            type="submit"
            className="text-center btn yellow-btn"
          >
            {t("LBL_SETTING_CONFIRM_BUTTON_TEXT")}
          </button>
        </div>
      </Dialog>
    </>
  );
}

export default SelectSchoolSetting;
