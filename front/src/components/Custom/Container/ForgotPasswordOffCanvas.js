import React from "react";
// import { useNavigate } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
// import { useTranslation } from "react-i18next";
import axios from "axios";
import { useTranslation } from "react-i18next";
import { Toast } from "primereact/toast";
// import { useDispatch } from "react-redux";

function ForgotPasswordOffCanvas({ visible, setVisibleRight }) {
  // const { t } = useTranslation();
  const { t } = useTranslation();
  const toast = React.useRef(null);
  // const lang = sessionStorage.getItem("langType")

  // end social sign up validation

  const initialFormValues = {
    vEmail: "",
  };
  const forgotPasswordSchema = Yup.object().shape({
    vEmail: Yup.string().required(t("LBL_LOGIN_USERNAME_ERROR_MESSAGE")),
  });
  // const navigate = useNavigate();
  // const dispatch = useDispatch();

  // React.useEffect(() => {
  //   let lng = "en";
  //   if (sessionStorage.getItem("langType")) {
  //     if (sessionStorage.getItem("langType") === "CZ") {
  //       lng = "cz";
  //     } else if (sessionStorage.getItem("langType") === "EN") {
  //       lng = "en";
  //     }
  //   }
  //   setLang(lng);
  // }, []);

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };

  const successShow = (msg) => {
    toast.current.show({
      severity: "success",

      detail: msg,
      life: 3000,
    });
  };

  const handleSubmit = async (values, { resetForm }) => {
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/user/forget-password`,
        {
          ...values,
          vLangCode: sessionStorage.getItem("langType") || "cz",
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const { data } = res;
      if (data && data.code === "200") {
        successShow(data.message);
      } else if (data && data.status === "error") {
        warnShow(data.message);
        return;
      }
      // }
    } catch (err) {
      warnShow(err.response.data.message);
      return;
    }
    resetForm();
  };

  return (
    <>
      <Toast ref={toast} />
      <div
        className="offcanvas offcanvas-end login-canvas"
        tabIndex="-1"
        // data-bs-backdrop="static"
        id="offcanvasRightforget"
        aria-labelledby="offcanvasRightforgetLabel"
      >
        <div className="offcanvas-header">
          <div className="canvas-title">{t("LBL_FORGOT_PASSWORD_TITLE")}</div>
        </div>
        <div className="offcanvas-body">
          <Formik
            initialValues={initialFormValues}
            validationSchema={forgotPasswordSchema}
            onSubmit={handleSubmit}
          >
            {({ errors, touched, resetForm }) => (
              <Form>
                <div className="form-group">
                  {/* <label>{t("LBL_LOGIN_OFFCANVAS_EMAIL_LABEL")}</label> */}
                  <label>{t("LBL_FORGOT_EMAIL")}</label>
                  <Field name="vEmail" />
                  {errors.vEmail && touched.vEmail ? (
                    <div className="text-danger">{errors.vEmail}</div>
                  ) : null}
                </div>

                <button type="submit" className="green-btn btn">
                  {/* {t("LBL_LOGIN_OFFCANVAS_SUBMIT_BUTTON_TEXT")} */}
                  {t("LBL_FORGOT_PASSWORD_SUBMIT_BUTTON")}
                </button>
                <div className="d-flex justify-content-center my-3">
                  <button
                    type="button"
                    className="text-reset"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasRightlogin"
                    aria-controls="offcanvasRightlogin"
                    // data-bs-dismiss="offcanvas"
                    // aria-label="Close"
                    // id="offcanvasid"
                    style={{ background: "transparent", border: "none" }}
                  >
                    {t("LBL_FORGOT_PASSWORD_BACK_BUTTON")}
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}

export default ForgotPasswordOffCanvas;
