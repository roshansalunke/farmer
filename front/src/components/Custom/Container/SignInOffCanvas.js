import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { Toast } from "primereact/toast";
import { confirmDialog } from "primereact/confirmdialog";
import { useDispatch, useSelector } from "react-redux";
import { GetUserByJWTAuthToken } from "../../../store/action/User";
import { Dialog } from "primereact/dialog";
import {
  STORE_PARENT_CHILD_DATA,
  STORE_USER_SCHOOL_DATA,
} from "../../../store/constants/parentSchoolDetails";

function SignInOffCanvas() {
  const { t } = useTranslation();
  const toast = React.useRef(null);
  const [visible, setVisible] = React.useState(false);
  const [accountType, setAccountType] = React.useState("");
  const [fbResponseData, setFbResponseData] = React.useState({});
  const [lang, setLang] = React.useState("cz");

  // social signup form
  const singUpValues = {
    vFirstName: "",
    vLastName: "",
    vEmail: "",
    vMobileNumber: "",
    eAccountType: "",
    vParentEmail: "",
  };
  const [singUpValue, setSignUpValue] = React.useState(singUpValues);

  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  // here is register basis schema
  const registerSchema = Yup.object().shape({
    vFirstName: Yup.string()
      .required(t("LBL_SINGUP_NAME_ERROR_MESSAGE"))
      .min(2, t("LBL_SINGUP_NAME_VALID_ERROR_MESSAGE")),
    vLastName: Yup.string()
      .required(t("LBL_SINGUP_SURNAME_ERROR_MESSAGE"))
      .min(2, t("LBL_SINGUP_SURNAME_VALID_ERROR_MESSAGE")),
    vEmail: Yup.string()
      .required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE"))
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE")),
    vParentEmail: Yup.string()
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE"))
      .when("eAccountType", {
        is: (v) => v === "Student",
        then: (schema) => schema.required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE")),
      }),
    vMobileNumber: Yup.string()
      .required(t("LBL_SIGNUP_PHONE_NO_ERROR_MESSAGE"))
      .matches(phoneRegExp, t("LBL_SIGNUP_PHONE_NO_VALID_ERROR_MESSAGE")),
    eAccountType: Yup.string().required(
      t("LBL_SIGNUP_ACCOUNT_TYPE_VALID_ERROR_MESSAGE")
    ),
  });
  // end social sign up validation

  const initialFormValues = {
    vEmail: "",
    vPassword: "",
    rememberMe: false,
  };
  const loginSchema = Yup.object().shape({
    vEmail: Yup.string().required(t("LBL_LOGIN_USERNAME_ERROR_MESSAGE")),
    vPassword: Yup.string().required(t("LBL_LOGIN_PASSWORD_ERROR_MESSAGE")),
  });
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { userByTokenData } = useSelector((state) => state.user);

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  React.useEffect(() => {
    if (!userByTokenData && sessionStorage.getItem("token")) {
      dispatch(
        GetUserByJWTAuthToken({ JwtAuthCode: sessionStorage.getItem("token") })
      );
    }
  }, [dispatch, userByTokenData]);

  // facebook signup logic
  React.useEffect(() => {
    window.fbAsyncInit = function () {
      window.FB.init({
        appId: "858535852725986",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v12.0", // Use the latest version of the Facebook Graph API
      });
    };

    // Load the SDK asynchronously
    (function (d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
  }, []);

  // get cookie
  const getCookie = React.useCallback((cookieName) => {
    const cookies = document.cookie.split(";");

    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();

      // Check if the cookie starts with the specified name
      if (cookie.startsWith(cookieName + "=")) {
        return cookie.substring(cookieName.length + 1);
      }
    }

    // Return null if the cookie is not found
    return null;
  }, []);

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",

      detail: msg,
      life: 3000,
    });
  };

  const successShow = (msg) => {
    toast.current.show({
      severity: "success",
      detail: msg,
      life: 300000000000000,
      closable: true,
    });
  };

  const handleSubmit = async (values, { resetForm }) => {
    try {
      const vCookieId = getCookie("user-id");
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/user/login`,
        {
          ...values,
          vLangCode: sessionStorage.getItem("langType") || lang,
          vCookieId: vCookieId,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const { data } = res;
      if (data && data.code === "200" && data.data) {
        sessionStorage.setItem("token", data.data.token);
        sessionStorage.setItem("vJWTToken", data.data.token);
        sessionStorage.setItem("iUserId", data.data.user);
        successShow(data.message);
        setTimeout(() => {
          const element = document.getElementsByTagName("body");
          if (element && element.length > 0) {
            element[0].style.overflow = "unset";
            element[0].style.paddingRight = "0";
            element[0].removeAttribute("data-bs-overflow");
          }

          window.location.href = data.data.url;
          // window.location.href = `${process.env.REACT_APP_URL}/${lang}/settings/${data.data.token}`;
        }, 3000);

        // if (data.data.eAccountType === "Student" && data.data.testCount > 0) {
        //   successShow(data.message);
        //   setTimeout(() => {
        //     const element = document.getElementsByTagName("body");
        //     if (element && element.length > 0) {
        //       element[0].style.overflow = "unset";
        //       element[0].style.paddingRight = "0";
        //       element[0].removeAttribute("data-bs-overflow");
        //     }
        //     // window.location.href = data.data.url;
        //     window.location.href = `${process.env.REACT_APP_URL}/${lang}/student-auth-main/${data.data.token}`;
        //   }, 3000);
        // } else if (data.data.eAccountType === "Student") {
        //   successShow(data.message);
        //   setTimeout(() => {
        //     const element = document.getElementsByTagName("body");
        //     if (element && element.length > 0) {
        //       element[0].style.overflow = "unset";
        //       element[0].style.paddingRight = "0";
        //       element[0].removeAttribute("data-bs-overflow");
        //     }
        //     // window.location.href = data.data.url;
        //     window.location.href = `${process.env.REACT_APP_URL}/${lang}/knowledge-verification/${data.data.token}`;
        //   }, 3000);
        // } else {
        //   successShow(data.message);

        // }
      } else if (data && data.status === "error") {
        warnShow(data.message);
        return;
      }
      // }
    } catch (err) {
      warnShow(err.response.data.message);
      return;
    }
    resetForm();
  };

  const handleLogout = React.useCallback(() => {
    confirmDialog({
      message: t("LBL_LOGOUT_MESSAGE"),
      className: "logout-popup",
      header: t("LBL_CONFIRMATION"),
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        // window.Android.performClick();
        navigate("/logout");
      },
      reject: () => {},
      acceptLabel: t("LBL_YES"),
      rejectLabel: t("LBL_NO"),
    });
  }, [navigate, t]);

  const handleGoogleLogin = async () => {
    const ClientID =
      "96632211137-634omjl3sf50s6dmtg6t1ohv3rh0o7oj.apps.googleusercontent.com";

    // const ClientSecret = "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";

    const googleAuthEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
    // const redirectUri = "http://localhost:3000/login"; // Set your redirect URI
    const redirectUri = `${process.env.REACT_APP_URL}/login`; // Set your redirect URI
    const responseType = "token";
    const scope =
      "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"; // Scopes you want to access

    const authUrl = `${googleAuthEndpoint}?client_id=${ClientID}&redirect_uri=${redirectUri}&response_type=${responseType}&scope=${scope}`;
    window.location.href = authUrl;
  };

  const handleFacebookLogin = () => {
    try {
      window.FB.login(
        function (response) {
          if (response.authResponse) {
            window.FB.api(
              "/me",
              { fields: "name, email" },
              async function (fbRes) {
                setFbResponseData(fbRes);
                try {
                  const authRes = await axios.post(
                    `${process.env.REACT_APP_API_URL}/api/user/social_authentication`,
                    {
                      vEmail: fbRes.email,
                      vLangCode: lang,
                      vFacebookId: fbRes.id,
                    },
                    {
                      headers: {
                        "Content-Type": "application/json",
                      },
                    }
                  );
                  if (authRes && authRes.status === 204) {
                    setVisible(true);
                    document.getElementById("offcanvasid")?.click();
                    setSignUpValue((pre) => ({
                      ...pre,
                      vFirstName: fbRes.name.split(" ").at(0),
                      vLastName: fbRes.name.split(" ").at(1),
                      vEmail: fbRes.email,
                    }));
                    return;
                  } else if (authRes.data.code === "200") {
                    sessionStorage.setItem("token", authRes.data.data.token);
                    setVisible(false);
                    // let url = authRes.data.data.url.split("/");
                    // url.splice(3, 0, lang);
                    // window.location.href = url.join("/");
                    window.location.href = authRes.data.data.url;
                  }
                } catch (err) {
                  console.log(err);
                }
              }
            );
          } else {
            console.log("User cancelled login or did not fully authorize.");
          }
        },
        { scope: "email" } // Request the 'email' permission
      );
    } catch (err) {
      console.log(err);
    }
  };

  //  for social singup submit form
  const handleSignUpSubmit = async (values, { resetForm }) => {
    if (fbResponseData && Object.keys(fbResponseData).length > 0) {
      try {
        const authRes = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/social_authentication`,
          {
            vEmail: fbResponseData.data.email,
            vGoogleId: fbResponseData.data.sub,
            vLangCode: lang
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        if (authRes.status === 204) {
          let data = {
            vEmail: fbResponseData.email,
            vFirstName: values.vFirstName,
            vLastName: values.vLastName,
            vFacebookId: fbResponseData.id,
            vParentEmail: values.vParentEmail,
            vMobileNumber: values.vMobileNumber,
            eAccountType: values.eAccountType,
            vLangCode: lang,
          };

          try {
            let res = await axios.post(
              `${process.env.REACT_APP_API_URL}/api/user/social_register`,
              data,
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
            if (res && res.data.code === "200") {
              sessionStorage.setItem("token", res.data.data.token);
              setVisible(false);
              // let url = res.data.data.url.split("/");
              // url.splice(3, 0, lang);
              // window.location.href = url.join("/");
              window.location.href = res.data.data.url;
            }
          } catch (err) {
            console.log(err);
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    resetForm();
  };

  const handleRemoveOverFlow = React.useCallback(() => {
    const element = document.getElementsByTagName("body");
    if (element && element.length > 0) {
      element[0].style.overflow = "unset";
      element[0].style.paddingRight = "0";
      element[0].removeAttribute("data-bs-overflow");
    }
  }, []);

  return (
    <>
      <Toast ref={toast} />
      {userByTokenData &&
      userByTokenData.code === "200" &&
      userByTokenData.data &&
      sessionStorage.getItem("token") ? (
        <div
          className="offcanvas offcanvas-end login-canvas"
          // data-bs-backdrop="static"
          tabIndex="-1"
          id="offcanvasRightlogin"
          aria-labelledby="offcanvasRightloginLabel"
        >
          <div className="offcanvas-header">
            <div
              id="offcanvasconnectaccountLabel"
              className="canvas-title"
            ></div>
            <button
              type="button"
              className="btn-close text-reset"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
              // id="offcanvasid"
            ></button>
          </div>
          <div className="offcanvas-body">
            <div className="after-login">
              <div className="profile-details">
                <div className="image-box">
                  <img
                    src="/../assets/images/profile-image.png"
                    alt=""
                    className="img-cover"
                  />
                </div>
                <div className="name">{`${userByTokenData.data?.vFirstName} ${userByTokenData.data?.vLastName}`}</div>
                <div className="emial-id">{userByTokenData.data?.vEmail}</div>
              </div>
              {userByTokenData &&
              userByTokenData.code === "200" &&
              userByTokenData.data &&
              userByTokenData.data.eAccountType === "Parent" ? (
                <ul
                  className="nav nav-pills mb-3"
                  id="pills-tab"
                  role="tablist"
                >
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link active"
                      id="pills-student-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#pills-student"
                      type="button"
                      role="tab"
                      aria-controls="pills-student"
                      aria-selected="true"
                    >
                      {/* Student */}
                      {t("LBL_LOGIN_OFFCANVAS_TYPE_1")}
                    </button>
                  </li>
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link"
                      id="pills-administrator-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#pills-administrator"
                      type="button"
                      role="tab"
                      aria-controls="pills-administrator"
                      aria-selected="false"
                    >
                      {/* Administrator */}
                      {t("LBL_LOGIN_OFFCANVAS_TYPE_2")}
                    </button>
                  </li>
                </ul>
              ) : (
                <></>
              )}

              <div className="tab-content" id="pills-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="pills-student"
                  role="tabpanel"
                  aria-labelledby="pills-student-tab"
                  tabIndex="0"
                >
                  <ul>
                    <li onClick={handleRemoveOverFlow}>
                      <Link
                        to={
                          userByTokenData.data.eAccountType === "Student"
                            ? `/${lang}/student-auth-main/${sessionStorage.getItem(
                                "token"
                              )}`
                            : `/${lang}/parents-student-detail/${sessionStorage.getItem(
                                "token"
                              )}`
                        }
                        onClick={() => {
                          dispatch({
                            type: STORE_PARENT_CHILD_DATA,
                            payload: undefined,
                          });
                          dispatch({
                            type: STORE_USER_SCHOOL_DATA,
                            payload: {},
                          });
                        }}
                      >
                        <div className="svg-box">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="22"
                            viewBox="0 0 24 22"
                            fill="none"
                          >
                            <path d="M11.7407 0L0 9.42857H2.93519V22H8.80556V15.7143H14.6759V22H20.5463V9.33429L23.4815 9.42857L11.7407 0Z" />
                          </svg>
                        </div>
                        <span>{t("LBL_LOGIN_OFFCANVAS_MENU_1")}</span>
                      </Link>
                    </li>
                    {/* <li>
                      <Link
                        to={`/${lang}/settings/${sessionStorage.getItem(
                          "token"
                        )}`}
                      >
                        <div className="svg-box">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="22"
                            height="23"
                            viewBox="0 0 22 23"
                            fill="none"
                          >
                            <path d="M18.9416 22.5522C19.8115 22.5522 20.6491 22.1817 21.229 21.5293C21.8089 20.8769 22.0828 20.007 21.9781 19.1452L20.0853 2.70626C19.9081 1.16788 18.6033 0 17.0488 0H4.95118C3.39669 0 2.09189 1.16788 1.91469 2.70626L0.0219167 19.1452C-0.0827899 20.007 0.191058 20.8769 0.770972 21.5293C1.35089 22.1817 2.18854 22.5522 3.05841 22.5522H18.9416ZM1.61668 19.3304L3.51751 2.89151C3.59805 2.15857 4.21824 1.61087 4.95118 1.61087H17.0488C17.7818 1.61087 18.402 2.15857 18.4825 2.89151L20.3833 19.3304C20.4316 19.7412 20.3028 20.1439 20.0209 20.4581C19.747 20.7722 19.3604 20.9413 18.9416 20.9413H3.05841C2.63958 20.9413 2.25297 20.7722 1.97912 20.4581C1.69722 20.1439 1.56835 19.7412 1.61668 19.3304Z" />
                            <path d="M11.0001 9.66516C13.665 9.66516 15.8327 7.49741 15.8327 4.83255V4.02712C15.8327 3.58235 15.472 3.22168 15.0273 3.22168C14.5825 3.22168 14.2218 3.58235 14.2218 4.02712V4.83255C14.2218 6.60902 12.7766 8.05429 11.0001 8.05429C9.22362 8.05429 7.77835 6.60902 7.77835 4.83255V4.02712C7.77835 3.58235 7.41768 3.22168 6.97292 3.22168C6.52815 3.22168 6.16748 3.58235 6.16748 4.02712V4.83255C6.16748 7.49741 8.33523 9.66516 11.0001 9.66516Z" />
                          </svg>
                        </div>
                        <span>{t("LBL_LOGIN_OFFCANVAS_MENU_2")}</span>
                      </Link>
                    </li> */}
                  </ul>
                </div>
                <div
                  className="tab-pane fade"
                  id="pills-administrator"
                  role="tabpanel"
                  aria-labelledby="pills-administrator-tab"
                  tabIndex="0"
                >
                  ...
                </div>
              </div>
              <ul className="bottom-box">
                <li onClick={handleRemoveOverFlow}>
                  <Link
                    // to={`/${lang}/settings/${sessionStorage.getItem("token")}`}
                    to={`/${lang}/page/help`}
                  >
                    <div className="svg-box">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 22 22"
                        fill="none"
                      >
                        <path
                          d="M17.4883 1.97656H4.51172C2.0239 1.97656 0 4.00046 0 6.48828V13.4566C0 15.4417 1.30333 17.1707 3.19194 17.7241L5.37093 19.9033C5.49194 20.0241 5.65576 20.0921 5.8268 20.0921C5.99767 20.0921 6.16165 20.0241 6.2825 19.9033L8.28139 17.9042H17.4883C19.9761 17.9042 22 15.8803 22 13.3925V6.48828C22 4.00046 19.9761 1.97656 17.4883 1.97656ZM20.7109 13.3925C20.7109 15.1695 19.2653 16.6151 17.4883 16.6151H8.01451C7.84364 16.6151 7.67966 16.6831 7.55881 16.804L5.8268 18.536L3.98216 16.6913C3.89941 16.6086 3.79568 16.5499 3.68205 16.5217C2.27315 16.171 1.28906 14.9107 1.28906 13.4566V6.48828C1.28906 4.71129 2.73473 3.26562 4.51172 3.26562H17.4883C19.2653 3.26562 20.7109 4.71129 20.7109 6.48828V13.3925Z"
                          fill="#5182B2"
                        />
                        <path
                          d="M15.0926 8.15668H6.90771C6.55171 8.15668 6.26318 8.44537 6.26318 8.80121C6.26318 9.15721 6.55171 9.44574 6.90771 9.44574H15.0926C15.4486 9.44574 15.7371 9.15721 15.7371 8.80121C15.7371 8.44537 15.4486 8.15668 15.0926 8.15668Z"
                          fill="#5182B2"
                        />
                        <path
                          d="M15.0926 10.4351H6.90771C6.55171 10.4351 6.26318 10.7236 6.26318 11.0796C6.26318 11.4356 6.55171 11.7241 6.90771 11.7241H15.0926C15.4486 11.7241 15.7371 11.4356 15.7371 11.0796C15.7371 10.7236 15.4486 10.4351 15.0926 10.4351Z"
                          fill="#5182B2"
                        />
                      </svg>
                    </div>
                    <span>{t("LBL_LOGIN_OFFCANVAS_MENU_3")}</span>
                  </Link>
                </li>
                {userByTokenData.data.eAccountType === "Student" ? (
                  <>
                    <li onClick={handleRemoveOverFlow}>
                      <Link
                        to={`/${lang}/parents-student-detail/${sessionStorage.getItem(
                          "token"
                        )}`}
                      >
                        <div className="svg-box">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="22"
                            height="22"
                            viewBox="0 0 22 22"
                            fill="none"
                          >
                            <g clipPath="url(#clip0_629_1460)">
                              <path
                                d="M11.1924 22H10.8005C10.2708 21.9989 9.75922 21.8072 9.35911 21.4601C8.95899 21.113 8.69706 20.6336 8.62116 20.1094L8.34616 18.1706C8.33687 17.9873 8.39647 17.8071 8.51325 17.6654C8.63002 17.5238 8.79554 17.4309 8.97729 17.4051C9.15904 17.3793 9.34388 17.4223 9.49551 17.5258C9.64714 17.6293 9.7546 17.7857 9.79678 17.9644L10.0718 19.9031C10.0966 20.0787 10.1841 20.2393 10.318 20.3556C10.4519 20.4718 10.6232 20.5357 10.8005 20.5356H11.1924C11.3647 20.537 11.532 20.478 11.6652 20.3686C11.7984 20.2593 11.889 20.1068 11.9212 19.9375L12.1962 17.9987C12.2167 17.8566 12.2784 17.7235 12.3737 17.616C12.469 17.5085 12.5937 17.4312 12.7324 17.3937C13.2733 17.2476 13.7931 17.0328 14.2793 16.7544C14.4035 16.6833 14.5458 16.65 14.6887 16.6585C14.8315 16.667 14.9688 16.7171 15.0837 16.8025L16.6512 17.9781C16.7924 18.0827 16.9664 18.1333 17.1417 18.1207C17.317 18.108 17.482 18.0331 17.6068 17.9094L17.8818 17.6344C18.0073 17.5092 18.0834 17.3428 18.096 17.166C18.1087 16.9891 18.057 16.8137 17.9505 16.6719L16.7749 15.0837C16.6887 14.9693 16.6381 14.8319 16.6296 14.6888C16.621 14.5457 16.6548 14.4033 16.7268 14.2794C17.0051 13.7932 17.22 13.2733 17.3662 12.7325C17.4036 12.5938 17.4809 12.4691 17.5884 12.3738C17.6959 12.2785 17.829 12.2168 17.9712 12.1963L19.9099 11.9213C20.0855 11.8964 20.2461 11.809 20.3624 11.6751C20.4786 11.5412 20.5425 11.3698 20.5424 11.1925V10.8006C20.5419 10.6285 20.4811 10.4621 20.3706 10.3302C20.2601 10.1983 20.1068 10.1093 19.9374 10.0788L17.9987 9.80375C17.8565 9.78321 17.7234 9.72148 17.6159 9.62619C17.5084 9.53089 17.4311 9.4062 17.3937 9.2675C17.2475 8.72669 17.0326 8.20683 16.7543 7.72063C16.6823 7.59668 16.6485 7.45426 16.6571 7.31118C16.6656 7.16811 16.7162 7.03074 16.8024 6.91625L17.978 5.34875C18.0783 5.20357 18.1231 5.02719 18.1041 4.85177C18.0852 4.67635 18.0039 4.51357 17.8749 4.39313L17.5999 4.125C17.4751 4.00128 17.3101 3.92633 17.1348 3.91372C16.9595 3.90111 16.7855 3.95167 16.6443 4.05625C16.4878 4.15913 16.2979 4.19842 16.1134 4.1661C15.9288 4.13378 15.7636 4.03229 15.6514 3.88233C15.5391 3.73237 15.4883 3.54523 15.5093 3.3591C15.5303 3.17296 15.6215 3.00183 15.7643 2.88063C16.1891 2.56639 16.7123 2.41445 17.2393 2.45228C17.7664 2.4901 18.2625 2.71519 18.638 3.08688L18.913 3.36188C19.2847 3.73744 19.5098 4.23357 19.5476 4.76061C19.5855 5.28765 19.4335 5.81083 19.1193 6.23563L18.2255 7.425C18.3826 7.7456 18.5182 8.07634 18.6312 8.415L20.1093 8.62813C20.6335 8.70403 21.113 8.96596 21.4601 9.36607C21.8072 9.76619 21.9988 10.2778 21.9999 10.8075V11.1994C21.9988 11.7291 21.8072 12.2407 21.4601 12.6408C21.113 13.0409 20.6335 13.3028 20.1093 13.3787L18.6312 13.5919C18.5182 13.9306 18.3827 14.2613 18.2255 14.5819L19.1193 15.7712C19.4335 16.196 19.5855 16.7192 19.5476 17.2463C19.5098 17.7733 19.2847 18.2694 18.913 18.645L18.638 18.92C18.2625 19.2917 17.7664 19.5168 17.2393 19.5546C16.7123 19.5924 16.1891 19.4405 15.7643 19.1262L14.5749 18.2325C14.2543 18.3895 13.9236 18.525 13.5849 18.6381L13.3718 20.1162C13.293 20.6386 13.0301 21.1156 12.6304 21.4611C12.2307 21.8066 11.7207 21.9977 11.1924 22Z"
                                fill="#5182B2"
                              />
                              <path
                                d="M4.91563 19.5594C4.62623 19.5607 4.33941 19.5049 4.07161 19.3952C3.80381 19.2855 3.56029 19.124 3.355 18.92L3.08 18.645C2.70832 18.2694 2.48323 17.7733 2.4454 17.2463C2.40757 16.7192 2.55951 16.196 2.87375 15.7713L3.7675 14.5819C3.61044 14.2613 3.47493 13.9305 3.36188 13.5919L1.88375 13.3787C1.35961 13.3012 0.88087 13.0376 0.535007 12.6362C0.189144 12.2348 -0.000756045 11.7224 2.26229e-06 11.1925V10.8006C0.00113601 10.2709 0.19275 9.75932 0.539855 9.3592C0.88696 8.95908 1.3664 8.69715 1.89063 8.62125L3.36875 8.40812C3.48172 8.06945 3.61724 7.73871 3.77438 7.41813L2.88063 6.23562C2.56638 5.81083 2.41445 5.28764 2.45227 4.76061C2.4901 4.23357 2.71519 3.73744 3.08688 3.36187L3.36188 3.08687C3.73744 2.71519 4.23357 2.4901 4.76061 2.45227C5.28764 2.41445 5.81083 2.56638 6.23563 2.88063L7.425 3.77437C7.74562 3.61731 8.07635 3.4818 8.415 3.36875L8.62813 1.89062C8.70403 1.3664 8.96595 0.886958 9.36607 0.539853C9.76619 0.192747 10.2778 0.00113374 10.8075 0L11.1994 0C11.7287 0.00255111 12.2395 0.194665 12.6394 0.541514C13.0392 0.888363 13.3015 1.36699 13.3787 1.89062L13.6537 3.82938C13.663 4.01272 13.6034 4.1929 13.4867 4.33455C13.3699 4.47619 13.2044 4.56906 13.0226 4.5949C12.8409 4.62074 12.656 4.57769 12.5044 4.47421C12.3528 4.37073 12.2453 4.21429 12.2031 4.03562L11.9212 2.0625C11.8964 1.88695 11.809 1.72628 11.6751 1.61007C11.5412 1.49385 11.3698 1.42991 11.1925 1.43H10.8006C10.6245 1.43155 10.4548 1.49624 10.3223 1.61231C10.1899 1.72838 10.1034 1.88812 10.0787 2.0625L9.80375 4.00125C9.78321 4.14344 9.72148 4.27651 9.62618 4.38402C9.53089 4.49153 9.4062 4.56879 9.2675 4.60625C8.72673 4.75258 8.20689 4.96745 7.72062 5.24563C7.5964 5.31673 7.45414 5.35005 7.31126 5.3415C7.16839 5.33295 7.03111 5.28291 6.91625 5.1975L5.35563 4.05625C5.21384 3.94979 5.03838 3.89813 4.86153 3.91076C4.68468 3.92339 4.51834 3.99947 4.39313 4.125L4.11813 4.4C3.99441 4.52481 3.91946 4.6898 3.90685 4.86509C3.89424 5.04037 3.9448 5.21439 4.04938 5.35563L5.225 6.92312C5.31041 7.03798 5.36045 7.17526 5.369 7.31814C5.37755 7.46101 5.34423 7.60328 5.27313 7.7275C4.99488 8.21374 4.78001 8.73358 4.63375 9.27437C4.59629 9.41307 4.51903 9.53776 4.41152 9.63306C4.30401 9.72836 4.17094 9.79008 4.02875 9.81063L2.0625 10.0787C1.88695 10.1036 1.72628 10.191 1.61007 10.3249C1.49385 10.4588 1.42991 10.6302 1.43 10.8075V11.1994C1.43156 11.3755 1.49624 11.5452 1.61231 11.6777C1.72838 11.8101 1.88812 11.8966 2.0625 11.9213L4.00125 12.1962C4.14344 12.2168 4.27651 12.2785 4.38402 12.3738C4.49153 12.4691 4.56879 12.5938 4.60625 12.7325C4.75251 13.2733 4.96738 13.7931 5.24563 14.2794C5.31673 14.4036 5.35005 14.5459 5.3415 14.6887C5.33295 14.8316 5.28291 14.9689 5.1975 15.0838L4.05625 16.6444C3.9498 16.7862 3.89813 16.9616 3.91076 17.1385C3.92339 17.3153 3.99947 17.4817 4.125 17.6069L4.4 17.8819C4.52482 18.0056 4.6898 18.0805 4.86509 18.0932C5.04038 18.1058 5.21439 18.0552 5.35563 17.9506C5.43217 17.8857 5.52119 17.8371 5.61723 17.8078C5.71326 17.7786 5.81426 17.7693 5.91402 17.7805C6.01378 17.7918 6.11018 17.8233 6.19729 17.8732C6.28439 17.9231 6.36037 17.9903 6.42052 18.0707C6.48068 18.151 6.52376 18.2429 6.54709 18.3405C6.57042 18.4382 6.57353 18.5395 6.5562 18.6384C6.53888 18.7373 6.50151 18.8316 6.44637 18.9155C6.39124 18.9994 6.31952 19.0711 6.23563 19.1262C5.85419 19.4099 5.39094 19.5619 4.91563 19.5594Z"
                                fill="#5182B2"
                              />
                              <path
                                d="M10.9998 14.6644C10.2751 14.6644 9.56661 14.4495 8.96401 14.0468C8.36141 13.6442 7.89173 13.0719 7.61438 12.4023C7.33704 11.7327 7.26447 10.9959 7.40586 10.2851C7.54725 9.57431 7.89625 8.92138 8.40872 8.4089C8.92119 7.89643 9.57412 7.54743 10.2849 7.40604C10.9958 7.26465 11.7325 7.33722 12.4021 7.61457C13.0717 7.89192 13.644 8.36159 14.0466 8.96419C14.4493 9.5668 14.6642 10.2753 14.6642 11C14.6624 11.9713 14.2757 12.9023 13.5889 13.5891C12.9021 14.2759 11.9711 14.6626 10.9998 14.6644ZM10.9998 8.80001C10.5647 8.80001 10.1394 8.92904 9.77757 9.17078C9.41578 9.41252 9.1338 9.75611 8.96729 10.1581C8.80078 10.5601 8.75721 11.0024 8.8421 11.4292C8.92699 11.856 9.13652 12.248 9.44419 12.5556C9.75187 12.8633 10.1439 13.0728 10.5706 13.1577C10.9974 13.2426 11.4397 13.1991 11.8417 13.0325C12.2437 12.866 12.5873 12.5841 12.8291 12.2223C13.0708 11.8605 13.1998 11.4351 13.1998 11C13.1998 10.4165 12.968 9.85695 12.5555 9.44437C12.1429 9.03179 11.5833 8.80001 10.9998 8.80001Z"
                                fill="#5182B2"
                              />
                            </g>
                            <defs>
                              <clipPath id="clip0_629_1460">
                                <rect width="22" height="22" fill="white" />
                              </clipPath>
                            </defs>
                          </svg>
                        </div>
                        <span>{t("LBL_LOGIN_OFFCANVAS_MENU_4")}</span>
                      </Link>
                    </li>
                    <li onClick={handleRemoveOverFlow}>
                      <Link
                        to={`/${lang}/student-force-test/${sessionStorage.getItem(
                          "token"
                        )}`}
                      >
                        <div className="svg-box">
                          <svg
                            width="22"
                            height="22"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 510.87 410.38"
                          >
                            <g id="Layer_2" data-name="Layer 2">
                              <g id="Layer_1-2" data-name="Layer 1">
                                <path d="M510.87,304c-5.57,6.44-13.1,4.64-20.13,4.67-18,.09-35.92.15-53.89,0-4,0-5.13,1.2-5,5.16.26,10.13.2,20.28,0,30.42-.07,3.23.81,5.2,3.77,6.74,10.45,5.44,16.21,15.82,15,26.49a28,28,0,0,1-20.56,23.79c-9.79,2.79-23-2.16-28.07-11.41-2.74-5-6-6.23-11.42-6.21q-92.31.3-184.63,0c-4,0-5.92,1-7.71,4.84-6.72,14.33-19,21.56-34.22,21.87-14.92.3-26.92-7-34.19-20.31-2.57-4.72-5.14-6.78-10.62-6.48-13.48.74-27.14.8-39.73-4.79-9.46-4.2-19.25-8.61-27.26-15-23-18.32-35-42.56-34.31-72.36.8-35.41,17.19-61.65,47.67-79.18,16.23-9.33,34.07-11.13,52.45-10.77,20.62.4,41.25,0,61.87.24,3.73,0,4.31-1.93,5-4.75,3.16-13.09,10.64-22.42,23.29-27.84,19.28-8.24,44,2.4,50.63,22.28,1,3.13,1.95,6.32,3,9.84,1.52.08,3.15.24,4.78.24,35.26,0,70.52.27,105.78,0,33.52-.27,63.45-24.12,71.76-56.57,10.91-42.64-17.45-84.28-58.73-92.17-5.5-1.06-11.25-1.11-16.88-1.18-14.47-.15-29,0-43.42-.14-3.1,0-4.69.86-6,3.73C308,79.47,279.65,82,264.77,72.26a51,51,0,0,1-14.5-14.62c-3-4.67-5.6-6.31-11.17-6.28-52.89.27-105.79.18-158.68.09-3.22,0-4.78.85-6.36,3.91-7.61,14.71-19.68,22.54-36.58,22-15.79-.55-27-8.25-33.81-22.35C-7.1,32.51,7,6.74,30.35.94c19.39-4.82,43.81,9.69,46.46,30.6.49,3.86,2.77,3.76,5.62,3.75q79.08-.07,158.18,0c3.15,0,4.33-.94,5.07-3.88C249,18.3,256.25,8.36,269.15,3c23.06-9.53,47.24,4.53,52.6,27.49,1,4.34,2.76,4.78,6.29,4.78,16.63,0,33.28-.33,49.89.37,22.7,1,41.88,10.45,58.15,26.13a85.24,85.24,0,0,1,24.38,43.27c8.24,36.95-5.1,74.33-37.39,96.61-17.16,11.85-36.23,16.43-56.95,16.18-34.25-.41-68.51-.2-102.77-.39-3.08,0-4.25,1.21-5.48,3.77-7.09,14.71-19,22.43-35.28,22.48s-27.76-7.84-34.89-22.22c-1.54-3.1-3.3-4-6.92-4-23.28.25-46.57.41-69.84,0-33.43-.63-64.33,21.66-73.51,52.79-8,27.08-2.81,51.64,16.37,72.65,15.39,16.87,34.54,25,57.64,24.43,8.3-.2,13.4-.31,16.59-10.5,4.73-15.09,17.16-23.13,33.48-23.8s28.62,6.11,36.44,20.49a43.5,43.5,0,0,1,3.58,10.28c.65,2.66,2,3.58,4.53,3.34,1.32-.13,2.66,0,4,0q91.07,0,182.14.07c3.73,0,6.42-.26,8.07-4.46,2.19-5.56,6.57-9.31,12.07-12C414,350,415.7,347,415.71,345c.2-30.6.13-61.19.11-91.79,0-5-.07-9.94,0-14.9,0-5.74,2.27-8.81,6.79-9.47,4.95-.73,7.29.85,9,6.89,1,3.43,3.41,3.11,5.88,3.11q28.19,0,56.38-.06c6.28,0,12.68-.7,17.05,5.31v6a10.26,10.26,0,0,0-2.23,1.81c-4.46,6.37-9,12.72-13.12,19.3a6,6,0,0,0,.12,5.22c4.16,6.77,8.65,13.34,13.12,19.91.33.48,1.39.46,2.11.67Zm-25.1-47.65c-1.22-.82-1.49-1.16-1.76-1.16-16.09.07-32.17.2-48.26.25-3,0-4,1.2-3.91,4.24.18,9.29.23,18.6-.07,27.88-.13,3.82,1.17,4.93,4.78,4.92,11.29,0,22.57.18,33.86.27,5,0,10,0,16.14,0-3.41-5.12-5.73-9.12-8.56-12.72-3.49-4.43-3-8.38.2-12.61C480.81,263.9,483.16,260.17,485.77,256.34ZM164.3,348.88c-12.5-.23-23.27,9.93-23.57,22.22-.29,11.7,10.51,22.68,22.51,22.9,11.65.22,22.48-10.25,22.66-21.9C186.08,359.52,176.39,349.11,164.3,348.88ZM306.52,39c0-12.07-10.43-22.47-22.53-22.55s-22.53,10.29-22.61,22.45,10.71,22.73,23,22.6C295.9,61.33,306.56,50.52,306.52,39Zm-245.34-.4c.49-12.35-10.33-22.74-23.11-22.06-14.46.78-22,11.59-21.55,23.24A22.61,22.61,0,0,0,40.24,61.46C51.71,60.8,61.77,49.88,61.18,38.55ZM245,205.2c0-12.55-9.55-22.31-21.83-22.38-14.53-.09-22.85,11-23.18,22-.35,11.86,10.5,23,22.48,22.66C235.37,227,244.94,217.91,245,205.2ZM424.05,364.74c-5.75.05-10.92,4.86-10.91,10.15s5.34,10.39,10.77,10.4a10.28,10.28,0,0,0,.14-20.55Z" />
                              </g>
                            </g>
                          </svg>
                        </div>
                        <span>{t("LBL_LOGIN_OFFCANVAS_MENU_5")}</span>
                      </Link>
                    </li>
                  </>
                ) : (
                  <li onClick={handleRemoveOverFlow}>
                    <Link
                      to={`/${lang}/settings/${sessionStorage.getItem(
                        "token"
                      )}`}
                    >
                      <div className="svg-box">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="22"
                          height="22"
                          viewBox="0 0 22 22"
                          fill="none"
                          >
                          <g clipPath="url(#clip0_629_1460)">
                            <path
                              d="M11.1924 22H10.8005C10.2708 21.9989 9.75922 21.8072 9.35911 21.4601C8.95899 21.113 8.69706 20.6336 8.62116 20.1094L8.34616 18.1706C8.33687 17.9873 8.39647 17.8071 8.51325 17.6654C8.63002 17.5238 8.79554 17.4309 8.97729 17.4051C9.15904 17.3793 9.34388 17.4223 9.49551 17.5258C9.64714 17.6293 9.7546 17.7857 9.79678 17.9644L10.0718 19.9031C10.0966 20.0787 10.1841 20.2393 10.318 20.3556C10.4519 20.4718 10.6232 20.5357 10.8005 20.5356H11.1924C11.3647 20.537 11.532 20.478 11.6652 20.3686C11.7984 20.2593 11.889 20.1068 11.9212 19.9375L12.1962 17.9987C12.2167 17.8566 12.2784 17.7235 12.3737 17.616C12.469 17.5085 12.5937 17.4312 12.7324 17.3937C13.2733 17.2476 13.7931 17.0328 14.2793 16.7544C14.4035 16.6833 14.5458 16.65 14.6887 16.6585C14.8315 16.667 14.9688 16.7171 15.0837 16.8025L16.6512 17.9781C16.7924 18.0827 16.9664 18.1333 17.1417 18.1207C17.317 18.108 17.482 18.0331 17.6068 17.9094L17.8818 17.6344C18.0073 17.5092 18.0834 17.3428 18.096 17.166C18.1087 16.9891 18.057 16.8137 17.9505 16.6719L16.7749 15.0837C16.6887 14.9693 16.6381 14.8319 16.6296 14.6888C16.621 14.5457 16.6548 14.4033 16.7268 14.2794C17.0051 13.7932 17.22 13.2733 17.3662 12.7325C17.4036 12.5938 17.4809 12.4691 17.5884 12.3738C17.6959 12.2785 17.829 12.2168 17.9712 12.1963L19.9099 11.9213C20.0855 11.8964 20.2461 11.809 20.3624 11.6751C20.4786 11.5412 20.5425 11.3698 20.5424 11.1925V10.8006C20.5419 10.6285 20.4811 10.4621 20.3706 10.3302C20.2601 10.1983 20.1068 10.1093 19.9374 10.0788L17.9987 9.80375C17.8565 9.78321 17.7234 9.72148 17.6159 9.62619C17.5084 9.53089 17.4311 9.4062 17.3937 9.2675C17.2475 8.72669 17.0326 8.20683 16.7543 7.72063C16.6823 7.59668 16.6485 7.45426 16.6571 7.31118C16.6656 7.16811 16.7162 7.03074 16.8024 6.91625L17.978 5.34875C18.0783 5.20357 18.1231 5.02719 18.1041 4.85177C18.0852 4.67635 18.0039 4.51357 17.8749 4.39313L17.5999 4.125C17.4751 4.00128 17.3101 3.92633 17.1348 3.91372C16.9595 3.90111 16.7855 3.95167 16.6443 4.05625C16.4878 4.15913 16.2979 4.19842 16.1134 4.1661C15.9288 4.13378 15.7636 4.03229 15.6514 3.88233C15.5391 3.73237 15.4883 3.54523 15.5093 3.3591C15.5303 3.17296 15.6215 3.00183 15.7643 2.88063C16.1891 2.56639 16.7123 2.41445 17.2393 2.45228C17.7664 2.4901 18.2625 2.71519 18.638 3.08688L18.913 3.36188C19.2847 3.73744 19.5098 4.23357 19.5476 4.76061C19.5855 5.28765 19.4335 5.81083 19.1193 6.23563L18.2255 7.425C18.3826 7.7456 18.5182 8.07634 18.6312 8.415L20.1093 8.62813C20.6335 8.70403 21.113 8.96596 21.4601 9.36607C21.8072 9.76619 21.9988 10.2778 21.9999 10.8075V11.1994C21.9988 11.7291 21.8072 12.2407 21.4601 12.6408C21.113 13.0409 20.6335 13.3028 20.1093 13.3787L18.6312 13.5919C18.5182 13.9306 18.3827 14.2613 18.2255 14.5819L19.1193 15.7712C19.4335 16.196 19.5855 16.7192 19.5476 17.2463C19.5098 17.7733 19.2847 18.2694 18.913 18.645L18.638 18.92C18.2625 19.2917 17.7664 19.5168 17.2393 19.5546C16.7123 19.5924 16.1891 19.4405 15.7643 19.1262L14.5749 18.2325C14.2543 18.3895 13.9236 18.525 13.5849 18.6381L13.3718 20.1162C13.293 20.6386 13.0301 21.1156 12.6304 21.4611C12.2307 21.8066 11.7207 21.9977 11.1924 22Z"
                              fill="#5182B2"
                            />
                            <path
                              d="M4.91563 19.5594C4.62623 19.5607 4.33941 19.5049 4.07161 19.3952C3.80381 19.2855 3.56029 19.124 3.355 18.92L3.08 18.645C2.70832 18.2694 2.48323 17.7733 2.4454 17.2463C2.40757 16.7192 2.55951 16.196 2.87375 15.7713L3.7675 14.5819C3.61044 14.2613 3.47493 13.9305 3.36188 13.5919L1.88375 13.3787C1.35961 13.3012 0.88087 13.0376 0.535007 12.6362C0.189144 12.2348 -0.000756045 11.7224 2.26229e-06 11.1925V10.8006C0.00113601 10.2709 0.19275 9.75932 0.539855 9.3592C0.88696 8.95908 1.3664 8.69715 1.89063 8.62125L3.36875 8.40812C3.48172 8.06945 3.61724 7.73871 3.77438 7.41813L2.88063 6.23562C2.56638 5.81083 2.41445 5.28764 2.45227 4.76061C2.4901 4.23357 2.71519 3.73744 3.08688 3.36187L3.36188 3.08687C3.73744 2.71519 4.23357 2.4901 4.76061 2.45227C5.28764 2.41445 5.81083 2.56638 6.23563 2.88063L7.425 3.77437C7.74562 3.61731 8.07635 3.4818 8.415 3.36875L8.62813 1.89062C8.70403 1.3664 8.96595 0.886958 9.36607 0.539853C9.76619 0.192747 10.2778 0.00113374 10.8075 0L11.1994 0C11.7287 0.00255111 12.2395 0.194665 12.6394 0.541514C13.0392 0.888363 13.3015 1.36699 13.3787 1.89062L13.6537 3.82938C13.663 4.01272 13.6034 4.1929 13.4867 4.33455C13.3699 4.47619 13.2044 4.56906 13.0226 4.5949C12.8409 4.62074 12.656 4.57769 12.5044 4.47421C12.3528 4.37073 12.2453 4.21429 12.2031 4.03562L11.9212 2.0625C11.8964 1.88695 11.809 1.72628 11.6751 1.61007C11.5412 1.49385 11.3698 1.42991 11.1925 1.43H10.8006C10.6245 1.43155 10.4548 1.49624 10.3223 1.61231C10.1899 1.72838 10.1034 1.88812 10.0787 2.0625L9.80375 4.00125C9.78321 4.14344 9.72148 4.27651 9.62618 4.38402C9.53089 4.49153 9.4062 4.56879 9.2675 4.60625C8.72673 4.75258 8.20689 4.96745 7.72062 5.24563C7.5964 5.31673 7.45414 5.35005 7.31126 5.3415C7.16839 5.33295 7.03111 5.28291 6.91625 5.1975L5.35563 4.05625C5.21384 3.94979 5.03838 3.89813 4.86153 3.91076C4.68468 3.92339 4.51834 3.99947 4.39313 4.125L4.11813 4.4C3.99441 4.52481 3.91946 4.6898 3.90685 4.86509C3.89424 5.04037 3.9448 5.21439 4.04938 5.35563L5.225 6.92312C5.31041 7.03798 5.36045 7.17526 5.369 7.31814C5.37755 7.46101 5.34423 7.60328 5.27313 7.7275C4.99488 8.21374 4.78001 8.73358 4.63375 9.27437C4.59629 9.41307 4.51903 9.53776 4.41152 9.63306C4.30401 9.72836 4.17094 9.79008 4.02875 9.81063L2.0625 10.0787C1.88695 10.1036 1.72628 10.191 1.61007 10.3249C1.49385 10.4588 1.42991 10.6302 1.43 10.8075V11.1994C1.43156 11.3755 1.49624 11.5452 1.61231 11.6777C1.72838 11.8101 1.88812 11.8966 2.0625 11.9213L4.00125 12.1962C4.14344 12.2168 4.27651 12.2785 4.38402 12.3738C4.49153 12.4691 4.56879 12.5938 4.60625 12.7325C4.75251 13.2733 4.96738 13.7931 5.24563 14.2794C5.31673 14.4036 5.35005 14.5459 5.3415 14.6887C5.33295 14.8316 5.28291 14.9689 5.1975 15.0838L4.05625 16.6444C3.9498 16.7862 3.89813 16.9616 3.91076 17.1385C3.92339 17.3153 3.99947 17.4817 4.125 17.6069L4.4 17.8819C4.52482 18.0056 4.6898 18.0805 4.86509 18.0932C5.04038 18.1058 5.21439 18.0552 5.35563 17.9506C5.43217 17.8857 5.52119 17.8371 5.61723 17.8078C5.71326 17.7786 5.81426 17.7693 5.91402 17.7805C6.01378 17.7918 6.11018 17.8233 6.19729 17.8732C6.28439 17.9231 6.36037 17.9903 6.42052 18.0707C6.48068 18.151 6.52376 18.2429 6.54709 18.3405C6.57042 18.4382 6.57353 18.5395 6.5562 18.6384C6.53888 18.7373 6.50151 18.8316 6.44637 18.9155C6.39124 18.9994 6.31952 19.0711 6.23563 19.1262C5.85419 19.4099 5.39094 19.5619 4.91563 19.5594Z"
                              fill="#5182B2"
                            />
                            <path
                              d="M10.9998 14.6644C10.2751 14.6644 9.56661 14.4495 8.96401 14.0468C8.36141 13.6442 7.89173 13.0719 7.61438 12.4023C7.33704 11.7327 7.26447 10.9959 7.40586 10.2851C7.54725 9.57431 7.89625 8.92138 8.40872 8.4089C8.92119 7.89643 9.57412 7.54743 10.2849 7.40604C10.9958 7.26465 11.7325 7.33722 12.4021 7.61457C13.0717 7.89192 13.644 8.36159 14.0466 8.96419C14.4493 9.5668 14.6642 10.2753 14.6642 11C14.6624 11.9713 14.2757 12.9023 13.5889 13.5891C12.9021 14.2759 11.9711 14.6626 10.9998 14.6644ZM10.9998 8.80001C10.5647 8.80001 10.1394 8.92904 9.77757 9.17078C9.41578 9.41252 9.1338 9.75611 8.96729 10.1581C8.80078 10.5601 8.75721 11.0024 8.8421 11.4292C8.92699 11.856 9.13652 12.248 9.44419 12.5556C9.75187 12.8633 10.1439 13.0728 10.5706 13.1577C10.9974 13.2426 11.4397 13.1991 11.8417 13.0325C12.2437 12.866 12.5873 12.5841 12.8291 12.2223C13.0708 11.8605 13.1998 11.4351 13.1998 11C13.1998 10.4165 12.968 9.85695 12.5555 9.44437C12.1429 9.03179 11.5833 8.80001 10.9998 8.80001Z"
                              fill="#5182B2"
                            />
                          </g>
                          <defs>
                            <clipPath id="clip0_629_1460">
                              <rect width="22" height="22" fill="white" />
                            </clipPath>
                          </defs>
                        </svg>
                      </div>
                      <span>{t("LBL_LOGIN_OFFCANVAS_MENU_6")}</span>
                    </Link>
                  </li>
                )}
                <li onClick={handleRemoveOverFlow}>
                  <div onClick={handleLogout}>
                    <div className="svg-box">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        height="24"
                        viewBox="0 0 24 24"
                        width="24"
                      >
                        <path
                          d="M17 16L21 12M21 12L17 8M21 12L7 12M13 16V17C13 18.6569 11.6569 20 10 20H6C4.34315 20 3 18.6569 3 17V7C3 5.34315 4.34315 4 6 4H10C11.6569 4 13 5.34315 13 7V8"
                          stroke="#5182B2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                        />
                      </svg>
                    </div>
                    <span>{t("LBL_LOGIN_OFFCANVAS_MENU_7")}</span>
                  </div>
                </li>
                <li onClick={handleRemoveOverFlow}>
                  <div
                    onClick={() =>
                      navigate(
                        `/delete-account/${sessionStorage.getItem("token")}`
                      )
                    }
                  >
                    <div className="svg-box">
                      <i className="far fa-user-times"></i>
                    </div>
                    <span>{t("LBL_LOGIN_OFFCANVAS_MENU_8")}</span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      ) : (
        <>
          <div
            className="offcanvas offcanvas-end login-canvas"
            // data-bs-backdrop="static"
            tabIndex="-1"
            id="offcanvasRightlogin"
            aria-labelledby="offcanvasRightloginLabel"
          >
            <div className="offcanvas-header">
              <div id="offcanvasRightloginLabel" className="canvas-title">
                {t("LBL_LOGIN_OFFCANVAS_TITLE")}
                {/* Log in to your Aimos Account! */}
              </div>
              <button
                type="button"
                className="btn-close text-reset"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
                id="offcanvasid"
              ></button>
            </div>
            <div className="offcanvas-body">
              <Formik
                initialValues={initialFormValues}
                validationSchema={loginSchema}
                onSubmit={handleSubmit}
              >
                {({ errors, touched, resetForm }) => (
                  <Form className="add-text">
                    <div className="form-group">
                      <label>{t("LBL_LOGIN_OFFCANVAS_EMAIL_LABEL")}</label>
                      {/* <label>Username or Email</label> */}
                      <Field name="vEmail" />
                      {errors.vEmail && touched.vEmail ? (
                        <div className="text-danger">{errors.vEmail}</div>
                      ) : null}
                    </div>
                    <div className="form-group">
                      <label>{t("LBL_LOGIN_OFFCANVAS_PASSWORD_LABEL")}</label>
                      {/* <label>Password</label> */}
                      <Field name="vPassword" type="password" />
                      {errors.vPassword && touched.vPassword ? (
                        <div className="text-danger">{errors.vPassword}</div>
                      ) : null}
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <div className="form-check d-flex">
                        <Field
                          type="checkbox"
                          name="rememberMe"
                          id="flexCheckDefault"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="flexCheckDefault"
                        >
                          {t("LBL_LOGIN_OFFCANVAS_REMEMBERME_TEXT")}
                          {/* Remember me */}
                        </label>
                      </div>
                      <button
                        className="forget-btn"
                        type="button"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasRightforget"
                        aria-controls="offcanvasRightforget"
                        // onClick={() => setVisibleRight(true)}
                      >
                        {t("LBL_LOGIN_OFFCANVAS_FORGOT_PASSWORD_BUTTON_TEXT")}
                        {/* Forgot Password */}
                      </button>
                    </div>
                    <button type="submit" className="green-btn btn">
                      {t("LBL_LOGIN_OFFCANVAS_SUBMIT_BUTTON_TEXT")}
                      {/* Login */}
                    </button>
                    <p className="sign-up blue-text">
                      {t("LBL_LOGIN_OFFCANVAS_SIGNUP_P_TEXT")}
                      {/* Don't have an account? */}
                      <span
                        type="button"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasRightsingup"
                        aria-controls="offcanvasRightsingup"
                        onClick={resetForm}
                      >
                        {t("LBL_LOGIN_OFFCANVAS_SINGNUP_SPAN_BUTTON")}
                        {/* Sign up */}
                      </span>
                    </p>
                  </Form>
                )}
              </Formik>
              <p className="blue-text">
                {t("LBL_LOGIN_OFFCANVAS_BLUE_P_TEXT")}
                {/* or use one of these platforms */}
              </p>
              <ul className="signup-with">
                <li>
                  <img
                    // onClick={() => setVisible(true)}
                    onClick={handleGoogleLogin}
                    src="/../assets/images/google login.png"
                    alt=""
                    className="img-contain"
                  />
                  {/* <Button
                  label=""
                  icon="pi pi-external-link"
                  onClick={() => setVisible(true)}
                /> */}
                </li>
                {/* <li>
                  <img
                    onClick={handleFacebookLogin}
                    src="/../assets/images/fb logo.png"
                    alt=""
                    className="img-contain"
                  />
                </li>
                <li>
                  <img
                    src="/../assets/images/twitter logo.png"
                    alt=""
                    className="img-contain"
                  />
                </li> */}
              </ul>
            </div>
          </div>
          {/* social signup form */}
          <div className="card flex justify-content-center">
            <Dialog
              visible={visible}
              draggable={false}
              resizable={false}
              modal
              // footer={footerContent}
              style={{ width: "30rem" }}
              onHide={() => {
                setVisible(false);
                setAccountType("");
              }}
              header={<h4>{t("LBL_SINGUP_OFFCANVAS_MODAL_TITLE")}</h4>}
            >
              {/* <h4></h4> <br /> */}
              <Formik
                initialValues={singUpValue}
                validationSchema={registerSchema}
                onSubmit={handleSignUpSubmit}
              >
                {({ errors, touched, handleChange, values, resetForm }) => (
                  <Form>
                    <div className="row g-3">
                      <div className="col-12">
                        <div className="form-group">
                          <label>{t("LBL_SINGUP_OFFCANVAS_NAME_LABEL")}</label>
                          {/* <label>Name</label> */}
                          <Field
                            className="form-control"
                            name="vFirstName"
                            onChange={handleChange}
                            value={values.vFirstName}
                            // value={googleRes && googleRes.data.name.split(" ").at(0)}
                          />
                          {errors.vFirstName && touched.vFirstName ? (
                            <div className="text-danger">
                              {errors.vFirstName}
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          <label>
                            {t("LBL_SINGUP_OFFCANVAS_SURNAME_LABEL")}
                          </label>
                          {/* <label>vLastName</label> */}
                          <Field
                            className="form-control"
                            name="vLastName"
                            onChange={handleChange}
                            value={values.vLastName}
                          />
                          {errors.vLastName && touched.vLastName ? (
                            <div className="text-danger">
                              {errors.vLastName}
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          <label>{t("LBL_SINGUP_OFFCANVAS_EMAIL_LABEL")}</label>
                          {/* <label>vEmail</label> */}
                          <Field
                            className="form-control"
                            name="vEmail"
                            onChange={handleChange}
                            value={values.vEmail}
                            type="email"
                          />
                          {errors.vEmail && touched.vEmail ? (
                            <div className="text-danger">
                              {errors.vEmail ? errors.vEmail : ""}
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          <label>
                            {t("LBL_SINGUP_OFFCANVAS_PHONE_NO_LABEL")}
                          </label>
                          {/* <label>Telephone number</label> */}
                          <Field
                            name="vMobileNumber"
                            className="form-control"
                          />
                          {errors.vMobileNumber && touched.vMobileNumber ? (
                            <div className="text-danger">
                              {errors.vMobileNumber}
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          {/* <label>Telephone number</label> */}
                          <label
                            className="form-check-label mb-2"
                            htmlFor="flexCheckDefault"
                          >
                            {t("LBL_SINGUP_OFFCANVAS_PARENT_OR_STUDENT_TEXT")}?
                          </label>
                          <div className="d-flex flex-wrap ">
                            <div className="form-check align-items-center p-0">
                              <label
                                className="form-check-label  mb-0 me-2"
                                htmlFor="flexCheckDefault1"
                              >
                                {t("LBL_SINGUP_OFFCANVAS_PARENT_TEXT")}
                              </label>
                              <Field
                                type="radio"
                                name="eAccountType"
                                id="flexCheckDefault1"
                                value="Parent"
                                onChange={(e) => {
                                  setAccountType("");
                                  handleChange(e);
                                }}
                              />
                            </div>
                            <div className="form-check align-items-center">
                              <label
                                className="form-check-label mb-0 me-2"
                                htmlFor="flexCheckDefault2"
                              >
                                {t("LBL_SINGUP_OFFCANVAS_STUDENT_TEXT")}
                              </label>
                              <Field
                                type="radio"
                                name="eAccountType"
                                id="flexCheckDefault2"
                                value="Student"
                                onChange={(e) => {
                                  setAccountType(e.target.value);
                                  handleChange(e);
                                }}
                              />
                            </div>
                          </div>
                          {errors.eAccountType && touched.eAccountType ? (
                            <div className="text-danger">
                              {errors.eAccountType}
                            </div>
                          ) : null}
                          {accountType === "Student" ? (
                            <div className="form-group mt-3">
                              <label>
                                {t("LBL_SINGUP_OFFCANVAS_PARENT_EMAIL_LABEL")}
                              </label>
                              <Field
                                name="vParentEmail"
                                type="email"
                                className="form-control"
                              />
                              <div className="text-danger">
                                {errors.vParentEmail && touched.vParentEmail ? (
                                  <div className="text-danger">
                                    {errors.vParentEmail}
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          ) : (
                            <></>
                          )}
                        </div>
                      </div>
                      <div className="col-12">
                        <button type="submit" className="green-btn btn w-auto">
                          {t("LBL_SINGUP_OFFCANVAS_BUTTON")}
                          {/* SIGN UP */}
                        </button>
                      </div>
                    </div>
                  </Form>
                )}
              </Formik>
            </Dialog>
          </div>
        </>
      )}
    </>
  );
}

export default SignInOffCanvas;
