import React from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import i18n from "i18next";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { GetLanguageList } from "../../../store/action/LanguageType";
import { GetUserByJWTAuthToken } from "../../../store/action/User";
import { GetApproachText } from "../../../store/action/Content";

function Header() {
  const { t } = useTranslation();
  const { languageTypeData } = useSelector((state) => state.languageType);
  const { userByTokenData } = useSelector((state) => state.user);
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const lang = sessionStorage.getItem("langType")
    ? sessionStorage.getItem("langType")
    : "CZ";

  React.useEffect(() => {
    if (!languageTypeData) {
      dispatch(GetLanguageList());
    }
  }, [dispatch, languageTypeData]);

  React.useEffect(() => {
    if (!userByTokenData && sessionStorage.getItem("token")) {
      dispatch(
        GetUserByJWTAuthToken({
          JwtAuthCode: sessionStorage.getItem("token"),
        })
      );
    }
  }, [dispatch, userByTokenData]);

  const changeLanguage = (lng) => {
    let url;
    if (lng === "EN") {
      url = location.pathname.replace("cz", "en");
    } else {
      url = location.pathname.replace("en", "cz");
    }
    sessionStorage.setItem("langType", lng);
    i18n.changeLanguage(lng);
    dispatch(GetApproachText({ vLangCode: lng || "EN" }));
    navigate(url, { state: location.state });
  };

  return (
    <header>
      <Link to={"/"} className="image-box">
        <img src="/../assets/images/logo.png" alt="" className="img-contain" />
      </Link>
      <ul>
        <li className="green-btn btn p-2">
          <span
            className="m-0"
            style={{ cursor: "pointer" }}
            onClick={() => navigate(`/${lang.toLowerCase()}/region`)}
          >
            {t("LBL_HEADER_MENU_MAP_SCHOOL")}
          </span>
        </li>
        <li className="lang-select">
          <span>{t("LBL_HEADER_LANG_TITLE")}:</span>
          <div className="image-container">
            {languageTypeData && (
              <img
                width="20"
                height="15"
                src={
                  languageTypeData.data.find(
                    (option) => option.vLangCode === lang
                  ).vLangImage
                }
                alt="flags"
              />
            )}
          </div>
          {/* <span>Page language:</span> */}
          <select
            className="form-select"
            value={lang}
            onChange={(e) => changeLanguage(e.target.value)}
            aria-label="Default select example"
          >
            {languageTypeData &&
              languageTypeData.code === "200" &&
              languageTypeData.data.map((lng, key) => (
                <option key={key} value={lng.vLangCode}>
                  {lng.vLangTitle}
                </option>
              ))}
            {/* <option defaultValue={"EN"} selected={lang === "EN" ? true : false}>
              English
            </option>
            <option value="cz" selected={lang === "CZ" ? true : false}>
              Czech
            </option> */}

            {/* <option value="uk">UK</option>
            <option value="ger">German</option> */}
          </select>
          {/* <svg
            xmlns="http://www.w3.org/2000/svg"
            width="39"
            height="38"
            viewBox="0 0 39 38"
            fill="none"
          >
            <path
              d="M14.0742 6.33325L13.9131 6.3441C13.3335 6.42273 12.8867 6.91956 12.8867 7.52075L12.8854 27.6164L7.78721 22.5219L7.65398 22.4069C7.18895 22.0622 6.52924 22.1009 6.10783 22.5225C5.64428 22.9866 5.6446 23.7384 6.10853 24.202L13.2395 31.327L13.3727 31.4419C13.8377 31.7866 14.4975 31.748 14.9189 31.3263L22.038 24.2013L22.1528 24.068C22.4976 23.603 22.459 22.9432 22.0372 22.5219L21.904 22.4069C21.439 22.0622 20.7792 22.1009 20.3578 22.5225L15.2604 27.6228L15.2617 7.52075L15.2509 7.35962C15.1723 6.77999 14.6754 6.33325 14.0742 6.33325ZM19.6145 7.12492C18.9587 7.12492 18.427 7.65659 18.427 8.31242C18.427 8.96825 18.9587 9.49992 19.6145 9.49992H32.2812C32.937 9.49992 33.4687 8.96825 33.4687 8.31242C33.4687 7.65659 32.937 7.12492 32.2812 7.12492H19.6145ZM18.427 13.0624C18.427 12.4066 18.9587 11.8749 19.6145 11.8749H27.5312C28.187 11.8749 28.7187 12.4066 28.7187 13.0624C28.7187 13.7183 28.187 14.2499 27.5312 14.2499H19.6145C18.9587 14.2499 18.427 13.7183 18.427 13.0624ZM19.6145 16.6249C18.9587 16.6249 18.427 17.1566 18.427 17.8124C18.427 18.4682 18.9587 18.9999 19.6145 18.9999H22.7812C23.437 18.9999 23.9687 18.4682 23.9687 17.8124C23.9687 17.1566 23.437 16.6249 22.7812 16.6249H19.6145Z"
              fill="#5182B2"
            />
          </svg> */}
        </li>
        <li
          className="menu"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasRightlogin"
          aria-controls="offcanvasRightlogin"
        >
          <span>{t("LBL_HEADER_MENU_TITLE")}</span>
          {/* <span>Menu</span> */}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="27"
            height="24"
            viewBox="0 0 27 24"
            fill="none"
          >
            <path
              d="M0.21875 0V3.326H26.8267V0H0.21875ZM0.21875 9.87821V13.2042H26.8267V9.87821H0.21875ZM0.21875 19.8562V23.1822H26.8267V19.8562H0.21875Z"
              fill="#5182B2"
            />
          </svg>
        </li>
      </ul>
    </header>
  );
}

export default Header;
