import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Toast } from "primereact/toast";
import { Dialog } from "primereact/dialog";

function SignUpOffCanvas() {
  const { t } = useTranslation();
  const [fbResponseData, setFbResponseData] = React.useState({});
  const [visible, setVisible] = React.useState(false);
  const toast = React.useRef(null);
  const singUpValues = {
    vFirstName: "",
    vLastName: "",
    vEmail: "",
    vPassword: "",
    vMobileNumber: "",
    eAccountType: "",
    vParentEmail: "",
  };
  const [singUpValue, setSignUpValue] = React.useState(singUpValues);
  const [emailExitsErrMsg, setEmailExitsErrMsg] = React.useState("");
  const [accountType, setAccountType] = React.useState("");
  const [lang, setLang] = React.useState("cz");
  const navigate = useNavigate();

  // here is phone regexp through match the numbers
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  // here is register basis schema
  const registerSchema = Yup.object().shape({
    vFirstName: Yup.string()
      .required(t("LBL_SINGUP_NAME_ERROR_MESSAGE"))
      .min(2, t("LBL_SINGUP_NAME_VALID_ERROR_MESSAGE")),
    vLastName: Yup.string()
      .required(t("LBL_SINGUP_SURNAME_ERROR_MESSAGE"))
      .min(2, t("LBL_SINGUP_SURNAME_VALID_ERROR_MESSAGE")),
    vEmail: Yup.string()
      .required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE"))
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE")),
    vParentEmail: Yup.string()
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE"))
      .when("eAccountType", {
        is: (v) => v === "Student",
        then: (schema) => schema.required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE")),
      }),
    vPassword: Yup.string()
      .required(t("LBL_SIGNUP_PASSWORD_ERROR_MESSAGE"))
      .min(8, t("LBL_SIGNUP_PASSWORD_LENGTH_ERROR_MESSAGE"))
      .matches(/[a-z]+/, t("LBL_SIGNUP_PASSWORD_LOWERCASE_ERROR_MESSAGE"))
      .matches(/[A-Z]+/, t("LBL_SIGNUP_PASSWORD_UPPARCASE_ERROR_MESSAGE"))
      .matches(
        /[@$!%*#?&]+/,
        t("LBL_SIGNUP_PASSWORD_SPECIALCASE_ERROR_MESSAGE")
      )
      .matches(/\d+/, t("LBL_SIGNUP_PASSWORD_NUMBERCASE_ERROR_MESSAGE")),
    vMobileNumber: Yup.string()
      .required(t("LBL_SIGNUP_PHONE_NO_ERROR_MESSAGE"))
      .matches(phoneRegExp, t("LBL_SIGNUP_PHONE_NO_VALID_ERROR_MESSAGE")),
    eAccountType: Yup.string().required(t("LBL_SIGNUP_ACCOUNT_TYPE_VALID_ERROR_MESSAGE")),
  });

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  // facebook signup logic
  React.useEffect(() => {
    window.fbAsyncInit = function () {
      window.FB.init({
        appId: "858535852725986",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v12.0", // Use the latest version of the Facebook Graph API
      });
    };

    // Load the SDK asynchronously
    (function (d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
  }, []);

  const successShow = (msg) => {
    toast.current.show({
      severity: "success",
      
      detail: msg,
      life: 3000,
    });
  };

  const warnShow = (msg) => {
    toast.current.show({
      severity: "warn",
      
      detail: msg,
      life: 3000,
    });
  };

  const handleSubmit = async (values, { resetForm }) => {
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/user/register`,
        {...values, vLangCode: lang},
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const { data } = res;
      if (data && data.code === "201") {
        successShow(data.message);
        setTimeout(() => {
          const element = document.getElementsByTagName("body");
          if (element && element.length > 0) {
            element[0].style.overflow = "unset";
            element[0].style.paddingRight = "0";
            element[0].removeAttribute("data-bs-overflow");
          }
          navigate("/thank-you");
        }, 3000);
        // document.getElementById("open-login-canvas-id").click();
        setEmailExitsErrMsg("");
      } else if (data && data.code === "404") {
        warnShow(data.message);
        setEmailExitsErrMsg(data.message);
        return;
      }
    } catch (err) {
      setEmailExitsErrMsg(err.response.data.message);
      return;
    }
    resetForm();
  };

  const handleGoogleLogin = async () => {
    const ClientID =
      "96632211137-634omjl3sf50s6dmtg6t1ohv3rh0o7oj.apps.googleusercontent.com";

    // const ClientSecret = "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";

    const googleAuthEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
    const redirectUri = `${process.env.REACT_APP_URL}/login`; // Set your redirect URI
    const responseType = "token";
    const scope =
      "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"; // Scopes you want to access

    const authUrl = `${googleAuthEndpoint}?client_id=${ClientID}&redirect_uri=${redirectUri}&response_type=${responseType}&scope=${scope}`;
    window.location.href = authUrl;
  };

  const handleFacebookLogin = () => {
    try {
      window.FB.login(
        function (response) {
          if (response.authResponse) {
            window.FB.api(
              "/me",
              { fields: "name, email" },
              async function (fbRes) {
                setFbResponseData(fbRes);
                try {
                  const authRes = await axios.post(
                    `${process.env.REACT_APP_API_URL}/api/user/social_authentication`,
                    {
                      vEmail: fbRes.email,
                      vLangCode: lang,
                      vFacebookId: fbRes.id,
                    },
                    {
                      headers: {
                        "Content-Type": "application/json",
                      },
                    }
                  );
                  if (authRes && authRes.status === 204) {
                    setVisible(true);
                    document.getElementById("offcanvasid")?.click();
                    setSignUpValue((pre) => ({
                      ...pre,
                      vFirstName: fbRes.name.split(" ").at(0),
                      vLastName: fbRes.name.split(" ").at(1),
                      vEmail: fbRes.email,
                    }));
                    return;
                  } else if (authRes.data.code === "200") {
                    sessionStorage.setItem("token", authRes.data.data.token);
                    setVisible(false);
                    let url = authRes.data.data.url.split("/");
                    url.splice(3, 0, lang);
                    window.location.href = url.join("/");
                    // window.location.href = authRes.data.data.url;
                  }
                } catch (err) {
                  console.log(err);
                }
              }
            );
          } else {
            console.log("User cancelled login or did not fully authorize.");
          }
        },
        { scope: "email" } // Request the 'email' permission
      );
    } catch (err) {
      console.log(err);
    }
  };

  //  for social singup submit form
  const handleSignUpSubmit = async (values, { resetForm }) => {
    if (fbResponseData && Object.keys(fbResponseData).length > 0) {
      try {
        const authRes = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/social_authentication`,
          {
            vEmail: fbResponseData.data.email,
            vGoogleId: fbResponseData.data.sub,
            vLangCode: lang,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        if (authRes.status === 204) {
          let data = {
            vEmail: fbResponseData.email,
            vFirstName: values.vFirstName,
            vLastName: values.vLastName,
            vFacebookId: fbResponseData.id,
            vParentEmail: values.vParentEmail,
            vMobileNumber: values.vMobileNumber,
            eAccountType: values.eAccountType,
            vLangCode: lang,
          };

          try {
            let res = await axios.post(
              `${process.env.REACT_APP_API_URL}/api/user/social_register`,
              data,
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
            if (res && res.data.code === "200") {
              sessionStorage.setItem("token", res.data.data.token);
              setVisible(false);
              let url = authRes.data.data.url.split("/");
              url.splice(3, 0, lang);
              window.location.href = url.join("/");
              // window.location.href = res.data.data.url;
            }
          } catch (err) {
            console.log(err);
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    resetForm();
  };

  return (
    <>
      <Toast ref={toast} />
      <div
        className="offcanvas offcanvas-end login-canvas sign-up-canvas"
        // data-bs-backdrop="static"
        tabIndex="-1"
        id="offcanvasRightsingup"
        aria-labelledby="offcanvasRightsingupLabel"
      >
        <div className="offcanvas-header">
          <div className="canvas-title" id="offcanvasRightLabel">
            {t("LBL_SINGUP_OFFCANVAS_TITLE")}
            {/* Sign Up and Start Learning! */}
          </div>
          <button
            type="button"
            className="btn-close text-reset"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
            id="offcanvasRightsingupId"
          ></button>
        </div>
        <div className="offcanvas-body">
          <Formik
            initialValues={singUpValues}
            validationSchema={registerSchema}
            onSubmit={handleSubmit}
          >
            {({ errors, touched, handleChange, resetForm }) => (
              <Form>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_NAME_LABEL")}</label>
                  {/* <label>Name</label> */}
                  <Field name="vFirstName" />
                  {errors.vFirstName && touched.vFirstName ? (
                    <div className="text-danger">{errors.vFirstName}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_SURNAME_LABEL")}</label>
                  {/* <label>vLastName</label> */}
                  <Field name="vLastName" />
                  {errors.vLastName && touched.vLastName ? (
                    <div className="text-danger">{errors.vLastName}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_EMAIL_LABEL")}</label>
                  {/* <label>vEmail</label> */}
                  <Field name="vEmail" type="email" />
                  {(errors.vEmail && touched.vEmail) ||
                  emailExitsErrMsg !== "" ? (
                    <div className="text-danger">
                      {errors.vEmail
                        ? errors.vEmail
                        : emailExitsErrMsg
                        ? emailExitsErrMsg
                        : ""}
                    </div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_PASSWORD_LABEL")}</label>
                  {/* <label>vPassword</label> */}
                  <Field name="vPassword" type="password" />
                  {errors.vPassword && touched.vPassword ? (
                    <div className="text-danger">{errors.vPassword}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_PHONE_NO_LABEL")}</label>
                  {/* <label>Telephone number</label> */}
                  <Field name="vMobileNumber" />
                  {errors.vMobileNumber && touched.vMobileNumber ? (
                    <div className="text-danger">{errors.vMobileNumber}</div>
                  ) : null}
                </div>

                <div className="form-group">
                  {/* <label>Telephone number</label> */}
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    {t("LBL_SINGUP_OFFCANVAS_PARENT_OR_STUDENT_TEXT")}?
                  </label>
                  <div className="d-flex flex-wrap ">
                    <div className="form-check align-items-center me-3">
                      <label
                        className="form-check-label  mb-0 me-2"
                        htmlFor="flexCheckDefault1"
                      >
                        {t("LBL_SINGUP_OFFCANVAS_PARENT_TEXT")}
                      </label>
                      <Field
                        type="radio"
                        name="eAccountType"
                        id="flexCheckDefault1"
                        value="Parent"
                        onChange={(e) => {
                          setAccountType("");
                          handleChange(e);
                        }}
                      />
                    </div>
                    <div className="form-check align-items-center">
                      <label
                        className="form-check-label mb-0 me-2"
                        htmlFor="flexCheckDefault2"
                      >
                        {t("LBL_SINGUP_OFFCANVAS_STUDENT_TEXT")}
                      </label>
                      <Field
                        type="radio"
                        name="eAccountType"
                        id="flexCheckDefault2"
                        value="Student"
                        onChange={(e) => {
                          setAccountType(e.target.value);
                          handleChange(e);
                        }}
                      />
                    </div>
                  </div>
                  {errors.eAccountType && touched.eAccountType ? (
                    <div className="text-danger">{errors.eAccountType}</div>
                  ) : null}
                </div>
                {accountType === "Student" ? (
                  <div className="form-group">
                    <label>
                      {t("LBL_SINGUP_OFFCANVAS_PARENT_EMAIL_LABEL")}
                    </label>
                    <Field name="vParentEmail" type="email" />
                    <div className="text-danger">
                      {errors.vParentEmail && touched.vParentEmail ? (
                        <div className="text-danger">{errors.vParentEmail}</div>
                      ) : null}
                    </div>
                  </div>
                ) : (
                  <></>
                )}

                <button type="submit" className="green-btn btn">
                  {t("LBL_SINGUP_OFFCANVAS_BUTTON")}
                  {/* SIGN UP */}
                </button>
                <p className="sign-up blue-text">
                  {t("LBL_SINGUP_OFFCANVAS_ALREADY_ACCOUNT_TEXT")}
                  {/* Already have an account? */}
                  <span
                    type="button"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasRightlogin"
                    aria-controls="offcanvasRightlogin"
                    id="open-login-canvas-id"
                    onClick={resetForm}
                  >
                    {t("LBL_SINGUP_OFFCANVAS_LOGIN_BUTTON")}
                    {/* Log In */}
                  </span>
                </p>
              </Form>
            )}
          </Formik>

          <p className="blue-text">
            {t("LBL_LOGIN_OFFCANVAS_BLUE_P_TEXT")}
            {/* or use one of these platforms */}
          </p>
          <ul className="signup-with">
            <li>
              <img
                onClick={handleGoogleLogin}
                src="/../assets/images/google login.png"
                alt=""
                className="img-contain"
              />
            </li>
            {/* <li>
              <img
                onClick={handleFacebookLogin}
                src="/../assets/images/fb logo.png"
                alt=""
                className="img-contain"
              />
            </li>
            <li>
              <img
                src="/../assets/images/twitter logo.png"
                alt=""
                className="img-contain"
              />
            </li> */}
          </ul>
        </div>
      </div>
      {/* social signup form */}
      <div className="card flex justify-content-center">
        <Dialog
          visible={visible}
          draggable={false}
          resizable={false}
          modal
          // footer={footerContent}
          style={{ width: "30rem" }}
          onHide={() => {
            setVisible(false);
            setAccountType("");
          }}
          header={<h4>{t("LBL_SINGUP_OFFCANVAS_MODAL_TITLE")}</h4>}
        >
          {/* <h4></h4> <br /> */}
          <Formik
            initialValues={singUpValue}
            validationSchema={registerSchema}
            onSubmit={handleSignUpSubmit}
          >
            {({ errors, touched, handleChange, values, resetForm }) => (
              <Form>
                <div className="row g-3">
                  <div className="col-12">
                    <div className="form-group">
                      <label>{t("LBL_SINGUP_OFFCANVAS_NAME_LABEL")}</label>
                      {/* <label>Name</label> */}
                      <Field
                        className="form-control"
                        name="vFirstName"
                        onChange={handleChange}
                        value={values.vFirstName}
                        // value={googleRes && googleRes.data.name.split(" ").at(0)}
                      />
                      {errors.vFirstName && touched.vFirstName ? (
                        <div className="text-danger">{errors.vFirstName}</div>
                      ) : null}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <label>{t("LBL_SINGUP_OFFCANVAS_SURNAME_LABEL")}</label>
                      {/* <label>vLastName</label> */}
                      <Field
                        className="form-control"
                        name="vLastName"
                        onChange={handleChange}
                        value={values.vLastName}
                      />
                      {errors.vLastName && touched.vLastName ? (
                        <div className="text-danger">{errors.vLastName}</div>
                      ) : null}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <label>{t("LBL_SINGUP_OFFCANVAS_EMAIL_LABEL")}</label>
                      {/* <label>vEmail</label> */}
                      <Field
                        name="vEmail"
                        onChange={handleChange}
                        value={values.vEmail}
                        type="email"
                        className="form-control"
                      />
                      {errors.vEmail && touched.vEmail ? (
                        <div className="text-danger">
                          {errors.vEmail ? errors.vEmail : ""}
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <label>{t("LBL_SINGUP_OFFCANVAS_PHONE_NO_LABEL")}</label>
                      {/* <label>Telephone number</label> */}
                      <Field name="vMobileNumber" className="form-control" />
                      {errors.vMobileNumber && touched.vMobileNumber ? (
                        <div className="text-danger">
                          {errors.vMobileNumber}
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      {/* <label>Telephone number</label> */}
                      <label
                        className="form-check-label"
                        htmlFor="flexCheckDefault"
                      >
                        {t("LBL_SINGUP_OFFCANVAS_PARENT_OR_STUDENT_TEXT")}?
                      </label>
                      <div className="d-flex flex-wrap ">
                        <div className="form-check align-items-center p-0">
                          <label
                            className="form-check-label  mb-0 me-2"
                            htmlFor="flexCheckDefault1"
                          >
                            {t("LBL_SINGUP_OFFCANVAS_PARENT_TEXT")}
                          </label>
                          <Field
                            type="radio"
                            name="eAccountType"
                            id="flexCheckDefault1"
                            value="Parent"
                            onChange={(e) => {
                              setAccountType("");
                              handleChange(e);
                            }}
                          />
                        </div>
                        <div className="form-check align-items-center">
                          <label
                            className="form-check-label mb-0 me-2"
                            htmlFor="flexCheckDefault2"
                          >
                            {t("LBL_SINGUP_OFFCANVAS_STUDENT_TEXT")}
                          </label>
                          <Field
                            type="radio"
                            name="eAccountType"
                            id="flexCheckDefault2"
                            value="Student"
                            onChange={(e) => {
                              setAccountType(e.target.value);
                              handleChange(e);
                            }}
                          />
                        </div>
                      </div>
                      {errors.eAccountType && touched.eAccountType ? (
                        <div className="text-danger">{errors.eAccountType}</div>
                      ) : null}
                    </div>
                  </div>
                  <div className="col-12">
                    {accountType === "Student" ? (
                      <div className="form-group">
                        <label>
                          {t("LBL_SINGUP_OFFCANVAS_PARENT_EMAIL_LABEL")}
                        </label>
                        <Field
                          name="vParentEmail"
                          type="email"
                          className="form-control"
                        />
                        <div className="text-danger">
                          {errors.vParentEmail && touched.vParentEmail ? (
                            <div className="text-danger">
                              {errors.vParentEmail}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="col-12">
                    <button type="submit" className="green-btn btn">
                      {t("LBL_SINGUP_OFFCANVAS_BUTTON")}
                      {/* SIGN UP */}
                    </button>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </Dialog>
      </div>
    </>
  );
}

export default SignUpOffCanvas;
