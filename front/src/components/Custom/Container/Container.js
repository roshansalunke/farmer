import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import SignInOffCanvas from "./SignInOffCanvas";
import SignUpOffCanvas from "./SignUpOffCanvas";
import AccountConnectionOffCanvas from "./AccountConnectionOffCanvas";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import ForgotPasswordOffCanvas from "./ForgotPasswordOffCanvas";
import SelectSchoolSetting from "./SelectSchoolSetting";
import SchoolChoiceOffCanvas from "../../KnowledgeVerification/SchoolChoiceOffCanvas";

function Container(props) {
  const url = window.location.pathname;
  const vJWTToken = url.split("/")[3];
  const location = window.location;
  const navigate = useNavigate();
  const [lang, setLang] = React.useState("cz");
  // const lang = sessionStorage.getItem("langType") || "cz"

  React.useEffect(() => {
    let lng = "en";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  const getAuthentication = React.useCallback(
    async (JWTSession, sessionURL) => {
      try {
        await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/check_token`,
          {
            url_token: JWTSession,
            session_token: sessionURL,
            vLangCode: lang,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        // if (res.status === 404) {
        //   navigate("/logout");
        // }
      } catch (err) {
        if (
          err &&
          err.response &&
          err.response.status &&
          err.response.status === 404
        ) {
          navigate("/logout");
        }
      }
    },
    [navigate, lang]
  );

  React.useEffect(() => {
    if (
      location.pathname ===
        `/${lang}/student-infinite-trees/${sessionStorage.getItem("token")}` ||
      location.pathname ===
        `/${lang}/student-intro-topic/${sessionStorage.getItem("token")}` ||
      location.pathname === `/${lang}/student-test`
    ) {
      let values = {};
      let screnType = location?.pathname.split("/")[2];
      if (screnType) {
        values = {
          ...values,
          vLangCode: lang,
          JwtAuthCode: sessionStorage.getItem("token"),
          iUserId: sessionStorage.getItem("iUserId"),
          eSubject: sessionStorage.getItem("eSubject"),
          eScreenType:
            screnType === "student-infinite-trees"
              ? "RoadMap"
              : screnType === "student-intro-topic"
              ? "Lesson Introduction"
              : "Lesson Test",
        };
      }
      setInterval(() => {
        (async function () {
          try {
            // user acitivity api call
            await axios.post(
              `${process.env.REACT_APP_API_URL}/api/user-log/createAndUpdateUserLog`,
              values,
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
          } catch (err) {
            console.log(err);
          }
        })();
      }, 60000);
    }
  }, [location, lang]);

  React.useEffect(() => {
    if (
      location.pathname === `/delete-account/${vJWTToken}` ||
      location.pathname === `/${lang}/knowledge-verification/${vJWTToken}` ||
      location.pathname === `/${lang}/parent-school-detail/${vJWTToken}` ||
      location.pathname === `/${lang}/parents-student-detail/${vJWTToken}` ||
      location.pathname === `/${lang}/settings/${vJWTToken}` ||
      location.pathname === `/${lang}/student-force-test/${vJWTToken}` ||
      location.pathname === `/${lang}/student-infinite-trees/${vJWTToken}` ||
      location.pathname === `/student-progress-ladders/${vJWTToken}` ||
      location.pathname === `/student-recap/${vJWTToken}` ||
      location.pathname === `/student-intro-topic/${vJWTToken}` ||
      location.pathname === `/student-lesson-done/${vJWTToken}` ||
      location.pathname === `/student-Test/${vJWTToken}` ||
      location.pathname === `/${lang}/student-auth-main/${vJWTToken}`
    ) {
      getAuthentication(vJWTToken, sessionStorage.getItem("token"));
    }
  }, [vJWTToken, getAuthentication, lang, navigate, location]);

  return (
    <div className="main-bg home-page">
      <div className="new-container">
        <Header />
        {props.children}
        <Footer />
        <SignInOffCanvas />
        <SignUpOffCanvas />
        <SelectSchoolSetting />
        <AccountConnectionOffCanvas />
        <SchoolChoiceOffCanvas />
        <ForgotPasswordOffCanvas />
      </div>
    </div>
  );
}

export default Container;
