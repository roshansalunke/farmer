import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import React from "react";
import { Dialog } from "primereact/dialog";
import { useDispatch, useSelector } from "react-redux";
// import { DistrictList } from "../../../store/action/Region";
// import { getAllSchoolAndPercentage } from "../../../store/action/SchoolStatistics";
import { RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA } from "../../../store/constants/schoolStatistics";
import { getAllSchoolAndPercentage } from "../../../store/action/SchoolStatistics";

const PrahaMapPath = ({ visible, setVisible }) => {
  const { districtList } = useSelector((state) => state.region);
  const [cardPositions, setCardPositions] = React.useState({
    Left: 0,
    Top: 0,
  });
  const [state, setState] = React.useState("");
  const [districtId, setDistrictId] = React.useState(null);
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("cz");
  //   const { districtList } = useSelector((state) => state.region);
  const { schoolPercentageData } = useSelector(
    (state) => state.schoolStatistic
  );

  const dispatch = useDispatch();

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  //   React.useEffect(() => {
  //     if (!districtList) {
  //       dispatch(DistrictList());
  //     }
  //   }, [districtList, dispatch]);

  React.useEffect(() => {
    if (districtId) {
      dispatch(getAllSchoolAndPercentage({ iDistrictId: districtId }));
    }
  }, [districtId, dispatch]);

  const handleClose = (e) => {
    dispatch({ type: RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA });
    setDistrictId(null);
    setState("");
    setCardPositions({
      Left: 0,
      Top: 0,
    });
  };

  const handleClick = React.useCallback(
    (e, state, distId) => {
      const { offsetX, offsetY } = e.nativeEvent;
      console.log(offsetX, offsetY);
      setCardPositions({
        Left: offsetX,
        Top: offsetY,
      });
      dispatch({ type: RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA });
      // setAddClass("region-card-bg");
      setCardPositions({
        Left: offsetX,
        Top: offsetY,
      });
      dispatch({ type: RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA });
      setState(state);
      setDistrictId(distId);
    },
    [dispatch]
  );

  return (
    <Dialog
      header="Praha"
      visible={visible}
      style={{ width: "50vw" }}
      onHide={() => setVisible(false)}
      className="map-modal"
    >
      <div className="map-box ">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          id="Layer_1"
          x="0px"
          y="0px"
          viewBox="0 0 1080 769.4"
        >
          <g>
            <path
              onClick={(e) => handleClick(e, 1, 1)}
              style={{
                fill: "transparent",
                stroke: "black",
              }}
              className="st0"
              d="M653.4,79.9l3.5,2.2"
            />
            {districtList !== undefined &&
              districtList.code === "200" &&
              districtList.data.length > 0 &&
              districtList.data.map((mapData, index) =>
                mapData.eStatus === "Inactive" ? (
                  <path
                    key={index}
                    id={mapData.iDistrictId}
                    onClick={(e) =>
                      handleClick(
                        e,
                        mapData.vDistrictTitle,
                        mapData.iDistrictId
                      )
                    }
                    style={{
                      fill: mapData.vColorCode,
                      // state === mapData.vDistrictTitle
                      //   ? mapData.vColorCode
                      //   : "transparent",
                      stroke: "black",
                    }}
                    className="cls-1"
                    d={mapData.tSVGCoordinates}
                  />
                ) : (
                  <React.Fragment key={index}></React.Fragment>
                )
              )}
          </g>
        </svg>
        {cardPositions && cardPositions.Left > 0 && cardPositions.Top > 0 ? (
          <div
            className="card"
            style={{
              position: "absolute",
              left: `${cardPositions.Left}px`,
              top: `${cardPositions.Top}px`,
            }}
          >
            <div className="close-btn" onClick={handleClose}>
              <i className="fal fa-times"></i>
            </div>
            {/* <h3> {t("LBL_CHOOSE_REGION_CARD_TITLE")}</h3> */}
            <h3>{state}</h3>
            <ul className="detail">
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                >
                  <g clipPath="url(#clip0_204_892)">
                    <path
                      d="M13.5562 3.32087L10.543 0.470187C10.2225 0.166969 9.80297 0 9.36178 0H3.625C2.67728 0 1.90625 0.771031 1.90625 1.71875V14.2812C1.90625 15.229 2.67728 16 3.625 16H12.375C13.3227 16 14.0938 15.229 14.0938 14.2812V4.56944C14.0938 4.09916 13.8978 3.64406 13.5562 3.32087ZM12.6457 3.75H10.3125C10.2263 3.75 10.1562 3.67991 10.1562 3.59375V1.39488L12.6457 3.75ZM12.375 15.0625H3.625C3.19422 15.0625 2.84375 14.712 2.84375 14.2812V1.71875C2.84375 1.28797 3.19422 0.9375 3.625 0.9375H9.21875V3.59375C9.21875 4.19684 9.70941 4.6875 10.3125 4.6875H13.1562V14.2812C13.1562 14.712 12.8058 15.0625 12.375 15.0625Z"
                      fill="#6A7A99"
                    />
                    <path
                      d="M11.3438 6.25H4.46875C4.20987 6.25 4 6.45987 4 6.71875C4 6.97763 4.20987 7.1875 4.46875 7.1875H11.3438C11.6026 7.1875 11.8125 6.97763 11.8125 6.71875C11.8125 6.45987 11.6026 6.25 11.3438 6.25Z"
                      fill="#6A7A99"
                    />
                    <path
                      d="M11.3438 8.75H4.46875C4.20987 8.75 4 8.95987 4 9.21875C4 9.47763 4.20987 9.6875 4.46875 9.6875H11.3438C11.6026 9.6875 11.8125 9.47763 11.8125 9.21875C11.8125 8.95987 11.6026 8.75 11.3438 8.75Z"
                      fill="#6A7A99"
                    />
                    <path
                      d="M6.74125 11.25H4.46875C4.20987 11.25 4 11.4599 4 11.7188C4 11.9776 4.20987 12.1875 4.46875 12.1875H6.74125C7.00012 12.1875 7.21 11.9776 7.21 11.7188C7.21 11.4599 7.00012 11.25 6.74125 11.25Z"
                      fill="#6A7A99"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_204_892">
                      <rect width="16" height="16" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                {/* <span>{t("LBL_CHOOSE_REGION_DETAIL_LI_1")}</span> */}
                <span>
                  {schoolPercentageData !== undefined &&
                  schoolPercentageData.code === "200"
                    ? schoolPercentageData.data.school_count
                    : " 0 "}
                  &nbsp;{t("LBL_MAP_REASON_SCHOOL_TITLE")}
                  {/* 50 &nbsp;schools */}
                </span>
              </li>
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                >
                  <g clipPath="url(#clip0_204_899)">
                    <path
                      d="M8 0C3.58891 0 0 3.58872 0 8C0 12.4113 3.58891 16 8 16C12.4113 16 16 12.4113 16 8C16 3.58872 12.4111 0 8 0ZM8 14.8321C4.23281 14.8321 1.16788 11.7672 1.16788 8C1.16788 4.23281 4.23281 1.16788 8 1.16788C11.7672 1.16788 14.8321 4.23278 14.8321 7.99981C14.8321 11.7672 11.7672 14.8321 8 14.8321Z"
                      fill="#6A7A99"
                    />
                    <path
                      d="M10.7251 7.99995H8.19472V4.49629C8.19472 4.17376 7.93331 3.91235 7.61078 3.91235C7.28825 3.91235 7.02684 4.17376 7.02684 4.49629V8.58389C7.02684 8.90642 7.28825 9.16782 7.61078 9.16782H10.7251C11.0477 9.16782 11.3091 8.90642 11.3091 8.58389C11.3091 8.26135 11.0477 7.99995 10.7251 7.99995Z"
                      fill="#6A7A99"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_204_899">
                      <rect width="16" height="16" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                <span>
                  {schoolPercentageData !== undefined &&
                  schoolPercentageData.code === "200" &&
                  schoolPercentageData.data &&
                  schoolPercentageData.data.school_avg
                    ? parseInt(schoolPercentageData.data.school_avg)?.toFixed(2)
                    : "0"}
                  {"%"}
                  {/* 12 */}
                </span>
              </li>
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  fill="none"
                >
                  <g clipPath="url(#clip0_204_908)">
                    <path
                      d="M2.15506 9.18042C0.966781 9.18042 0 10.1472 0 11.3355V13.8448C0 15.0331 0.966781 15.9999 2.15509 15.9999C3.34341 15.9999 4.31016 15.0331 4.31016 13.8448V11.3355C4.31016 10.1472 3.34337 9.18042 2.15506 9.18042ZM3.06013 13.8448C3.06013 14.3439 2.65412 14.7499 2.15506 14.7499C1.65603 14.7499 1.25 14.3439 1.25 13.8448V11.3355C1.25 10.8364 1.65603 10.4304 2.15509 10.4304C2.65416 10.4304 3.06016 10.8365 3.06013 11.3355V13.8448Z"
                      fill="#6A7A99"
                    />
                    <path
                      d="M7.99989 0C6.81158 0 5.8448 0.966781 5.8448 2.15509V13.8449C5.8448 15.0332 6.81158 16 7.99989 16C9.1882 16 10.155 15.0332 10.155 13.8449V2.15509C10.155 0.966781 9.1882 0 7.99989 0ZM8.90498 13.8449C8.90498 14.344 8.49895 14.75 7.99989 14.75C7.50083 14.75 7.0948 14.344 7.0948 13.8449V2.15509C7.0948 1.656 7.50083 1.25 7.99989 1.25C8.49895 1.25 8.90498 1.656 8.90498 2.15509V13.8449Z"
                      fill="#6A7A99"
                    />
                    <path
                      d="M13.8447 4.59033C12.6564 4.59033 11.6897 5.55711 11.6897 6.74543V13.845C11.6897 15.0333 12.6564 16.0001 13.8448 16.0001C15.033 16.0001 15.9998 15.0333 15.9998 13.845V6.74543C15.9998 5.55711 15.033 4.59033 13.8447 4.59033ZM14.7498 13.845C14.7498 14.3441 14.3438 14.7501 13.8447 14.7501C13.3457 14.7501 12.9397 14.3441 12.9397 13.845V6.74543C12.9397 6.24633 13.3457 5.84033 13.8448 5.84033C14.3438 5.84033 14.7498 6.24633 14.7498 6.74543V13.845Z"
                      fill="#6A7A99"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_204_908">
                      <rect width="16" height="16" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                {t("LBL_CHOOSE_REGION_DETAIL_LI_3")}
                <span></span>
              </li>
            </ul>
            <ul className="list-detail">
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                >
                  <circle cx="10.5" cy="10" r="9.5" stroke="#DDDDDD" />
                  <path
                    d="M13.3243 9.02505L10.159 12.1901C9.92461 12.4245 9.54439 12.4245 9.30978 12.1901L7.67586 10.556C7.44138 10.3216 7.44138 9.94133 7.67586 9.70685C7.91038 9.47233 8.29057 9.47233 8.52499 9.70677L9.73452 10.9163L12.475 8.17583C12.7095 7.9413 13.0897 7.94148 13.3242 8.17583C13.5586 8.41031 13.5586 8.79043 13.3243 9.02505Z"
                    fill="#6A7A99"
                  />
                </svg>
                <span> {t("LBL_CHOOSE_REGION_DETAIL_LI_4")}</span>
              </li>
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                >
                  <circle cx="10.5" cy="10" r="9.5" stroke="#DDDDDD" />
                  <path
                    d="M13.3243 9.02505L10.159 12.1901C9.92461 12.4245 9.54439 12.4245 9.30978 12.1901L7.67586 10.556C7.44138 10.3216 7.44138 9.94133 7.67586 9.70685C7.91038 9.47233 8.29057 9.47233 8.52499 9.70677L9.73452 10.9163L12.475 8.17583C12.7095 7.9413 13.0897 7.94148 13.3242 8.17583C13.5586 8.41031 13.5586 8.79043 13.3243 9.02505Z"
                    fill="#6A7A99"
                  />
                </svg>
                <span> {t("LBL_CHOOSE_REGION_DETAIL_LI_5")}</span>
              </li>
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                >
                  <circle cx="10.5" cy="10" r="9.5" stroke="#DDDDDD" />
                  <path
                    d="M13.3243 9.02505L10.159 12.1901C9.92461 12.4245 9.54439 12.4245 9.30978 12.1901L7.67586 10.556C7.44138 10.3216 7.44138 9.94133 7.67586 9.70685C7.91038 9.47233 8.29057 9.47233 8.52499 9.70677L9.73452 10.9163L12.475 8.17583C12.7095 7.9413 13.0897 7.94148 13.3242 8.17583C13.5586 8.41031 13.5586 8.79043 13.3243 9.02505Z"
                    fill="#6A7A99"
                  />
                </svg>
                <span> {t("LBL_CHOOSE_REGION_DETAIL_LI_6")}</span>
              </li>
              <li>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="21"
                  height="20"
                  viewBox="0 0 21 20"
                  fill="none"
                >
                  <circle cx="10.5" cy="10" r="9.5" stroke="#DDDDDD" />
                  <path
                    d="M13.3243 9.02505L10.159 12.1901C9.92461 12.4245 9.54439 12.4245 9.30978 12.1901L7.67586 10.556C7.44138 10.3216 7.44138 9.94133 7.67586 9.70685C7.91038 9.47233 8.29057 9.47233 8.52499 9.70677L9.73452 10.9163L12.475 8.17583C12.7095 7.9413 13.0897 7.94148 13.3242 8.17583C13.5586 8.41031 13.5586 8.79043 13.3243 9.02505Z"
                    fill="#6A7A99"
                  />
                </svg>
                <span> {t("LBL_CHOOSE_REGION_DETAIL_LI_7")}</span>
              </li>
            </ul>

            <Link
              to={`/${lang}/subdivision`}
              state={{ districtId }}
              className="btn pink-btn"
            >
              {t("LBL_CHOOSE_REGION_DETAIL_LI_8")}
            </Link>
          </div>
        ) : (
          <></>
        )}
      </div>
    </Dialog>
  );
};

export default PrahaMapPath;
