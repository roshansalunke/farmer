import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import {
  GetElementarySchoolDifference,
  GetHighSchoolDifference,
} from "../../store/action/ParentSchoolDetails";
import CanvasJSReact from "@canvasjs/react-charts";

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function SchoolAnalysisTab() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { getElemAndHighSchoolData } = useSelector(
    (state) => state.parentSchoolDetails
  );
  const { getDiffElementarySchoolData, getDiffHighSchoolData } = useSelector(
    (state) => state.parentSchoolDetails
  );
  const { state } = useLocation();
  const [barchartFilter, setBarchartFilter] = React.useState("avg");
  // const avg_chart_details =
  const [barchartOptions, setBarchartOptions] = React.useState({});
  const [groupBarchartOptions, setGroupBarchartOptions] = React.useState({});

  React.useEffect(() => {
    if (
      getElemAndHighSchoolData &&
      getElemAndHighSchoolData.code === "200" &&
      getElemAndHighSchoolData.data &&
      getElemAndHighSchoolData.data.length > 0 &&
      state &&
      state.iUserId
    ) {
      const user_data = getElemAndHighSchoolData.data.find(
        (item) => item.iUserId === state.iUserId
      );
      if (user_data && Object.keys(user_data).length > 0) {
        dispatch(
          GetElementarySchoolDifference({
            iSchoolId: user_data.iElementarySchoolId,
            iUserId: user_data.iParentId,
          })
        );
        dispatch(GetHighSchoolDifference({ iUserId: user_data.iParentId }));
      }
    }
  }, [dispatch, getElemAndHighSchoolData, state]);

  React.useEffect(() => {
    if (
      getDiffElementarySchoolData &&
      getDiffElementarySchoolData.code === "200" &&
      getDiffElementarySchoolData.data &&
      Object.keys(getDiffElementarySchoolData.data).length > 0 &&
      getDiffHighSchoolData &&
      getDiffHighSchoolData.code === "200" &&
      getDiffHighSchoolData.data &&
      getDiffHighSchoolData.data.length > 0
    ) {
      const avg = parseFloat(getDiffElementarySchoolData.data.avg);
      let ele_c_per = 0;
      let ele_m_per = 0;
      if (
        getDiffElementarySchoolData.data.school &&
        getDiffElementarySchoolData.data.school.school_results
      ) {
        const ele_s =
          getDiffElementarySchoolData.data.school.school_results.find(
            (item) => item.eSubject === "C"
          );
        if (Object.keys(ele_s).length > 0 && ele_s.dPercentage) {
          ele_c_per = parseFloat(ele_s.dPercentage);
        }
      }

      if (
        getDiffElementarySchoolData.data.school &&
        getDiffElementarySchoolData.data.school.school_results
      ) {
        const ele_m =
          getDiffElementarySchoolData.data.school.school_results.find(
            (item) => item.eSubject === "M"
          );
        if (Object.keys(ele_m).length > 0 && ele_m.dPercentage) {
          ele_m_per = parseFloat(ele_m.dPercentage);
        }
      }

      const barchart_options = [];
      const x_options = [];
      const y_options = [];

      for (let i = 0; i < getDiffHighSchoolData.data.length; i++) {
        const high_school = getDiffHighSchoolData.data[i];
        const point =
          avg -
          (parseFloat(high_school.HighSchoolData.cj_proc) +
            parseFloat(high_school.HighSchoolData.ma_proc)) /
            2;
        if (barchartFilter === "avg") {
          barchart_options.push({
            indexLabelPlacement: "auto",
            indexLabel:
              point < 0
                ? `-${Math.abs(point)}%`
                : `${point}%`,
            label: "HS-" + (i + 1),
            y: point < 0 ? Math.abs(point) : point,
            color: point < 0 ? "#e61c63" : "#b1d724",
            indexLabelFontSize: 12
          });
        } else if (barchartFilter === "cz") {
          const point =
            ele_c_per - parseFloat(high_school.HighSchoolData.cj_proc);
          barchart_options.push({
            indexLabelPlacement: "auto",
            indexLabel:
              point < 0
                ? `-${Math.abs(Math.ceil(point))}%`
                : `${Math.ceil(point)}%`,
            label: "HS-" + (i + 1),
            y: point < 0 ? Math.abs(Math.ceil(point)) : Math.ceil(point),
            color: point < 0 ? "#e61c63" : "#b1d724",
            indexLabelFontSize: 12
          });
        } else if (barchartFilter === "math") {
          const point =
            ele_m_per - parseFloat(high_school.HighSchoolData.ma_proc);
          barchart_options.push({
            indexLabelPlacement: "auto",
            indexLabel:
              point < 0
                ? `-${Math.abs(Math.ceil(point))}%`
                : `${Math.ceil(point)}%`,
            label: "HS-" + (i + 1),
            y: point < 0 ? Math.abs(Math.ceil(point)) : Math.ceil(point),
            color: point < 0 ? "#e61c63" : "#b1d724",
            indexLabelFontSize: 12
          });
        }
        x_options.push({
          label: "HS-" + (i + 1),
          y: parseInt(high_school.HighSchoolData.ma_proc),
          indexLabel: high_school.HighSchoolData.ma_proc + "%",
          indexLabelPlacement: "inside",
          indexLabelFontSize: 12
        });
        y_options.push({
          label: "HS-" + (i + 1),
          y: parseInt(high_school.HighSchoolData.cj_proc),
          indexLabel: high_school.HighSchoolData.cj_proc + "%",
          indexLabelPlacement: "inside",
          indexLabelFontSize: 12
        });
      }
      setGroupBarchartOptions({
        animationEnabled: true,
        toolTip:{
          enabled: false,
        },
        title: {
          // text: "Operating Expenses of ACME",
          // fontFamily: "verdana",
        },
        axisX: {
          lineThickness: 0,
          tickThickness: 0,
          gridColor: "transparent",
        },
        axisY: {
          labelFormatter: function (e) {
            return "";
          },
          lineThickness: 0,
          gridThickness: 0,
          tickLength: 0,
          gridColor: "transparent",
          labelBackgroundColor: "transparent",
        },
        // toolTip: {
        //   shared: true,
        //   reversed: true,
        // },
        legend: {
          verticalAlign: "center",
          horizontalAlign: "right",
          reversed: true,
          cursor: "pointer",
        },

        // width: "100%",
        // height:300,
        data: [
          {
            type: "stackedColumn",
            name: "Mathematics",
            dataPoints: x_options,
          },
          {
            type: "stackedColumn",
            name: "Czech language",
            // showInLegend: true,
            // yValueFormatString: "#,###k",
            dataPoints: y_options,
          },
        ],
      });
      setBarchartOptions({
        animationEnabled: true,
        toolTip:{
          enabled: false,
        },
        theme: "light2", // "light1", "dark1", "dark2"
        title: {
          // text: "Bounce Rate by  of Year",
          fontSize: 50,
        },
        axisY: {
          labelFormatter: function (e) {
            return "";
          },
          lineThickness: 0,
          gridThickness: 0,
          tickLength: 0,
          gridColor: "transparent",
          labelBackgroundColor: "transparent",
        },
        axisX: {
          lineThickness: 0,
          tickThickness: 0,
          gridColor: "transparent",
        },

        data: [
          {
            type: "column",
            visible: true,
            toolTipContent: "{y}%",
            dataPoints: barchart_options,
          },
        ],
      });
    }
  }, [getDiffElementarySchoolData, getDiffHighSchoolData, barchartFilter]);

  return (
    <div
      className="tab-pane fade school-analy-sec"
      id="school-analysis-tab-pane"
      role="tabpanel"
      aria-labelledby="school-analysis-tab"
      tabIndex="0"
    >
      <ul className="breadcumb">
        <li onClick={() => setBarchartFilter("avg")}>{t("LBL_ALL_TEXT")}</li>
        <li onClick={() => setBarchartFilter("cz")}>
          <span>/</span>
          {t("LBL_CZECH_TEXT")}
        </li>
        <li onClick={() => setBarchartFilter("math")}>
          <span>/</span>
          {t("LBL_MATH_TEXT")}
        </li>
      </ul>
      <div className="activity-box chart-main-flex">
        <div className="inner-box">
          <div className="box">
            <h2 className="small-title">
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SCHOOL_ANALYSIS_TAB_H2_2")}
            </h2>
            {/* <h2 className="small-title">High school comparison</h2> */}
            <div className="chart-image h-100">
              <CanvasJSChart options={groupBarchartOptions} />
              {/* <img
                src="/assets/images/high-school-compare.png"
                alt=""
                className="img-contain"
              /> */}
            </div>
          </div>
        </div>
        <div className="inner-box">
          <div className="box">
            <h2 className="small-title">
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SCHOOL_ANALYSIS_TAB_H2_4")}

              {/* Where preparation is most difficult */}
            </h2>
            <div className="chart-image h-100">
              <CanvasJSChart options={barchartOptions} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SchoolAnalysisTab;
