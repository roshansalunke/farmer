import React from "react";
import ActivityTab from "./ActivityTab";
// import EducationMapTab from "./EducationMapTab";
// import ProbabilityOfSuccessTab from "./ProbabilityOfSuccessTab";
import SchoolAnalysisTab from "./ SchoolAnalysisTab";
import SettingsTab from "./SettingsTab";

function TabsContent() {
  return (
    <div className="tab-content" id="myTabContent">
      <ActivityTab />
      {/* <EducationMapTab /> */}
      {/* <ProbabilityOfSuccessTab /> */}
      <SchoolAnalysisTab />
      <SettingsTab />
    </div>
  );
}

export default TabsContent;
