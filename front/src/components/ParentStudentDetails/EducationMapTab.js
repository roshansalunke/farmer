import React from "react";
import { useTranslation } from "react-i18next";

function EducationMapTab() {
  const { t } = useTranslation();
  return (
    <div
      className="tab-pane fade"
      id="education-map-tab-pane"
      role="tabpanel"
      aria-labelledby="education-map-tab"
      tabIndex="0"
    >
      <div className="education-map-box">
        <h2 className="sub-title">
          {t("LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_2")}
        </h2>
        {/* <h2 className="sub-title">Education map</h2> */}
        <div className="chart-image-small h-auto">
          <div className="chart-image-box">
            <img
              src="/assets/images/cestina.png"
              alt=""
              className="img-cotain"
            />
          </div>
          <div className="chart-image-box">
            <img
              src="/assets/images/matematika.png"
              alt=""
              className="img-cotain"
            />
          </div>
        </div>
        <div className="in-sec">
          <div className="bar-image">
            <img src="/assets/images/Bar.png" alt="" />
          </div>
          <div className="owl-image">
            <p>
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_EDUCATION_MAP_TAB_P")}
              {/* text for owl, specific for this page, because here we
                    are starting with different point of view */}
            </p>
            <img
              src="/assets/images/sova_new_clr 1.png"
              alt=""
              className="img-contain"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default EducationMapTab;
