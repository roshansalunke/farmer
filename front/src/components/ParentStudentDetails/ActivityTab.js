import axios from "axios";
import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { GetParentChildList } from "../../store/action/ParentSchoolDetails";
import { GetParentChildList as childList } from "../../store/action/shoppingCartActions";
import { Toast } from "primereact/toast";
import { confirmDialog } from "primereact/confirmdialog";

function ActivityTab() {
  const { t } = useTranslation();
  const { state } = useLocation();
  const dispatch = useDispatch();
  const toast = React.useRef(null);
  const [activityData, setActivityData] = React.useState(null);
  const { userByTokenData } = useSelector((state) => state.user);
  const [childId, setChildId] = React.useState(0);
  const { parentChildListData } = useSelector(
    (state) => state.parentSchoolDetails
  );

  React.useEffect(() => {
    if (
      !parentChildListData &&
      userByTokenData &&
      userByTokenData.code === "200"
    ) {
      dispatch(GetParentChildList({ iUserId: userByTokenData.data.iUserId }));
    }
  }, [userByTokenData, dispatch, parentChildListData]);

  React.useEffect(() => {
    try {
      let value = {};
      if (state && state.iUserId) {
        value = {
          iUserId: state.iUserId,
          vLangCode: sessionStorage.getItem("langType") || "CZ",
        };
        (async function () {
          const resData = await axios.post(
            `${process.env.REACT_APP_API_URL}/api/user-log/get-daily-and-weekly-minutes`,
            value,
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          if (resData && resData.data && resData.data.code === "200") {
            setActivityData(resData.data.data);
          }
        })();
      }
    } catch (err) {
      console.log(err);
    }
  }, [userByTokenData, state]);

  const handleRemoveChild = (cId) => {
    try {
      let values = {};
      confirmDialog({
        message: t("LBL_DELETE_CHILD_WARN_MESSAGE"),
        className: "logout-popup",
        // header: t("LBL_CONFIRMATION"),
        icon: "pi pi-exclamation-triangle",
        accept: async () => {
          if (userByTokenData && userByTokenData.code === "200") {
            values = {
              iUserId: cId,
              iParentId: userByTokenData.data.iUserId,
              vLangCode: sessionStorage.getItem("langType") || "CZ",
            };
            // api logic
            const res = await axios.post(
              `${process.env.REACT_APP_API_URL}/api/user/delete-parent-and-student-data`,
              values,
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
            if (res.data.code === "200") {
              dispatch(
                GetParentChildList({ iUserId: userByTokenData.data.iUserId })
              );
              dispatch(childList({ iParentId: userByTokenData.data.iUserId }));
            }
          }
        },
        reject: () => {},
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Toast ref={toast} />
      <div
        className="tab-pane fade show active"
        id="activity-tab-pane"
        role="tabpanel"
        aria-labelledby="activity-tab"
        tabIndex="0"
      >
        <div className="activity-box">
          <div className="inner-box">
            <div className="box">
              <h2 className="small-title">
                {t("LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_1")}
              </h2>
              {/* <h2 className="small-title">Activity</h2> */}
              <div className="table-box table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th></th>
                      <th>
                        {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TH_1")}
                      </th>
                      <th>
                        {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TH_2")}
                      </th>
                      {/* <th>Czech</th>
                          <th>Math</th> */}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TD_1")}
                      </td>
                      {/* <td>Daily minutes</td> */}

                      {activityData !== null && (
                        <>
                          <td>
                            {Math.round(activityData?.daily_minutes_C) || 0}
                          </td>
                          <td>
                            {Math.round(activityData?.daily_minutes_M) || 0}
                          </td>
                        </>
                      )}
                    </tr>
                    <tr>
                      <td>
                        {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TD_2")}
                      </td>
                      {/* <td>Daily lessons</td> */}
                      <td>{Math.round(activityData?.daily_lessons_C) || 0}</td>
                      <td>{Math.round(activityData?.daily_lessons_M) || 0}</td>
                    </tr>
                    <tr>
                      <td>
                        {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TD_3")}
                      </td>
                      {/* <td>Weekly minutes</td> */}
                      {activityData !== null && (
                        <>
                          <td>
                            {Math.round(activityData?.weekly_minutes_C) || 0}
                          </td>
                          <td>
                            {Math.round(activityData?.weekly_minutes_M) || 0}
                          </td>
                        </>
                      )}
                    </tr>
                    {/* <tr>
                    <td>
                      {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TD_4")}
                    </td>
                    <td className="green-color">50% (+8%)</td>
                    <td className="pink-color">44% (- 6%)</td>
                  </tr> */}
                  </tbody>
                </table>
              </div>
            </div>
            {/* <div className="box">
            <h2 className="small-title">
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_ANALYSIS_TEXT")}
            </h2>
            <div className="chart-image-small">
              <img src="/assets/images/Analysis-targets-graphs.png" alt="" />
            </div>
          </div> */}
          </div>
          <div className="inner-box">
            <div className="box">
              <h2 className="small-title">
                {t("LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_H2")}
                {userByTokenData &&
                userByTokenData.code === "200" &&
                userByTokenData.data ? (
                  `${userByTokenData.data.vFirstName || ""} ${
                    userByTokenData.data.vLastName || ""
                  }`
                ) : (
                  <></>
                )}
                {/* Who can see account: Cody Fischer */}
              </h2>
              <div className="table-box table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th>
                        {userByTokenData &&
                        userByTokenData.code === "200" &&
                        userByTokenData.data ? (
                          `${userByTokenData.data.vFirstName || ""} ${
                            userByTokenData.data.vLastName || ""
                          }`
                        ) : (
                          <></>
                        )}
                        &nbsp;
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TH_1"
                        )}
                      </th>
                      {/* <th>Amerigo Vespucci (me)</th> */}
                      {/* <th></th>
                          <th></th> */}
                    </tr>
                  </thead>
                  <tbody>
                    {parentChildListData &&
                      parentChildListData.code === "200" &&
                      parentChildListData.data &&
                      parentChildListData.data.length > 0 &&
                      parentChildListData.data.map((child, key) =>
                        child.iUserId ===
                        parseInt(sessionStorage.getItem("iUserId")) ? (
                          <React.Fragment key={key}></React.Fragment>
                        ) : (
                          <tr key={key}>
                            <td>{`${child.vFirstName} ${child.vLastName}`}</td>
                            <td onClick={() => setChildId(child.iUserId)}>
                              {childId === child.iUserId ? (
                                <p className="email-show show">
                                  {child.vEmail}
                                </p>
                              ) : (
                                <p className="eye-icon show">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="32"
                                    height="24"
                                    viewBox="0 0 32 24"
                                    fill="none"
                                  >
                                    <path
                                      d="M16.12 0C6 0 0 12 0 12C0 12 6 24 16.12 24C26 24 32 12 32 12C32 12 26 0 16.12 0ZM16 4C20.44 4 24 7.6 24 12C24 16.44 20.44 20 16 20C11.6 20 8 16.44 8 12C8 7.6 11.6 4 16 4ZM16 8C13.8 8 12 9.8 12 12C12 14.2 13.8 16 16 16C18.2 16 20 14.2 20 12C20 11.6 19.84 11.24 19.76 10.88C19.44 11.52 18.8 12 18 12C16.88 12 16 11.12 16 10C16 9.2 16.48 8.56 17.12 8.24C16.76 8.12 16.4 8 16 8Z"
                                      fill="#5182B2"
                                      fillOpacity="0.5"
                                    ></path>
                                  </svg>
                                </p>
                              )}
                            </td>
                            {userByTokenData &&
                            userByTokenData.code === "200" &&
                            userByTokenData.data &&
                            userByTokenData.data.eAccountType === "Parent" ? (
                              <td
                                onClick={() => handleRemoveChild(child.iUserId)}
                              >
                                <span className="cursor-pointer text-danger">
                                  {t(
                                    "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_4_COMMON"
                                  )}
                                </span>
                              </td>
                            ) : (
                              <></>
                            )}
                          </tr>
                        )
                      )}
                    {/* <tr>
                    <td>
                      {t(
                        "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_2"
                      )}
                    </td>
               
                    <td>
                      <span className="eye-icon show">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="32"
                          height="24"
                          viewBox="0 0 32 24"
                          fill="none"
                        >
                          <path
                            d="M16.12 0C6 0 0 12 0 12C0 12 6 24 16.12 24C26 24 32 12 32 12C32 12 26 0 16.12 0ZM16 4C20.44 4 24 7.6 24 12C24 16.44 20.44 20 16 20C11.6 20 8 16.44 8 12C8 7.6 11.6 4 16 4ZM16 8C13.8 8 12 9.8 12 12C12 14.2 13.8 16 16 16C18.2 16 20 14.2 20 12C20 11.6 19.84 11.24 19.76 10.88C19.44 11.52 18.8 12 18 12C16.88 12 16 11.12 16 10C16 9.2 16.48 8.56 17.12 8.24C16.76 8.12 16.4 8 16 8Z"
                            fill="#5182B2"
                            fillOpacity="0.5"
                          />
                        </svg>
                      </span>
                      <span className="email-show">marco@***them.all</span>
                    </td>
                    <td>
                      <span className="cursor-pointer">
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_4_COMMON"
                        )}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {t(
                        "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_3"
                      )}
                    </td>
                   
                    <td>
                      <span className="eye-icon">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="32"
                          height="24"
                          viewBox="0 0 32 24"
                          fill="none"
                        >
                          <path
                            d="M16.12 0C6 0 0 12 0 12C0 12 6 24 16.12 24C26 24 32 12 32 12C32 12 26 0 16.12 0ZM16 4C20.44 4 24 7.6 24 12C24 16.44 20.44 20 16 20C11.6 20 8 16.44 8 12C8 7.6 11.6 4 16 4ZM16 8C13.8 8 12 9.8 12 12C12 14.2 13.8 16 16 16C18.2 16 20 14.2 20 12C20 11.6 19.84 11.24 19.76 10.88C19.44 11.52 18.8 12 18 12C16.88 12 16 11.12 16 10C16 9.2 16.48 8.56 17.12 8.24C16.76 8.12 16.4 8 16 8Z"
                            fill="#5182B2"
                            fillOpacity="0.5"
                          />
                        </svg>
                      </span>
                      <span className="email-show show">paint@***them.all</span>
                    </td>
                    <td>
                      <span className="cursor-pointer">
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_4_COMMON"
                        )}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {t(
                        "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_5"
                      )}
                    </td>
                   
                    <td>
                      <span className="eye-icon">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="32"
                          height="24"
                          viewBox="0 0 32 24"
                          fill="none"
                        >
                          <path
                            d="M16.12 0C6 0 0 12 0 12C0 12 6 24 16.12 24C26 24 32 12 32 12C32 12 26 0 16.12 0ZM16 4C20.44 4 24 7.6 24 12C24 16.44 20.44 20 16 20C11.6 20 8 16.44 8 12C8 7.6 11.6 4 16 4ZM16 8C13.8 8 12 9.8 12 12C12 14.2 13.8 16 16 16C18.2 16 20 14.2 20 12C20 11.6 19.84 11.24 19.76 10.88C19.44 11.52 18.8 12 18 12C16.88 12 16 11.12 16 10C16 9.2 16.48 8.56 17.12 8.24C16.76 8.12 16.4 8 16 8Z"
                            fill="#5182B2"
                            fillOpacity="0.5"
                          />
                        </svg>
                      </span>
                      <span className="email-show show">bless@***me.com</span>
                    </td>
                    <td>
                      <span className="cursor-pointer">
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_ACTIVITY_TAB_TABLE_2_TD_4_COMMON"
                        )}
                      </span>
                      
                    </td>
                  </tr> */}
                  </tbody>
                </table>
              </div>
            </div>

            {/* <div className="box">
            <h2 className="small-title">
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_2")}
            </h2>
            <div className=" chart-image-small h-auto">
              <div className="chart-image-box">
                <img src="/assets/images/small-cestina.png" alt="" />
              </div>
              <div className="chart-image-box">
                <img src="/assets/images/small-matematika.png" alt="" />
              </div>
            </div>
          </div> */}
          </div>
        </div>
      </div>
    </>
  );
}

export default ActivityTab;
