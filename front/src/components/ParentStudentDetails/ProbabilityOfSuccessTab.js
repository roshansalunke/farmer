import React from "react";
import { useTranslation } from "react-i18next";

function ProbabilityOfSuccessTab() {
  const { t } = useTranslation();
  return (
    <div
      className="tab-pane fade"
      id="prob-success-tab-pane"
      role="tabpanel"
      aria-labelledby="prob-success-tab"
      tabIndex="0"
    >
      <div className="prob-succes-sec">
        <h2 className="sub-title">
          {t("LBL_PARENT_STUDENT_DETAIL_PAGE_PROB_OF_SUCCESS_TAB_H2")}
          {/* Probability of success in admission */}
        </h2>
        <div className="in-sec">
          <div className="image-box">
            <img src="/assets/images/Osy.png" alt="" className="img-contain" />
          </div>
          <div className="owl-image">
            <img
              src="/assets/images/sova_new_clr 1.png"
              alt=""
              className="img-contain"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProbabilityOfSuccessTab;
