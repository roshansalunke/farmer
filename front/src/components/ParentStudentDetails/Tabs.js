import React from "react";
import Tab from "./Tab";
import TabsContent from "./TabsContent";

function Tabs() {
  return (
    <div className="right-side">
      <ul className="nav nav-tabs" id="myTab" role="tablist">
        <li className="nav-item" role="presentation">
          <Tab
            tabName={"activity-tab"}
            label={"LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_1"}
            defaultActive
          />
        </li>
        {/* <li className="nav-item" role="presentation">
          <Tab
            tabName={"education-map-tab"}
            label={"LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_2"}
          />
        </li> */}
        {/* <li className="nav-item" role="presentation">
          <Tab
            tabName={"prob-success-tab"}
            label={"LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_3"}
          />
        </li> */}
        <li className="nav-item" role="presentation">
          <Tab
            tabName={"school-analysis-tab"}
            label={"LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_4"}
          />
        </li>
        <li className="nav-item" role="presentation">
          <Tab
            tabName={"settings-tab"}
            label={"LBL_PARENT_STUDENT_DETAIL_PAGE_TAB_LI_5"}
          />
        </li>
      </ul>
      <TabsContent />
    </div>
  );
}

export default Tabs;
