import React from "react";
import { useTranslation } from "react-i18next";

function Tab({ tabName, label, defaultActive }) {
  const { t } = useTranslation();
  return (
    <button
      className={defaultActive ? "nav-link active" : "nav-link"}
      id={tabName}
      data-bs-toggle="tab"
      data-bs-target={`#${tabName}-pane`}
      type="button"
      role="tab"
      aria-controls={`#${tabName}-pane`}
      aria-selected="false"
    >
      {t(label)}
    </button>
  );
}

export default Tab;
