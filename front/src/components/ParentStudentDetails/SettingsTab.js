import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { GetUserShcoolList } from "../../store/action/shoppingCartActions";
import { useFormik } from "formik";
import * as Yup from "yup";
import { GetElementaryAndHighSchoolList } from "../../store/action/ParentSchoolDetails";
import axios from "axios";
import { useLocation } from "react-router-dom";

function SettingsTab() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [schoolTypeVal, setSchoolTypeVal] = React.useState(null);
  const { userSchoolData } = useSelector((state) => state.shoppingCart);
  const { userByTokenData } = useSelector((state) => state.user);
  const { state } = useLocation();
  const { getElemAndHighSchoolData } = useSelector(
    (state) => state.parentSchoolDetails
  );

  const initialFormValues = {
    iSchoolId: "",
  };
  const [initialValues] = React.useState(initialFormValues);

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: Yup.object({
      iSchoolId: Yup.string().required("School is Required."),
    }),
    enableReinitialize: true,
    onSubmit: async (values, { resetForm }) => {
      if (
        userByTokenData &&
        userByTokenData.code === "200" &&
        userByTokenData.data &&
        Object.keys(userByTokenData.data).length > 0 &&
        state &&
        state.iUserId
      ) {
        if (schoolTypeVal === "High School") {
          values = {
            ...values,
            iParentId: userByTokenData.data.iUserId,
            iUserId: state.iUserId,
            iHighSchoolId: values.iSchoolId,
            vLangCode: sessionStorage.getItem("langType") || "CZ",
          };
        } else if (schoolTypeVal === "Elementary School") {
          values = {
            ...values,
            iParentId: userByTokenData.data.iUserId,
            iUserId: state.iUserId,
            iElementarySchoolId: values.iSchoolId,
            vLangCode: sessionStorage.getItem("langType") || "CZ",
          };
        }
      }
      try {
        await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user-school-setting/createAndUpdateUserSchoolData`,
          values,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (state && state.iUserId) {
          const req_data = {
            iUserId: state.iUserId,
          };
          dispatch(GetElementaryAndHighSchoolList(req_data));
        }

        document.getElementById("offcanvass-school-close-id").click();
        resetForm();
      } catch (err) {
        console.log(err);
      }
    },
  });

  React.useEffect(() => {
    if (
      !getElemAndHighSchoolData &&
      userByTokenData &&
      userByTokenData.code === "200" &&
      state &&
      state.iUserId
    ) {
      const req_data = {
        iUserId: state.iUserId,
      };
      dispatch(GetElementaryAndHighSchoolList(req_data));
    }
  }, [getElemAndHighSchoolData, userByTokenData, dispatch, state]);

  React.useEffect(() => {
    if (!userSchoolData && userByTokenData && userByTokenData.code === "200")
      dispatch(GetUserShcoolList({ iUserId: userByTokenData.data.iUserId }));
  }, [userSchoolData, dispatch, userByTokenData]);

  return (
    <div
      className="tab-pane fade school-analy-sec"
      id="settings-tab-pane"
      role="tabpanel"
      aria-labelledby="settings-tab"
      tabIndex="0"
    >
      <div className="activity-box">
        <div className="inner-box">
          <div className="box">
            <h2 className="small-title">
              {state && state.user
                ? `${state.user.vFirstName} ${state.user.vLastName} - `
                : ""}
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_BOX_1_H2")} &nbsp;
            </h2>
            <div className="cold-school-change">
              <div className="box">
                <div className="label-title">
                  {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_MODAL_DIV_1")}
                </div>
                {getElemAndHighSchoolData &&
                getElemAndHighSchoolData.code === "200" &&
                getElemAndHighSchoolData.data &&
                getElemAndHighSchoolData.data.length > 0 ? (
                  getElemAndHighSchoolData.data.map((ele_school, index) => (
                    <div className="label-title" key={index}>
                      {ele_school.iElementarySchoolId && ele_school.school
                        ? ele_school.school?.vSchoolFullName
                        : ""}
                    </div>
                  ))
                ) : (
                  <></>
                )}

                {userByTokenData &&
                userByTokenData.code === "200" &&
                userByTokenData.data &&
                userByTokenData.data.eAccountType === "Parent" ? (
                  <div
                    className="change-box"
                    onClick={() => setSchoolTypeVal("Elementary School")}
                  >
                    {/* <span>
                    {t(
                      "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_BOX_1_SPAN"
                    )}
                  </span> */}
                    <button
                      className="pink-color bg-transparent border-0"
                      type="button"
                      data-bs-toggle="offcanvas"
                      data-bs-target="#offcanvaschooseschool"
                      aria-controls="offcanvaschooseschool"
                    >
                      {t(
                        "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_BOX_1_BUTTON_TEXT_COMMON"
                      )}
                    </button>
                  </div>
                ) : (
                  <></>
                )}
              </div>
              <div className="box">
                <div className="label-title">
                  {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_BOX_2_DIV_1")}
                </div>

                {getElemAndHighSchoolData &&
                getElemAndHighSchoolData.code === "200" &&
                getElemAndHighSchoolData.data &&
                getElemAndHighSchoolData.data.length > 0 ? (
                  getElemAndHighSchoolData.data.map((high_school, index) => (
                    <div className="label-title" key={index}>
                      {high_school.iHighSchoolId && high_school.highschool
                        ? `${high_school.highschool.nazev || ""} - ${
                            high_school.highschool.smo_name || ""
                          }`
                        : ""}
                    </div>
                  ))
                ) : (
                  <></>
                )}

                {userByTokenData &&
                userByTokenData.code === "200" &&
                userByTokenData.data &&
                userByTokenData.data.eAccountType === "Parent" ? (
                  <div
                    className="change-box"
                    onClick={() => setSchoolTypeVal("High School")}
                  >
                    {/* <span>
                    {t(
                      "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_BOX_2_SPAN"
                    )}
                  </span> */}
                    <button
                      className="pink-color bg-transparent border-0"
                      type="button"
                      data-bs-toggle="offcanvas"
                      data-bs-target="#offcanvaschooseschool"
                      aria-controls="offcanvaschooseschool"
                    >
                      {t(
                        "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_BOX_1_BUTTON_TEXT_COMMON"
                      )}
                    </button>
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
            <form onSubmit={formik.handleSubmit}>
              <div
                className="offcanvas offcanvas-end login-canvas choose-school-canvas"
                // data-bs-backdrop="static"
                tabIndex="-1"
                id="offcanvaschooseschool"
                aria-labelledby="offcanvaschooseschoolLabel"
              >
                <div className="offcanvas-header">
                  <div
                    id="offcanvasconnectaccountLabel"
                    className="canvas-title"
                  >
                    {schoolTypeVal !== null && schoolTypeVal === "High School"
                      ? t("LBL_HIGH_SCHOOL_TITLE")
                      : t("LBL_SCHOOL_TITLE")}

                    {/* {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_MODAL_DIV_1")} */}
                  </div>
                  <button
                    type="button"
                    className="btn-close text-reset"
                    data-bs-dismiss="offcanvas"
                    aria-label="Close"
                    onClick={() => formik.resetForm()}
                    id="offcanvass-school-close-id"
                  ></button>
                </div>

                <div className="offcanvas-body">
                  <select
                    className="form-select"
                    id="iSchoolId"
                    name="iSchoolId"
                    value={formik.values.iSchoolId}
                    onChange={(e) => formik.handleChange(e)}
                  >
                    <option value="">
                      {t("LBL_SETTING_PAGE_OFFCANVAS_HEADER_BUTTON_2_COMMON")}
                      &nbsp;
                      {schoolTypeVal !== null && schoolTypeVal === "High School"
                        ? t("LBL_HIGH_SCHOOL_TITLE")
                        : t("LBL_SCHOOL_TITLE")}
                    </option>
                    {userSchoolData !== undefined &&
                    userSchoolData.code === "200" &&
                    userSchoolData.data.length > 0 ? (
                      userSchoolData.data
                        .filter((high) =>
                          schoolTypeVal === "High School"
                            ? high.eSchoolType === "High School"
                            : high.eSchoolType !== "High School"
                        )
                        ?.map((school, index) => (
                          <option key={index} value={school.iSchoolId}>
                            {school.eSchoolType === "High School"
                              ? `${school.highschool.nazev || ""} - ${
                                  school.highschool.smo_name || ""
                                } `
                              : school.school.vSchoolFullName}
                          </option>
                        ))
                    ) : (
                      <>School Data Not Found</>
                    )}
                  </select>
                  {formik.errors.iSchoolId && formik.touched.iSchoolId ? (
                    <div className="text-danger">{formik.errors.iSchoolId}</div>
                  ) : null}
                  <button className="btn green-btn">
                    {t(
                      "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_MODAL_BUTTON_1"
                    )}
                  </button>
                </div>
              </div>
            </form>
          </div>
          {/* <div className="box">
            <div className="small-topbar-area">
              <h2 className="small-title">
                {t(
                  "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TOOLBAR_AREA_H2"
                )}
              </h2>
              <div>
                <button className="pink-color bg-transparent border-0">
                  {t(
                    "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TOOLBAR_AREA_BUTTON"
                  )}
                </button>
              </div>
            </div>
            <div className="switch-opt">
              <div className="table-box table-responsive">
                <table className="table">
                  <tbody>
                    <tr>
                      <td></td>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_1_TD_1"
                        )}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_2_TD"
                        )}
                      </td>

                      <td>
                        <span className="count">10</span>
                      </td>
                      <td>
                        <div className="form-check form-switch green-switch">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault"
                          />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_3_TD"
                        )}
                      </td>
                      <td>
                        <span className="count">70</span>
                      </td>
                      <td>
                        <div className="form-check form-switch green-switch">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault"
                          />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_4_TD"
                        )}
                      </td>
                      <td>
                        <span className="count">65%</span>
                      </td>
                      <td>
                        <div className="form-check form-switch green-switch">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault"
                          />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_5_TD"
                        )}
                      </td>
                      <td>
                        <span className="count">60%</span>
                      </td>
                      <td>
                        <div className="form-check form-switch green-switch">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault"
                          />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_6_TD"
                        )}
                      </td>
                      <td></td>
                      <td>
                        <div className="form-check form-switch pink-switch">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault"
                          />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {t(
                          "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_TABLE_BOX_TR_7_TD"
                        )}
                      </td>
                      <td></td>
                      <td>
                        <div className="form-check form-switch pink-switch">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault"
                          />
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div> */}
        </div>
        {/* <div className="inner-box">
          <div className="box">
            <ul className="breadcumb">
              <li>
                {t(
                  "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_INNER_BOX_LI_1"
                )}
              </li>
              <li>
                <span>/</span>
                {t(
                  "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_INNER_BOX_LI_2"
                )}
              </li>
              <li>
                <span>/</span>
                {t(
                  "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_INNER_BOX_LI_3"
                )}
              </li>
            </ul>
            <h2 className="small-title">
              {t("LBL_PARENT_STUDENT_DETAIL_PAGE_SCHOOL_ANALYSIS_TAB_H2")}
            </h2>
            <div className="chart-image">
              <img
                src="../assets/images/Analysis-targets-graphs.png"
                alt="graph"
                className="img-contain"
              />
            </div>
            <div className="stn-summary">
              <div className="improve-box">
                <div className="table-box table-responsive">
                  <table className="table">
                    <tbody>
                      <tr>
                        <td>
                          {t(
                            "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_STN_SUMMARY_TR_1_TD_1"
                          )}
                        </td>
                        <td>49%</td>
                      </tr>
                      <tr>
                        <td>
                          {t(
                            "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_STN_SUMMARY_TR_2_TD_1"
                          )}
                        </td>
                        <td>50%</td>
                        <td>
                          {t(
                            "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_STN_SUMMARY_TR_2_TD_2"
                          )}
                        </td>
                      </tr>
                      <tr>
                        <td>50%</td>
                        <td>
                          {t(
                            "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_STN_SUMMARY_TR_3_TD_1"
                          )}
                        </td>
                        <td>50%</td>
                        <td>
                          {t(
                            "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_STN_SUMMARY_TR_3_TD_2"
                          )}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="owl-image">
                <p>
                  {t(
                    "LBL_PARENT_STUDENT_DETAIL_PAGE_SETTINGS_TAB_STN_SUMMARY_P"
                  )}
                </p>
                <img src="../assets/images/sova_new_clr 1.png" alt="logo" />
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
}

export default SettingsTab;
