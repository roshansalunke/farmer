import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { GetApproachText } from "../../store/action/Content";

function ModelFlow() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { contentApproachData } = useSelector((state) => state.content);

  React.useEffect(() => {
    if (!contentApproachData) {
      dispatch(
        GetApproachText({
          vLangCode: sessionStorage.getItem("langType") || "CZ",
        })
      );
    }
  }, [dispatch, contentApproachData]);

  return (
    <div className="apr-section pt-50 pb-50">
      <h2 className="title">
        {t("LBL_MODEL_FLOW_TITLE")}
        {/* How we have prepared data model */}
      </h2>
      <div className="inner-box">
        <div className="inner-box">
          {contentApproachData &&
          contentApproachData.code === "200" &&
          contentApproachData.data &&
          contentApproachData.data.length > 0 ? (
            contentApproachData.data.map((content, index) => (
              <div className="box" key={index}>
                <div className="image-box">
                  <img
                    src={content.vImage}
                    width="45"
                    className="img-contain"
                    height="50"
                    alt=""
                  />
                  {/* <svg
                    xmlns={content.vImage}
                    width="45"
                    height="50"
                    viewBox="0 0 45 50"
                    fill="none"
                  >
                    <path
                      d="M13 0C19.25 12.5 0.5 18.75 0.5 31.25C0.5 43.75 13 50 13 50C6.875 37.625 25.5 31.25 25.5 18.75C25.5 6.25 13 0 13 0ZM31.75 18.75C38 31.25 19.25 37.5 19.25 50H38C40.5 50 44.25 46.875 44.25 37.5C44.25 25 31.75 18.75 31.75 18.75Z"
                      fill="#E61C63"
                    />
                  </svg> */}
                </div>
                <p className="descri">
                  {content.tDescription}
                  {/* Focused on every knowledge gained within our educational platform
            with best results. */}
                </p>
              </div>
            ))
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
}

export default ModelFlow;
