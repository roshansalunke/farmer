import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";

import MapPath from "../Custom/Map/MapPath";
import { DistrictList } from "../../store/action/Region";
import { RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA } from "../../store/constants/schoolStatistics";
import PrahaMapPath from "../Custom/Map/PrahaMapPath";

function ChooseRegion() {
  const { t } = useTranslation();
  const [state, setState] = React.useState("");
  const [districtId, setDistrictId] = React.useState(null);
  const [visible, setVisible] = React.useState(false);
  const [addClass, setAddClass] = React.useState("");
  const [cardPositions, setCardPositions] = React.useState({
    Left: 0,
    Top: 0,
  });
  const { districtList } = useSelector((state) => state.region);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (!districtList) {
      dispatch(DistrictList());
    }
  }, [districtList, dispatch]);

  const handleClick = React.useCallback(
    (e, state, distId) => {
      if (distId === 79) {
        setVisible(true);
      } else {
        dispatch({ type: RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA });
        setAddClass("region-card-bg");
        const { offsetX, offsetY } = e.nativeEvent;
        setCardPositions({
          Left: offsetX,
          Top: offsetY,
        });
        setState(state);
        setDistrictId(distId);
      }
    },
    [dispatch]
  );

  const handleSelect = React.useCallback(
    (e) => {
      let paths = document.querySelectorAll("path");
      dispatch({ type: RESET_SCHOOL_AND_PERCENTAGE_STAT_DATA });
      const { value: distId } = e.target;
      let distVal =
        districtList &&
        districtList.data &&
        districtList.data.length > 0 &&
        districtList.data.find((val) => +val.iDistrictId === +distId)
          .vDistrictTitle;
      setState(distVal);
      setDistrictId(distId);
      for (let path of paths) {
        if (+path.id === +distId) {
          const myPath = document.getElementById(path.id);
          const boundingBox = myPath.ownerSVGElement.getBoundingClientRect();

          // Calculate the center coordinates
          const offsetX =
            myPath.getBoundingClientRect().left - boundingBox.left;
          const offsetY = myPath.getBoundingClientRect().top - boundingBox.top;
          setAddClass("region-card-bg");
          setCardPositions({
            Left: offsetX + 20,
            Top: offsetY + 20,
          });
        }
      }
    },
    [districtList, dispatch]
  );

  return (
    <div className="historic-sec pt-50 pb-50 plr-50 region-page region-page-space">
      <h2>{t("LBL_CHOOSE_REGION_HEADER_TITLE")}</h2>
      {/* <h2>Choose your region for results</h2> */}
      <div className="action-btn">
        <Link to={"/"} className="green-btn btn">
          {t("LBL_CHOOSE_REGION_LINE_1")}
          {/* Elementary schools */}
        </Link>
        {/* <Link to={"/"} className="blue-btn btn">
          {t("LBL_CHOOSE_REGION_LINE_2")}

        </Link> */}
        {/* <Link to={"/"} className="pink-btn btn">
          {t("LBL_CHOOSE_REGION_LINE_3")}
        </Link> */}
        <div className="btn p-0 pink-color region-select">
          <span>{t("LBL_HOME_HISTORICAL_RESULT_REGION_TEXT")}:</span>
          <select
            onChange={handleSelect}
            className="form-select pink-color h-100 cursor-pointer border-0 bg-transparent text-decoration-underline"
            aria-label="Default select example"
            value={
              districtId !== null &&
              districtList !== undefined &&
              districtList.code === "200" &&
              districtList.data.length > 0 &&
              districtList.data.find((val) => +val.iDistrictId === +districtId)
                .iDistrictId
            }
          >
            <option value="">{t("LBL_SELECT_DISTRICT_OPTION_TITLE")}</option>
            {districtList !== undefined &&
            districtList.code === "200" &&
            districtList.data.length > 0 ? (
              districtList.data.map((dist, index) => (
                <option key={index} value={dist.iDistrictId}>
                  {dist.vDistrictTitle}
                </option>
              ))
            ) : (
              <>District Data Not Found</>
            )}
          </select>
        </div>
      </div>
      <div className="inner-box">
        <MapPath
          state={state}
          handleClick={handleClick}
          cardPositions={cardPositions}
          setCardPositions={setCardPositions}
          setState={setState}
          addClass={addClass}
          setAddClass={setAddClass}
          setDistrictId={setDistrictId}
          districtId={districtId}
        />
        <div className="image-box">
          <img
            src="../assets/images/sova_new_clr 1.png"
            alt=""
            className="img-contain"
          />
        </div>
      </div>
      <ul>
        <li>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
          >
            <path
              d="M23.5673 8.32052L12.3173 3.07052C12.218 3.02415 12.1097 3.00012 12 3.00012C11.8904 3.00012 11.7821 3.02415 11.6828 3.07052L0.432786 8.32052C0.303524 8.38092 0.194169 8.47692 0.117554 8.59728C0.0409394 8.71764 0.000244141 8.85735 0.000244141 9.00002C0.000244141 9.1427 0.0409394 9.28241 0.117554 9.40277C0.194169 9.52313 0.303524 9.61913 0.432786 9.67952L4.50004 11.5778V17.25C4.50004 19.3883 7.72504 21 12 21C16.275 21 19.5 19.3883 19.5 17.25V11.5778L21 10.8773V15C21 15.1989 21.0791 15.3897 21.2197 15.5304C21.3604 15.671 21.5511 15.75 21.75 15.75C21.9489 15.75 22.1397 15.671 22.2804 15.5304C22.421 15.3897 22.5 15.1989 22.5 15V10.1775L23.5673 9.67952C23.6965 9.61913 23.8059 9.52313 23.8825 9.40277C23.9591 9.28241 23.9998 9.1427 23.9998 9.00002C23.9998 8.85735 23.9591 8.71764 23.8825 8.59728C23.8059 8.47692 23.6965 8.38092 23.5673 8.32052ZM18 17.25C18 18.168 15.663 19.5 12 19.5C8.33704 19.5 6.00004 18.168 6.00004 17.25V12.2775L11.6828 14.9295C11.7821 14.9759 11.8904 14.9999 12 14.9999C12.1097 14.9999 12.218 14.9759 12.3173 14.9295L16.0673 13.179C16.2444 13.093 16.3806 12.9407 16.4465 12.7552C16.5123 12.5696 16.5025 12.3655 16.4192 12.1871C16.3359 12.0087 16.1857 11.8702 16.0012 11.8015C15.8166 11.7328 15.6125 11.7395 15.4328 11.82L12 13.425L2.52304 9.00002L12 4.57502L21.4763 9.00002L18.4328 10.4205C18.2917 10.4873 18.1752 10.5969 18.0999 10.7337C18.0246 10.8705 17.9944 11.0276 18.0135 11.1825C18.0079 11.2048 18.0034 11.2273 18 11.25V17.25Z"
              fill="#0071B8"
            />
          </svg>
          {t("LBL_CHOOSE_REGION_DETAIL_LI_9")}

          {/* <span>Over 12 million students</span> */}
        </li>
        <li>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="22"
            height="22"
            viewBox="0 0 22 22"
            fill="none"
          >
            <g clipPath="url(#clip0_207_326)">
              <path
                d="M14.4805 0H7.51953C3.37326 0 0 3.37326 0 7.51953V14.4805C0 18.6267 3.37326 22 7.51953 22H14.4805C18.6267 22 22 18.6267 22 14.4805V7.51953C22 3.37326 18.6267 0 14.4805 0ZM20.7109 14.4805C20.7109 17.9159 17.9159 20.7109 14.4805 20.7109H7.51953C4.08405 20.7109 1.28906 17.9159 1.28906 14.4805V7.51953C1.28906 4.08405 4.08405 1.28906 7.51953 1.28906H14.4805C17.9159 1.28906 20.7109 4.08405 20.7109 7.51953V14.4805Z"
                fill="#0071B8"
              />
              <path
                d="M14.8002 9.74949L9.47216 6.1897C8.47374 5.52265 7.13281 6.23997 7.13281 7.44018V14.5597C7.13281 15.7667 8.48186 16.4718 9.4722 15.8102L14.8003 12.2504C15.6897 11.6562 15.6923 10.3455 14.8002 9.74949ZM14.0841 11.1786L8.75609 14.7384C8.61352 14.8336 8.42188 14.7331 8.42188 14.5598V7.44018C8.42188 7.26903 8.61171 7.16505 8.75609 7.26151L14.0842 10.8213C14.2117 10.9066 14.2119 11.0932 14.0841 11.1786Z"
                fill="#0071B8"
              />
            </g>
            <defs>
              <clipPath id="clip0_207_326">
                <rect width="22" height="22" fill="white" />
              </clipPath>
            </defs>
          </svg>
          {t("LBL_CHOOSE_REGION_DETAIL_LI_10")}

          {/* <span>More than 60,000 courses</span> */}
        </li>
        <li>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
          >
            <path
              d="M20.2224 12.6562H19.684C19.3694 10.467 17.6089 8.73858 15.4043 8.47317C16.339 7.57678 16.9219 6.31612 16.9219 4.92188C16.9219 2.20795 14.7139 0 12 0C9.2861 0 7.07814 2.20795 7.07814 4.92188C7.07814 6.31617 7.66103 7.57678 8.59577 8.47317C6.39119 8.73858 4.63066 10.467 4.31608 12.6562H3.77767C2.49297 12.6562 1.50836 13.7959 1.68935 15.0629L2.86628 23.3952C2.91527 23.7421 3.21217 24 3.56252 24H20.4375C20.7879 24 21.0848 23.7421 21.1338 23.3952L22.3106 15.0639C22.4921 13.7932 21.5047 12.6562 20.2224 12.6562ZM8.48439 4.92188C8.48439 2.98336 10.0615 1.40625 12 1.40625C13.9385 1.40625 15.5156 2.98336 15.5156 4.92188C15.5156 6.86039 13.9385 8.4375 12 8.4375C10.0615 8.4375 8.48439 6.86039 8.48439 4.92188ZM9.18752 9.84375H14.8125C16.5103 9.84375 17.9308 11.0535 18.2575 12.6562H5.74253C6.06921 11.0535 7.48975 9.84375 9.18752 9.84375ZM20.9183 14.8662L19.8267 22.5938H4.1733L3.08163 14.865C3.02097 14.441 3.34853 14.0625 3.77767 14.0625C3.97202 14.0625 20.141 14.0625 20.2224 14.0625C20.651 14.0625 20.9792 14.44 20.9183 14.8662Z"
              fill="#0071B8"
            />
            <path
              d="M12.0001 16.9219C10.837 16.9219 9.89075 17.8681 9.89075 19.0312C9.89075 20.1944 10.837 21.1406 12.0001 21.1406C13.1632 21.1406 14.1095 20.1944 14.1095 19.0312C14.1095 17.8681 13.1632 16.9219 12.0001 16.9219ZM12.0001 19.7344C11.6124 19.7344 11.297 19.419 11.297 19.0312C11.297 18.6435 11.6124 18.3281 12.0001 18.3281C12.3878 18.3281 12.7032 18.6435 12.7032 19.0312C12.7032 19.419 12.3878 19.7344 12.0001 19.7344Z"
              fill="#0071B8"
            />
          </svg>
          {t("LBL_CHOOSE_REGION_DETAIL_LI_11")}
          {/* <span>Learn anything online</span> */}
        </li>
      </ul>
      <PrahaMapPath visible={visible} setVisible={setVisible} />
    </div>
  );
}

export default ChooseRegion;
