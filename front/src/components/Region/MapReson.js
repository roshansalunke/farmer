import React from "react";
import { useTranslation } from "react-i18next";

function MapReson() {
  const { t } = useTranslation();

  return (
    <div className="our-story pt-50 pb-50">
      <div className="image-box">
        <img
          src="../assets/images/MAP-image.png"
          alt=""
          className="img-contain"
        />
      </div>
      <h2 className="title">{t("LBL_MAP_REASON_TITLE")}</h2>
      {/* <h2 className="title">Why we made this map</h2> */}
      <p className="descri">
        {t("LBL_MAP_REASON_DESCRI")}
        {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non est
        a ex elementum dignissim. In faucibus, arcu et luctus condimentum,
        mauris orci bibendum turpis, a interdum nibh nunc ac arcu. */}
      </p>
    </div>
  );
}

export default MapReson;
