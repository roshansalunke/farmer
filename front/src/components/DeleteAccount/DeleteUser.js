import React from "react";
import { Dialog } from "primereact/dialog";
import { confirmDialog } from "primereact/confirmdialog";
import axios from "axios";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

function DeleteUser() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { userByTokenData } = useSelector((state) => state.user);
  const [tDeleteRegion, settDeleteRegion] = React.useState("");
  const [visible, setVisible] = React.useState(false);

  const handleAccpet = React.useCallback(async () => {
    if (
      userByTokenData &&
      userByTokenData.code === "200" &&
      userByTokenData.data &&
      Object.keys(userByTokenData.data).length > 0
    ) {
      const reqParameters = {
        tDeleteRegion: tDeleteRegion,
        iUserId: userByTokenData.data.iUserId,
        vLangCode: sessionStorage.getItem("langType") || "CZ",
      };
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/deleteAccountByiUserId`,
          reqParameters,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (res && res.status === 200) {
          const { data } = res;
          if (data && data.code === "200") {
            navigate("/logout");
          }
        }
      } catch (err) {
        return;
      }
    }
  }, [tDeleteRegion, navigate, userByTokenData]);

  const handleDelete = React.useCallback(() => {
    confirmDialog({
      message: t("LBL_DELETE_CONFIRMATION_TEXT"),
      className: "logout-popup",
      header: t("LBL_CONFIRMATION"),
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        // window.Android.performClick();
        //   navigate("/logout");
        handleAccpet();
        setVisible(false);
      },
      reject: () => {
        setVisible(false);
      },
      acceptLabel: t("LBL_YES"),
      rejectLabel: t("LBL_NO"),
    });
  }, [handleAccpet, t]);

  const footerContent = (
    <div>
      <button
        label="No"
        // icon="pi pi-times"
        onClick={() => setVisible(false)}
        className="btn me-2"
      >
        {t("LBL_DELETE_NO_BUTTON")}
      </button>
      <button
        label="Delete Account"
        className="btn yellow-btn"
        // icon="pi pi-check"
        onClick={handleDelete}
        autoFocus
      >
        {t("LBL_DELETE_ACCOUNT_BUTTON")}
      </button>
    </div>
  );

  return (
    <>
      <div className="white-card plr-50 pt-50 pb-50 custom-min-h lesson-done">
        <div className="row align-items-center g-md-4 gy-5">
          <div className="col-md-6">
            <div className="details-left">
              <div className="form-floating">
                <textarea
                  className="form-control"
                  placeholder={t("LBL_DELETE_ACCOUNT_TEXT")}
                  name="tDeleteRegion"
                  id="tDeleteRegion"
                  style={{ minHeight: "100px" }}
                  onChange={(e) => settDeleteRegion(e.target.value)}
                ></textarea>
                <label htmlFor="tDeleteRegion">
                  {t("LBL_DELETE_ACCOUNT_TEXT")}
                </label>
              </div>
              <button
                className="btn yellow-btn mt-5"
                onClick={() => setVisible(true)}
              >
                {t("LBL_DELETE_ACCOUNT_BUTTON_TEXT")}
              </button>
            </div>
          </div>
          <div className="col-md-6">
            <div className="details-right">
              <div className="image-box">
                <img
                  src="../assets/images/sova_new_clr 1.png"
                  alt=""
                  className="img-contain"
                />
              </div>
              <p className="dark-blue-color">{t("LBL_DELETE_P_DESCRIPTION")}</p>
            </div>
          </div>
        </div>
      </div>
      <Dialog
        header={t("LBL_DELETE_DIALOG_TEXT")}
        visible={visible}
        style={{ width: "50vw" }}
        onHide={() => setVisible(false)}
        footer={footerContent}
      >
        <p className="m-0">
          {t("LBL_DELETE_P_DESCRIPTION_1")}
          <br />
          <br />
          {t("LBL_DELETE_P_DESCRIPTION_2")}
          <br />
          <br />
          {t("LBL_DELETE_P_DESCRIPTION_3")}
          <br />
          <br />
          {t("LBL_DELETE_P_DESCRIPTION_4")}
          <br />
          <br />
          {t("LBL_DELETE_P_DESCRIPTION_5")}
        </p>
      </Dialog>
    </>
  );
}

export default DeleteUser;
