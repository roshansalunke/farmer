import React from "react";

function Calculator() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="42"
      height="47"
      viewBox="0 0 42 47"
      fill="none"
    >
      <g filter="url(#filter0_i_1816_244)">
        <path
          d="M1.50688 1C1.16896 1 1 1.22528 1 1.50688V45.4931C1 45.7747 1.22528 46 1.50688 46H39.8611C40.1427 46 40.368 45.7747 40.368 45.4931V1.50688C40.368 1.16896 40.1427 1 39.8611 1L1.50688 1ZM6.63204 6.63204H34.7922V17.8961H6.63204V6.63204ZM6.63204 23.5282H12.2641V29.1602H6.63204V23.5282ZM17.8961 23.5282H23.5282V29.1602H17.8961V23.5282ZM29.1602 23.5282H34.7922V40.4243H29.1602V23.5282ZM6.63204 34.7922H12.2641V40.4243H6.63204V34.7922ZM17.8961 34.7922H23.5282V40.4243H17.8961V34.7922Z"
          stroke="#BACF1B"
          strokeWidth="2"
        />
      </g>
      <defs>
        <filter
          id="filter0_i_1816_244"
          x="0"
          y="0"
          width="41.368"
          height="51"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="2" />
          <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="shape"
            result="effect1_innerShadow_1816_244"
          />
        </filter>
      </defs>
    </svg>
  );
}

export default Calculator;
