import React from "react";

function Circle() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="35"
      height="35"
      viewBox="0 0 35 35"
      fill="none"
    >
      <g filter="url(#filter0_i_1685_4018)">
        <path
          d="M13.6797 7.91795L12.125 6.88148V8.75V26.25V28.1185L13.6797 27.0821L26.8047 18.3321L28.0528 17.5L26.8047 16.6679L13.6797 7.91795ZM1 17.5C1 8.38353 8.38353 1 17.5 1C26.6165 1 34 8.38353 34 17.5C34 26.6165 26.6165 34 17.5 34C8.38353 34 1 26.6165 1 17.5Z"
          stroke="#5182B2"
          strokeWidth="2"
        />
      </g>
      <defs>
        <filter
          id="filter0_i_1685_4018"
          x="0"
          y="0"
          width="35"
          height="39"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="2" />
          <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="shape"
            result="effect1_innerShadow_1685_4018"
          />
        </filter>
      </defs>
    </svg>
  );
}

export default Circle;
