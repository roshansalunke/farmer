import React from "react";
import Container from "../components/Custom/Container/Container";
import Overview from "../components/ParentsHome/Overview";
import UserActivity from "../components/ParentsHome/UserActivity";

function ParentsHome() {
  return (
    <Container>
      <div className="white-card pt-50 pb-50 plr-50 parent-details-page">
        <div className="p-student-details parent-main-box">
          <Overview />
          <UserActivity />
        </div>
      </div>
    </Container>
  );
}

export default ParentsHome;
