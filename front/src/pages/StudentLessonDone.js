import React from "react";
import Container from "../components/Custom/Container/Container";

function StudentLessonDone() {
  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 custom-min-h lesson-done">
        <div className="row align-items-center g-md-4 gy-5">
          <div className="col-md-6">
            <div className="details-left">
              <h1 className="sec-heading">Congratulations</h1>
              <p className="green-color">You won this week/month!</p>
              <p className="dark-blue-color">Here's a reward for you</p>
              <p className="pink-color">You get an extra 3 points</p>
              <button className="btn yellow-btn">Continue</button>
            </div>
          </div>
          <div className="col-md-6">
            <div className="details-right">
              <div className="image-box">
                <img
                  src="../assets/images/sova_new_clr 1.png"
                  alt=""
                  className="img-contain"
                />
              </div>
              <p className="dark-blue-color">
                text for owl, specific for this page, because here we are starting with
                different point of view
              </p>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default StudentLessonDone;
