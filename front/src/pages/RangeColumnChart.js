import React from "react";
import CanvasJSReact from "@canvasjs/react-charts";

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function RangeColumnChart() {
  const options = {
    exportEnabled: true,
    animationEnabled: true,

    theme: "light2", // "light1", "dark1", "dark2"
    axisY: {
      labelFormatter: function (e) {
        return "";
      },
      lineThickness: 0,
      gridThickness: 0,
      tickLength: 0,
      gridColor: "transparent",
      labelBackgroundColor: "transparent",
    },
    axisX: {
      lineThickness: 0,
      tickThickness: 0,
      gridColor: "transparent",
    },

    data: [
      {
        type: "rangeColumn",
        indexLabel: "{y[#index]}°",
        xValueFormatString: "MMM YYYY",
        toolTipContent:
          "<strong>{x}</strong></br> Max: {y[1]} °C<br/> Min: {y[0]} °C",
        dataPoints: [
          { x: new Date("2017- 01- 01"), y: [19, 26] },
          { x: new Date("2017- 02- 01"), y: [19, 26] },
          { x: new Date("2017- 03- 01"), y: [18, 25] },
          { x: new Date("2017- 04- 01"), y: [15, 23] },
          { x: new Date("2017- 05- 01"), y: [12, 20] },
        ],
      },
    ],
  };

  return (
    <div>
      <CanvasJSChart options={options} />
    </div>
  );
}

export default RangeColumnChart;
