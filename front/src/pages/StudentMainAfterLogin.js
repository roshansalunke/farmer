import React from "react";
import Container from "../components/Custom/Container/Container";
import { CircularProgressbar } from "react-circular-progressbar";
import { useNavigate } from "react-router-dom";

function StudentMainAfterLogin() {
  const [lang, setLang] = React.useState("en");
  const value = 0.66;
  const navigate = useNavigate();

  React.useEffect(() => {
    let lng = "en";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 custom-min-h student-main">
        <div className="inner-box">
          <div className="details-left">
            <h2 className="sec-heading text-decoration-underline">
              Improvement
            </h2>
            <div className="table-box table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th className="font-24-semibold"></th>
                    <th className="blue-color font-24-semibold text-center">
                      Starting at
                    </th>
                    <th className="blue-color font-24-semibold text-center">
                      Now
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="yellow-color font-24-semibold">
                      Czech language
                    </td>
                    <td className="yellow-color font-24-semibold text-center">
                      22%
                    </td>
                    <td className="green-color font-24-semibold text-center">
                      38%
                    </td>
                  </tr>
                  <tr>
                    <td className="dots blue-color font-24-semibold">
                      <ul>
                        <li>Verbs</li>
                      </ul>
                    </td>
                    <td className="blue-color font-24-semibold text-center">
                      22%
                    </td>
                    <td className="green-color font-24-semibold text-center">
                      37%
                    </td>
                  </tr>
                  <tr>
                    <td className="dots blue-color font-24-semibold">
                      <ul>
                        <li>Subject-verb agreement</li>
                      </ul>
                    </td>
                    <td className="blue-color font-24-semibold text-center">
                      18%
                    </td>
                    <td className="green-color font-24-semibold text-center">
                      41%
                    </td>
                  </tr>
                  <tr>
                    <td className="yellow-color font-24-semibold">
                      Mathematics - total
                    </td>
                    <td className="yellow-color font-24-semibold text-center">
                      28%
                    </td>
                    <td className="green-color font-24-semibold text-center">
                      34%
                    </td>
                  </tr>
                  <tr>
                    <td className="dots blue-color font-24-semibold">
                      <ul>
                        <li>Inequalities</li>
                      </ul>
                    </td>
                    <td className="blue-color font-24-semibold text-center">
                      11%
                    </td>
                    <td className="green-color font-24-semibold text-center">
                      36%
                    </td>
                  </tr>
                  <tr>
                    <td className="dots blue-color font-24-semibold">
                      <ul>
                        <li>Circle</li>
                      </ul>
                    </td>
                    <td className="blue-color font-24-semibold text-center">
                      18%
                    </td>
                    <td className="pink-color font-24-semibold text-center">
                      17%
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="perfome-box">
              <h3 className="text-decoration-underline blue-color font-24-semibold">
                Your points
              </h3>
              <div className="table-box table-responsive">
                <table className="table">
                  <tbody>
                    <tr>
                      <td className="blue-color font-24-semibold">
                        Points for chapters
                      </td>
                      <td className="green-color font-24-semibold">
                        39 points
                      </td>
                    </tr>
                    <tr>
                      <td className="blue-color font-24-semibold">
                        Points for competitions
                      </td>
                      <td className="green-color font-24-semibold">
                        18 points
                      </td>
                    </tr>
                    <tr>
                      <td className="blue-color font-24-semibold">
                        Current exam discount
                      </td>
                      <td className="green-color font-24-semibold">
                        41% (57 points)
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="perfome-box">
              <h3 className="text-decoration-underline blue-color font-24-semibold">
                Your BEST
              </h3>
              <div className="table-box table-responsive">
                <table className="table">
                  <tbody>
                    <tr>
                      <td className="blue-color font-24-semibold">
                        Hardest lesson
                      </td>
                      <td className="blue-color font-24-semibold">
                        name (repeat)
                      </td>
                    </tr>
                    <tr>
                      <td className="blue-color font-24-semibold">
                        The best lessons
                      </td>
                      <td className="blue-color font-24-semibold">
                        name (time)
                      </td>
                    </tr>
                    <tr>
                      <td className="blue-color font-24-semibold">
                        The least knowledge
                      </td>
                      <td className="blue-color font-24-semibold">name (%)</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="details-right">
            <h2 className="sec-heading text-decoration-underline">Studium</h2>
            <div className="right-box">
              <div className="progress-sec">
                <div className="box">
                  <CircularProgressbar
                    // value={value}
                    maxValue={1}
                    text={`${value * 100}%`}
                    className="green-color"
                  />
                </div>
                <div className="box">
                  <CircularProgressbar
                    // value={value}
                    maxValue={1}
                    text={`${value * 100}%`}
                    className="pink-color"
                  />
                </div>
              </div>
              <div className="action-btn">
                <div className="text-center w-100">
                  <button className="yellow-btn btn">
                    Continue with the education plan
                  </button>
                </div>
                <div
                  className="select-btn"
                  onClick={() =>
                    navigate(
                      `/${lang}/student-force-test/${sessionStorage.getItem("token")}`
                    )
                  }
                >
                  Selection of learning material
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default StudentMainAfterLogin;
