import React from "react";
import { useNavigate } from "react-router-dom";
import { Toast } from "primereact/toast";

function Logout() {
  const navigate = useNavigate();
  const toast = React.useRef(null);

  React.useEffect(() => {
    sessionStorage.clear();
    window?.webkit?.messageHandlers?.sessiontokencleared?.postMessage({
      message: "Hello! I'm React.",
    });
    // toast.current.show({
    //   severity: "warn",
    //   detail: "found messageHandlers and logged out",
    //   life: 2000,
    // });
    // setTimeout(() => {
    navigate(`/`);
    // }, 2000);
  }, [navigate]);

  return (
    <>
      <Toast ref={toast} />
    </>
  );
}

export default Logout;
