import React from 'react'
import Container from '../components/Custom/Container/Container'
import ResetPasswordData from '../components/ResetPassword/ResetPasswordData'

function ResetPassword() {
  return (
    <Container>
      <ResetPasswordData />
    </Container>
  )
}

export default ResetPassword