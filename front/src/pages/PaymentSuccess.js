import React from "react";
import Container from "../components/Custom/Container/Container";
import TestNextStep from "../components/ResultScreen/TestNextStep";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import axios from "axios";

function PaymentSuccess() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("cz");

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  React.useEffect(() => {
    const createCredit = async () => {
      const values = {
        amount: sessionStorage.getItem("amount"),
        iUserId: sessionStorage.getItem("iUserId"),
        iCreditDetailOfSchool: sessionStorage.getItem("totalSchools"),
        iCreditEductionProgram: sessionStorage.getItem("educationProgram"),
        iTransactionId: Math.floor(100000 + Math.random() * 900000),
      };
      const resData = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/stripe/credit-create`,
        values,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (
        resData &&
        resData.status === 200 &&
        resData.data &&
        resData.data.message
      ) {
        sessionStorage.removeItem("amount");
        sessionStorage.removeItem("totalSchools");
        sessionStorage.removeItem("educationProgram");
        sessionStorage.removeItem("iUserId");
        navigate(`/${lang}/settings/${sessionStorage.getItem("token")}`);
      }
    };
    createCredit();
  }, [lang, navigate]);

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 result-sec">
        <div className="self-asset-sec">
          <div className="details-left">
            <h1 className="sec-heading">{t("LBL_PAYMENT_ACCEPT_TITLE")}</h1>
            <p className="sub-capion">
              {/* {t("LBL_TEST_RESULT_CAPTION_TEXT")} */}
              {t("LBL_PAYMENT_ACCEPT_SUB_CAPTION")}
            </p>
            <br />
            <button
              className="green-btn btn"
              type="button"
              onClick={() =>
                navigate(`/${lang}/settings/${sessionStorage.getItem("token")}`)
              }
            >
              {t("LBL_PAYMENT_BACK_BUTTON")}
            </button>
          </div>
          <TestNextStep />
        </div>
      </div>
    </Container>
  );
}

export default PaymentSuccess;
