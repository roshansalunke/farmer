import React from "react";
import Container from "../components/Custom/Container/Container";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

function StudentForceTest() {
  const [selectedSub, setSelectedSub] = React.useState("C");
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("en");

  React.useEffect(() => {
    let lng = "en";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 forcetest-inst">
        <h1 className="sec-heading">
          {t("LBL_INFINITE_TREE_INSTRUCTION_TEXT")}
        </h1>
        <div className="instruction-box">
          <div className="image-box">
            <img
              src="/../assets/images/sova_new_clr 1.png"
              alt=""
              className="img-contain"
            />
          </div>
          <div className="box up">
            <p>
              {t("LBL_INFINITE_TREE_DESCRI_1")}
              {/* Welcome, first you need to pass 2 verification tests, one of which
              is about 45 minutes long. You can pause it and finish it later. */}
            </p>
          </div>
          {/* <div className="box down">
            <p>
              {t("LBL_INFINITE_TREE_DESCRI_2")}?
              Are you ready? What shall we start with?
            </p>
          </div> */}
        </div>
        <div className="self-asset-sec">
          <div className="opt-select-box">
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="exampleRadiosopt"
                id="exampleRadiosopt1"
                value="C"
                defaultChecked
                onChange={() => {
                  sessionStorage.setItem("eSubject", "C");
                  setSelectedSub("C");
                }}
              />
              <label className="form-check-label" htmlFor="exampleRadiosopt1">
                <span>{t("LBL_TASK_TABLE_TH_2")}</span>
                <div className="image-box">
                  <img
                    src="/../assets/images/fitness-kniha-ikona-300x300 2.png"
                    alt=""
                    className="img-contain"
                  />
                </div>
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="exampleRadiosopt"
                id="exampleRadiosopt1"
                value="M"
                onChange={() => {
                  sessionStorage.setItem("eSubject", "M");
                  setSelectedSub("M");
                }}
              />
              <label className="form-check-label" htmlFor="exampleRadiosopt1">
                <span>{t("LBL_TASK_TABLE_TH_3")}</span>
                <div className="image-box">
                  <img
                    src="/../assets/images/pngegg (kopie) 2.png"
                    alt=""
                    className="img-contain"
                  />
                </div>
              </label>
            </div>
          </div>
          <Link
            to={`/${lang}/student-infinite-trees/${sessionStorage.getItem(
              "token"
            )}`}
            state={{ eSubject: selectedSub }}
            className="skip align-self-end"
          >
            {t("LBL_TEST_RESULT_LINK_START_LEARNING_TEXT")}
            {/* Start Learning */}
          </Link>
          {/* <Link to={"/student-force-test"} className="skip align-self-end">
            skip the test and go on to study
          </Link> */}
        </div>
      </div>
    </Container>
  );
}

export default StudentForceTest;
