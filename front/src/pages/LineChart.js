import React from "react";
import CanvasJSReact from "@canvasjs/react-charts";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

function LineChart() {
  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "light2", // "light1", "dark1", "dark2"
    // title: {
    //   text: "Bounce Rate by Week of Year",
    // },
    axisY: {
      labelFormatter: function (e) {
        return "";
      },
      lineThickness: 0,
      gridThickness: 0,
      tickLength: 0,
      gridColor: "transparent",
      labelBackgroundColor: "transparent",
    },
    axisX: {
      lineThickness: 0,
      tickThickness: 0,
      gridColor: "transparent",
    },

    data: [
      {
        type: "line",
        visible: true,
        toolTipContent: "Week {x}: {y}%",
        dataPoints: [
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${1} : ${64} %`,
            x: 1,
            y: 64,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${2} : ${61} %`,
            x: 2,
            y: 61,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${3} : ${64} %`,
            x: 3,
            y: 64,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${4} : ${62} %`,
            x: 4,
            y: 62,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${5} : ${64} %`,
            x: 5,
            y: 64,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${6} : ${60} %`,
            x: 6,
            y: 60,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${7} : ${58} %`,
            x: 7,
            y: 58,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${8} : ${59} %`,
            x: 8,
            y: 59,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${9} : ${53} %`,
            x: 9,
            y: 53,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${10} : ${54} %`,
            x: 10,
            y: 54,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${11} : ${61} %`,
            x: 11,
            y: 61,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${12} : ${60} %`,
            x: 12,
            y: 60,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${13} : ${55} %`,
            x: 13,
            y: 55,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${14} : ${60} %`,
            x: 14,
            y: 60,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${15} : ${56} %`,
            x: 15,
            y: 56,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${16} : ${60} %`,
            x: 16,
            y: 60,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${17} : ${59} %`,
            x: 17,
            y: 59.5,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${18} : ${63} %`,
            x: 18,
            y: 63,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${19} : ${58} %`,
            x: 19,
            y: 58,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${20} : ${54} %`,
            x: 20,
            y: 54,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${21} : ${59} %`,
            x: 21,
            y: 59,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${22} : ${64} %`,
            x: 22,
            y: 64,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: `Week ${23} : ${59} %`,
            x: 23,
            y: 59,
          },
        ],
      },
    ],
  };

  return (
    <div>
      <CanvasJSChart options={options} />
    </div>
  );
}

export default LineChart;
