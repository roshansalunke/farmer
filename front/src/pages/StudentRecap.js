import React from "react";
import Container from "../components/Custom/Container/Container";

function StudentRecap() {
  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 custom-min-h recap-sec">
        <div className="inner-box row">
          <div className="col-lg-6">
            <div className="row align-items-center g-4">
              <div className="col-lg-12 col-sm-8">
                <div className="detail-sec">
                  <h1 className="sec-heading">Congratulations,</h1>
                  <h2 className="sub-font">the whole lesson is done!</h2>
                  <h3 className="sub-font">9 minutes - 93%</h3>
                  <p className="sub-font">Do you want to repeat any part?</p>
                </div>
              </div>
              <div className="col-lg-12 col-sm-4 detail-sec">
                <div className="image-box">
                  <img src="../assets/images/sova_new_clr 1.png" className="img-contain" alt="" />
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <ul className="test-list">
              <li>
                <div className="list-box">
                  <div className="name">
                    <div className="image-box">
                      <img src="../assets/images/icon_video_.png"  className="img-contain" alt="" />
                    </div>
                    <h3>Circle</h3>
                  </div>
                  <div>
                    <i className="fas fa-check-circle"></i>
                  </div>
                </div>
              </li>
              <li>
                <div className="list-box">
                  <div className="name">
                    <div className="image-box">
                      <img src="../assets/images/icon_video_.png"  className="img-contain" alt="" />
                    </div>
                    <h3>Circle circumference</h3>
                  </div>
                  <div>
                    <i className="fas fa-check-circle"></i>
                  </div>
                </div>
              </li>
              <li>
                <div className="list-box">
                  <div className="name">
                    <div className="image-box">
                      <img src="../assets/images/icon_calc.png" className="img-contain" alt="" />
                    </div>
                    <h3>Circle circumference</h3>
                  </div>
                  <div>
                    <p>(85%)</p>
                  </div>
                </div>
              </li>
              <li>
                <div className="list-box">
                  <div className="name">
                    <div className="image-box">
                      <img src="../assets/images/icon_video_.png" className="img-contain" alt="" />
                    </div>
                    <h3>Area of the circle</h3>
                  </div>
                  <div>
                    <i className="fas fa-check-circle"></i>
                  </div>
                </div>
              </li>
              <li>
                <div className="list-box">
                  <div className="name">
                    <div className="image-box">
                      <img src="../assets/images/icon_calc.png" className="img-contain" alt="" />
                    </div>
                    <h3>Area of the circle </h3>
                  </div>
                  <div>
                    <p>(85%)</p>
                  </div>
                </div>
              </li>
            </ul>
            <div className="info-box">Don't repeat, keep going!</div>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default StudentRecap;
