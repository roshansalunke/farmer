import React from "react";
import { useParams } from "react-router-dom";
import Container from "../components/Custom/Container/Container";
import axios from "axios";

function TermAndCondtion() {
  const { lang, pageCode } = useParams();
  const [content, setContent] = React.useState("");

  React.useEffect(() => {
    if ((lang, pageCode)) {
      (async function () {
        try {
          const resData = await axios.post(
            `${process.env.REACT_APP_API_URL}/api/setting/get-all-page-setting-data`,
            { vPageCode: pageCode, vLangCode: lang.toUpperCase() },
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          if (resData.data.code === "200") {
            setContent(resData.data.data.tLongContent);
          } else if (resData.status === 204) {
            setContent("");
          }
        } catch (err) {
          console.log(err);
        }
      })();
    }
  }, [lang, pageCode]);

  return (
    <Container>
      <div className="new-container">
        <pre
          className="white-card plr-50 pt-50 pb-50"
          dangerouslySetInnerHTML={{ __html: content }}
        ></pre>
      </div>
    </Container>
  );
}

export default TermAndCondtion;
