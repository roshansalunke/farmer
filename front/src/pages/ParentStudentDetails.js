import React from "react";
import Container from "../components/Custom/Container/Container";
import Overview from "../components/ParentsHome/Overview";
import Tabs from "../components/ParentStudentDetails/Tabs";

function ParentStudentDetails() {
  return (
    <Container>
      <div className="white-card pt-50 pb-50 plr-50 parent-details-page">
        <div className="p-student-details parent-main-box">
          <Overview />
          <Tabs />
        </div>
      </div>
    </Container>
  );
}

export default ParentStudentDetails;
