import Container from "../components/Custom/Container/Container";
import TestBody from "../components/TestBody/TestBody";

function Test() {
  return (
    <Container>
      <TestBody />
    </Container>
  );
}

export default Test;
