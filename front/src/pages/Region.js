import React from "react";
import Container from "../components/Custom/Container/Container";
import ChooseRegion from "../components/Region/ChooseRegion";
import MapReson from "../components/Region/MapReson";
import ModelFlow from "../components/Region/ModelFlow";

function Region() {
  return (
    <Container>
      <ChooseRegion />
      <div className="white-card plr-50">
        <MapReson />
        <ModelFlow />
      </div>
    </Container>
  );
}

export default Region;
