import React from 'react'
import Container from '../components/Custom/Container/Container'
import ResetPasswordData from '../components/ResetPasswordStudent/ResetPasswordData'

function ResetPasswordStudent() {
  return (
    <Container>
      <ResetPasswordData />
    </Container>
  )
}

export default ResetPasswordStudent