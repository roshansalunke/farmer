import Container from "../components/Custom/Container/Container";
import StudentTest from "../components/StudentSubjectTest/StudentSubjectTest";

function SubjectTest() {
  return (
    <Container>
      <StudentTest />
    </Container>
  );
}

export default SubjectTest;
