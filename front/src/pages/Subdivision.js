import React from "react";
import Container from "../components/Custom/Container/Container";
import SelectSubDivision from "../components/Subdivision/SelectSubDivision";
import SchoolList from "../components/Subdivision/SchoolList";
import TopSchoolList from "../components/Subdivision/TopSchoolList";
import ProgressChart from "../components/Subdivision/ProgressChart";

function Subdivision() {
  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 subdvision-page">
        <div className="region-num-sec">
          <SelectSubDivision />
          <div className="inner-box max-1300">
            <SchoolList />
            <TopSchoolList />
          </div>
          <ProgressChart />
        </div>
      </div>
    </Container>
  );
}

export default Subdivision;
