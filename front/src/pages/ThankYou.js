import React from "react";
import Container from "../components/Custom/Container/Container";
import { useTranslation } from "react-i18next";
import TestNextStep from "../components/ResultScreen/TestNextStep";

function ThankYou() {
  const { t } = useTranslation();

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 result-sec">
        <div className="self-asset-sec">
          <div className="details-left">
            <h1 className="sec-heading">{t("LBL_THANK_YOU_PAGE_TITLE")}</h1>
            <p className="sub-capion">
              {t("LBL_THANK_YOU_PAGE_DESCRIPTION")}
              {/* Thank you for registering with us! Your account has been created
              successfully. To activate your account and gain full access to our
              platform, please check your email inbox for a verification link.
              Click on the link in the email to complete the activation process.
              We look forward to having you as a valued member of our
              community!. */}
            </p>
          </div>
          <TestNextStep />
        </div>
      </div>
    </Container>
  );
}

export default ThankYou;
