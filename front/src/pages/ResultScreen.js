import React from "react";
import Container from "../components/Custom/Container/Container";
import TestResult from "../components/ResultScreen/TestResult";
import TestNextStep from "../components/ResultScreen/TestNextStep";

function ResultScreen() {
  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 result-sec">
        <div className="self-asset-sec">
          <TestResult />
          <TestNextStep />
        </div>
      </div>
    </Container>
  );
}

export default ResultScreen;
