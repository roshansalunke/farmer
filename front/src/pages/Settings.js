import React from "react";
import Container from "../components/Custom/Container/Container";
import AccountConnectSetting from "../components/Setting/AccountConnectSetting";
import ServiceBox from "../components/Setting/ServiceBox";
import ShoppingCart from "../components/Setting/ShoppingCart";

function Settings() {
  return (
    <Container>
      <AccountConnectSetting />
      <div className="white-card pt-50 pb-50 plr-50 test-lang-sec">
        <div className="setting-page">
          <ServiceBox />
          <ShoppingCart />
        </div>
      </div>
    </Container>
  );
}

export default Settings;
