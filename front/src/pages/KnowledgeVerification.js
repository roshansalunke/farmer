import React from "react";
import Container from "../components/Custom/Container/Container";
import SchoolChoiceOffCanvas from "../components/KnowledgeVerification/SchoolChoiceOffCanvas";
import SelfAssessment from "../components/KnowledgeVerification/SelfAssessment";
// import PageDetails from "../components/KnowledgeVerification/PageDetails";

function KnowledgeVerification() {
  return (
    <Container>
      <SchoolChoiceOffCanvas />
      <div className="white-card plr-50 pt-50 pb-50 knowledge-page">
          <SelfAssessment />
          {/* <PageDetails /> */}
        </div>
    </Container>
  );
}

export default KnowledgeVerification;
