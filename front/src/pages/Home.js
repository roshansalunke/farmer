import React from "react";
import Container from "../components/Custom/Container/Container";
import Banner from "../components/Home/Banner";
import StoryAndMission from "../components/Home/StoryAndMission";
import Approch from "../components/Home/Approch";
import HistoricResults from "../components/Home/HistoricResults";
import { useLocation, useNavigate } from "react-router-dom";

function Home() {
  const location = useLocation();
  const navigate = useNavigate();

  React.useEffect(() => {
    if (location.pathname === "/") {
      if (
        sessionStorage.getItem("langType") &&
        sessionStorage.getItem("langType") === "CZ"
      ) {
        navigate("/cz");
      } else if (
        sessionStorage.getItem("langType") &&
        sessionStorage.getItem("langType") === "EN"
      ) {
        navigate("/en");
      } else {
        navigate("/cz");
      }
    }
  }, [navigate, location]);

  return (
    <Container>
      <Banner />
      <div className="white-card plr-50">
        <StoryAndMission />
        <Approch />
        <HistoricResults />
      </div>
    </Container>
  );
}

export default Home;
