import React from "react";
import Container from "../components/Custom/Container/Container";
import Topic from "../components/StudentIntroTopic/Topic";

function StudentIntroTopic() {
  return (
    <Container>
      <Topic />
    </Container>
  );
}

export default StudentIntroTopic;
