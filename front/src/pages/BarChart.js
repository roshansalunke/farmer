import React from "react";
import CanvasJSReact from "@canvasjs/react-charts";

var CanvasJSChart = CanvasJSReact.CanvasJSChart;
function BarChart() {
  const options = {
    animationEnabled: true,
    title: {
      // text: "Operating Expenses of ACME",
      // fontFamily: "verdana",
    },
    axisX: {
      lineThickness: 0,
      tickThickness: 0,
      gridColor: "transparent",
    },
    axisY: {
      labelFormatter: function (e) {
        return "";
      },
      lineThickness: 0,
      gridThickness: 0,
      tickLength: 0,
      gridColor: "transparent",
      labelBackgroundColor: "transparent",
    },
    toolTip: {
      shared: true,
      reversed: true,
    },
    legend: {
      verticalAlign: "center",
      horizontalAlign: "right",
      reversed: true,
      cursor: "pointer",
    },
    data: [
      {
        type: "stackedColumn",
        name: "Elementary School",
        // showInLegend: true,
        // yValueFormatString: "#,###k",
        dataPoints: [
          {
            label: "School 1",
            y: 40,
            indexLabel: "40%",
            indexLabelPlacement: "inside",
          },
          {
            label: "School 2",
            y: 45,
            indexLabel: "45%",
            indexLabelPlacement: "inside",
          },
          {
            label: "School 3",
            y: 55,
            indexLabel: "55%",
            indexLabelPlacement: "inside",
          },
          {
            label: "School 4",
            y: 30,
            indexLabel: "30%",
            indexLabelPlacement: "inside",
          },
        ],
      },
      {
        type: "stackedColumn",
        name: "High School",
        // showInLegend: true,
        // yValueFormatString: "#,###k",
        dataPoints: [
          {
            label: "School 1",
            y: 40,
            indexLabel: "40%",
            indexLabelPlacement: "inside",
          },
          {
            label: "School 2",
            y: 42,
            indexLabel: "42%",
            indexLabelPlacement: "inside",
          },
          {
            label: "School 3",
            y: 37,
            indexLabel: "37%",
            indexLabelPlacement: "inside",
          },
          {
            label: "School 4",
            y: 30,
            indexLabel: "30%",
            indexLabelPlacement: "inside",
          },
        ],
      },
    ],
  };

  const options2 = {
    animationEnabled: true,
    theme: "light2", // "light1", "dark1", "dark2"
    // title: {
    //   text: "Bounce Rate by  of Year",
    // },
    axisY: {
      labelFormatter: function (e) {
        return "";
      },
      lineThickness: 0,
      gridThickness: 0,
      tickLength: 0,
      gridColor: "transparent",
      labelBackgroundColor: "transparent",
    },
    axisX: {
      lineThickness: 0,
      tickThickness: 0,
      gridColor: "transparent",
    },

    data: [
      {
        type: "column",
        visible: true,
        toolTipContent: "{y}%",
        dataPoints: [
          {
            indexLabelPlacement: "auto",
            indexLabel: `${10}%`,
            label: "School 1",
            y: 10,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: ` ${15}%`,
            label: "School 2",
            y: 15,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: ` ${25}%`,
            label: "School 3",
            y: 25,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: ` ${30}%`,
            label: "School 4",
            y: 30,
          },
          {
            indexLabelPlacement: "auto",
            indexLabel: ` ${28}%`,
            label: "School 5",
            y: 28,
          },
        ],
      },
    ],
  };
  return (
    <div>
      <CanvasJSChart options={options} />
      <CanvasJSChart options={options2} />
    </div>
  );
}
export default BarChart;
