import React from "react";
import Container from "../components/Custom/Container/Container";
import Overview from "../components/ParentsHome/Overview";
import FullDetail from "../components/ParentSchoolDetails/FullDetail";

function ParentSchoolDetails() {
  return (
    <Container>
      <div className="white-card pt-50 pb-50 plr-50 parent-details-page ">
        <div className="p-school-details parent-main-box">
          <Overview />
          <FullDetail />
        </div>
      </div>
    </Container>
  );
}

export default ParentSchoolDetails;
