import React from "react";
import Container from "../components/Custom/Container/Container";
import TestNextStep from "../components/ResultScreen/TestNextStep";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import axios from "axios";

function Success() {
  const { t } = useTranslation();
  const { search } = useLocation();
  const vUniqueCode = new URLSearchParams(search).get("vUniqueCode");

  React.useEffect(() => {
    const verifyAccount = async (vAuthCode) => {
      try {
        await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/verify`,
          { vAuthCode: vAuthCode },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
      } catch (err) {
      }
    };
    if (vUniqueCode && vUniqueCode !== null && vUniqueCode.length > 0) {
      verifyAccount(vUniqueCode);
    }
  }, [vUniqueCode]);

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 result-sec">
        <div className="self-asset-sec">
          <div className="details-left">
            <h1 className="sec-heading">{t("LBL_STUDENT_SUCCESS_HEADING")}</h1>
            <p className="sub-capion">
              {t("LBL_SUCCESS_PAGE_SUB_CAPTION")}
              {/* Congratulations! Your email address has been successfully
              verified, and your account is now fully activated. You can now log
              in to our platform and start enjoying all the features and
              benefits we have to offer. We're excited to have you as a part of
              our community. */}
            </p>
            <br />
            <Link
              className="green-btn btn"
              type="button"
              data-bs-toggle="offcanvas"
              data-bs-target="#offcanvasRightlogin"
              aria-controls="offcanvasRightlogin"
              id="open-login-canvas-id"
            >
              {t("LBL_LOGIN_OFFCANVAS_SUBMIT_BUTTON_TEXT")}
            </Link>
          </div>
          <TestNextStep />
        </div>
      </div>
    </Container>
  );
}

export default Success;
