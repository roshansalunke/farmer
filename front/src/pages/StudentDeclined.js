import React from "react";
import Container from "../components/Custom/Container/Container";
import TestNextStep from "../components/ResultScreen/TestNextStep";
import { Link, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import axios from "axios";

function StudentDeclined() {
  const { t } = useTranslation();
  const { search } = useLocation();
  const vUserRelationToken = new URLSearchParams(search).get(
    "vUserRelationToken"
  );

  React.useEffect(() => {
    const verifyAccount = async (vUserRelationToken) => {
      try {
        await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/updateAcceptedAndDeclineRequest`,
          {
            vUserRelationToken: vUserRelationToken,
            eStudentStatus: "Decline",
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
      } catch (err) {}
    };
    if (
      vUserRelationToken &&
      vUserRelationToken !== null &&
      vUserRelationToken.length > 0
    ) {
      verifyAccount(vUserRelationToken);
    }
  }, [vUserRelationToken]);

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 result-sec">
        <div className="self-asset-sec">
          <div className="details-left">
            <h1 className="sec-heading">{t("LBL_STUDENT_SUCCESS_HEADING")}</h1>
            <p className="sub-capion">
              {t("LBL_STUDENT_DECLINE_SUB_CAPTION")}
              {/* Thank you for your prompt response regarding the recent request.
              We understand that circumstances may vary, and we respect your
              decision to decline the request. */}
            </p>
            <br />
            <Link
              className="green-btn btn"
              type="button"
              data-bs-toggle="offcanvas"
              data-bs-target="#offcanvasRightlogin"
              aria-controls="offcanvasRightlogin"
              id="open-login-canvas-id"
            >
               {t("LBL_LOGIN_OFFCANVAS_SUBMIT_BUTTON_TEXT")}
            </Link>
          </div>
          <TestNextStep />
        </div>
      </div>
    </Container>
  );
}

export default StudentDeclined;
