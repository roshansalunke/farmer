import React from "react";
import Container from "../components/Custom/Container/Container";

function StudentProgressLadders() {
  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 custom-min-h progress-ladder-sec">
        <h1 className="sec-heading">Leaderboards</h1>
        <div className="self-asset-sec">
          <div className="details-left d-block">
            <ul className="nav nav-tabs" id="myTab" role="tablist">
              <li className="nav-item" role="presentation">
                <button className="nav-link active" id="ladder-community-tab" data-bs-toggle="tab" data-bs-target="#ladder-community" type="button" role="tab" aria-controls="ladder-community" aria-selected="true">Community</button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link" id="ladder-district-tab" data-bs-toggle="tab" data-bs-target="#ladder-district" type="button" role="tab" aria-controls="ladder-district" aria-selected="false">District</button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link" id="ladder-country-tab" data-bs-toggle="tab" data-bs-target="#ladder-country" type="button" role="tab" aria-controls="ladder-country" aria-selected="false">Country</button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link" id="ladder-republic-tab" data-bs-toggle="tab" data-bs-target="#ladder-republic" type="button" role="tab" aria-controls="ladder-republic" aria-selected="false">Republic</button>
              </li>
            </ul>
            <div className="tab-content" id="myTabContent">
              <div className="tab-pane fade show active" id="ladder-community" role="tabpanel" aria-labelledby="ladder-community-tab">
                <div className="tab-in-box">
                  <h3>The best improvement in the community</h3>
                  <ul>
                    <li><p>Child 1</p><p className="green-color">+ 10%</p></li>
                    <li><p>Child 1</p><p className="green-color">+ 10%</p></li>
                    <li><p>Child 1</p><p className="green-color">+ 10%</p></li>
                  </ul>
                </div>
                <div className="tab-in-box">
                  <h3>The most active in the community</h3>
                  <ul>
                    <li><p>Child 1</p><p className="green-color">14 lessons</p></li>
                    <li><p>Child 1</p><p className="green-color">13 lessons</p></li>
                    <li><p>Child 1</p><p className="green-color">11 lessons</p></li>
                  </ul>
                </div>
                <div className="tab-in-box">
                  <h3>The most active schools</h3>
                  <ul>
                    <li><p>School 1, district</p><p className="green-color">some calculation</p></li>
                    <li><p>School 2, district</p><p className="pink-color">some calculation</p></li>
                    <li><p>School 3, district</p><p className="green-color">some calculation</p></li>
                  </ul>
                </div>
              </div>
              <div className="tab-pane fade" id="ladder-district" role="tabpanel" aria-labelledby="ladder-district-tab">
                <div className="tab-in-box">
                  <h3>The best improvement in the district</h3>
                  <ul>
                    <li><p>Child 1</p><p className="green-color">+ 10%</p></li>
                    <li><p>Child 1</p><p className="green-color">+ 10%</p></li>
                    <li><p>Child 1</p><p className="green-color">+ 10%</p></li>
                  </ul>
                </div>
                <div className="tab-in-box">
                  <h3>The most active in the district</h3>
                  <ul>
                    <li><p>Child 1</p><p className="green-color">14 lessons</p></li>
                    <li><p>Child 1</p><p className="green-color">13 lessons</p></li>
                    <li><p>Child 1</p><p className="green-color">11 lessons</p></li>
                  </ul>
                </div>
                <div className="tab-in-box">
                  <h3>The most active schools</h3>
                  <ul>
                    <li><p>School 1, district</p><p className="green-color">some calculation</p></li>
                    <li><p>School 2, district</p><p className="pink-color">some calculation</p></li>
                    <li><p>School 3, district</p><p className="green-color">some calculation</p></li>
                  </ul>
                </div>
              </div>
              <div className="tab-pane fade" id="ladder-country" role="tabpanel" aria-labelledby="ladder-country-tab">...</div>
              <div className="tab-pane fade" id="ladder-republic" role="tabpanel" aria-labelledby="ladder-republic-tab">...</div>
            </div>
          </div>
          <div className="details-right">
            <div className="image-box">
              <img
                src="../assets/images/sova_new_clr 1.png"
                alt=""
                className="img-contain"
              />
            </div>
            <p>
              text for owl, specific for this page, because here we are starting with
              different point of view
            </p>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default StudentProgressLadders;
