import React from "react";
import Container from "../components/Custom/Container/Container";
import TestNextStep from "../components/ResultScreen/TestNextStep";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

function StudentDeclined() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [lang, setLang] = React.useState("cz");

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  return (
    <Container>
      <div className="white-card plr-50 pt-50 pb-50 result-sec">
        <div className="self-asset-sec">
          <div className="details-left">
            <h1 className="sec-heading">{t("LBL_PAYMENT_DECLINE_TITLE")}</h1>
            <p className="sub-capion">{t("LBL_PAYMENT_DECLINE_SUB_CAPTION")}</p>
            <br />
            <button
              className="green-btn btn"
              type="button"
              onClick={() =>
                navigate(`/${lang}/settings/${sessionStorage.getItem("token")}`)
              }
            >
              {t("LBL_PAYMENT_BACK_BUTTON")}
            </button>
          </div>
          <TestNextStep />
        </div>
      </div>
    </Container>
  );
}

export default StudentDeclined;
