import axios from "axios";
import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { ConfirmDialog } from "primereact/confirmdialog";
import { Dialog } from "primereact/dialog";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useTranslation } from "react-i18next";

// import { tuple } from "yup";

export default function GLoginVerification() {
  const location = useLocation();
  const navigate = useNavigate();
  const [visible, setVisible] = React.useState(false);
  const [accountType, setAccountType] = React.useState("");
  const [googleRes, setGoogleRes] = React.useState({});
  const [lang, setLang] = React.useState("cz");
  const { t } = useTranslation();

  const singUpValues = {
    vFirstName: "",
    vLastName: "",
    vEmail: "",
    vMobileNumber: "",
    eAccountType: "",
    vParentEmail: "",
  };
  const [singUpValue, setSignUpValue] = React.useState(singUpValues);

  // here is phone regexp through match the numbers
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  // here is register basis schema
  const registerSchema = Yup.object().shape({
    vFirstName: Yup.string()
      .required(t("LBL_SINGUP_NAME_ERROR_MESSAGE"))
      .min(2, t("LBL_SINGUP_NAME_VALID_ERROR_MESSAGE")),
    vLastName: Yup.string()
      .required(t("LBL_SINGUP_SURNAME_ERROR_MESSAGE"))
      .min(2, t("LBL_SINGUP_SURNAME_VALID_ERROR_MESSAGE")),
    vEmail: Yup.string()
      .required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE"))
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE")),
    vParentEmail: Yup.string()
      .email(t("LBL_SINGUP_EMAIL_VALID_ERROR_MESSAGE"))
      .when("eAccountType", {
        is: (v) => v === "Student",
        then: (schema) => schema.required(t("LBL_SINGUP_EMAIL_ERROR_MESSAGE")),
      }),
    vMobileNumber: Yup.string()
      .required(t("LBL_SIGNUP_PHONE_NO_ERROR_MESSAGE"))
      .matches(phoneRegExp, t("LBL_SIGNUP_PHONE_NO_VALID_ERROR_MESSAGE")),
    eAccountType: Yup.string().required(t("LBL_SIGNUP_ACCOUNT_TYPE_VALID_ERROR_MESSAGE")),
  });

  React.useEffect(() => {
    let lng = "cz";
    if (sessionStorage.getItem("langType")) {
      if (sessionStorage.getItem("langType") === "CZ") {
        lng = "cz";
      } else if (sessionStorage.getItem("langType") === "EN") {
        lng = "en";
      }
    }
    setLang(lng);
  }, []);

  const handleRemoveOverFlow = React.useCallback(() => {
    const element = document.getElementsByTagName("body");
    if (element && element.length > 0) {
      element[0].style.overflow = "unset";
      element[0].style.paddingRight = "0";
      element[0].removeAttribute("data-bs-overflow");
    }
  }, []);

  React.useEffect(() => {
    const getData = async () => {
      const params = new URLSearchParams(location.hash);
      const authToken = params.get("#access_token");
      try {
        if (authToken) {
          const res = await axios.get(
            `https://www.googleapis.com/oauth2/v3/userinfo?access_token=${authToken}`
          );
          if (res && res.status === 200) {
            setGoogleRes(res);
            try {
              const authRes = await axios.post(
                `${process.env.REACT_APP_API_URL}/api/user/social_authentication`,
                {
                  vEmail: res.data.email,
                  vLangCode: lang,
                  vGoogleId: res.data.sub,
                },
                {
                  headers: {
                    "Content-Type": "application/json",
                  },
                }
              );
              if (authRes.status === 204) {
                setSignUpValue((pre) => ({
                  ...pre,
                  vFirstName: res.data.name.split(" ").at(0),
                  vLastName: res.data.name.split(" ").at(1),
                  vEmail: res.data.email,
                }));
                setVisible(true);
              } else if (authRes.data.code === "200") {
                sessionStorage.setItem("token", authRes.data.data.token);
                sessionStorage.setItem("iUserId", authRes.data.data.user);
                setVisible(false);
                const element = document.getElementsByTagName("body");
                if (element && element.length > 0) {
                  element[0].style.overflow = "unset";
                  element[0].style.paddingRight = "0";
                  element[0].removeAttribute("data-bs-overflow");
                }
                // let url = authRes.data.data.url.split("/");
                // url.splice(3, 0, lang);
                // window.location.href = url.join("/");
                window.location.href = authRes.data.data.url;
              }
            } catch (err) {
              console.log(err);
            }
          }
        }
      } catch (err) {
        console.warn(err);
      }
    };
    getData();
  }, [location, lang, navigate]);

  const handleSubmit = async (values, { resetForm }) => {
    if (googleRes && googleRes.status === 200) {
      try {
        const authRes = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/user/social_authentication`,
          {
            vEmail: googleRes.data.email,
            vLangCode: lang,
            vGoogleId: googleRes.data.sub,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        if (authRes.status === 204) {
          let data = {
            vEmail: googleRes.data.email,
            vFirstName: values.vFirstName,
            vLastName: values.vLastName,
            vGoogleId: googleRes.data.sub,
            vParentEmail: values.vParentEmail,
            vMobileNumber: values.vMobileNumber,
            eAccountType: values.eAccountType,
            vLangCode: lang,
          };

          try {
            let res = await axios.post(
              `${process.env.REACT_APP_API_URL}/api/user/social_register`,
              data,
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
            if (res && res.data.code === "200") {
              sessionStorage.setItem("token", res.data.data.token);
              sessionStorage.setItem("iUserId", res.data.data.user);

              setVisible(false);
              window.location.href = res.data.data.url;
              handleRemoveOverFlow();
              // let url = res.data.data.url.split("/");
              // url.splice(3, 0, lang);
              // window.location.href = url.join("/");
              // navigate(`/student-auth-main/${res.data.data.token}`);
            }
          } catch (err) {
            console.log(err);
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
    resetForm();
  };

  return (
    <>
      <ConfirmDialog />
      <div className="card flex justify-content-center">
        <Dialog
          visible={visible}
          draggable={false}
          resizable={false}
          modal
          // footer={footerContent}
          style={{ width: "40rem" }}
          onHide={() => {
            setVisible(false);
            setAccountType("");
          }}
          header={<h4>{t("LBL_SINGUP_OFFCANVAS_MODAL_TITLE")}</h4>}
        >
          <Formik
            initialValues={singUpValue}
            validationSchema={registerSchema}
            onSubmit={handleSubmit}
          >
            {({ errors, touched, handleChange, values, resetForm }) => (
              <Form className="add-text">
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_NAME_LABEL")}</label>
                  {/* <label>Name</label> */}
                  <Field
                    name="vFirstName"
                    onChange={handleChange}
                    value={values.vFirstName}
                    // value={googleRes && googleRes.data.name.split(" ").at(0)}
                  />
                  {errors.vFirstName && touched.vFirstName ? (
                    <div className="text-danger">{errors.vFirstName}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_SURNAME_LABEL")}</label>
                  {/* <label>vLastName</label> */}
                  <Field
                    name="vLastName"
                    onChange={handleChange}
                    value={values.vLastName}
                  />
                  {errors.vLastName && touched.vLastName ? (
                    <div className="text-danger">{errors.vLastName}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_EMAIL_LABEL")}</label>
                  {/* <label>vEmail</label> */}
                  <Field
                    name="vEmail"
                    onChange={handleChange}
                    value={values.vEmail}
                    type="email"
                  />
                  {errors.vEmail && touched.vEmail ? (
                    <div className="text-danger">
                      {errors.vEmail ? errors.vEmail : ""}
                    </div>
                  ) : null}
                </div>
                <div className="form-group">
                  <label>{t("LBL_SINGUP_OFFCANVAS_PHONE_NO_LABEL")}</label>
                  {/* <label>Telephone number</label> */}
                  <Field name="vMobileNumber" />
                  {errors.vMobileNumber && touched.vMobileNumber ? (
                    <div className="text-danger">{errors.vMobileNumber}</div>
                  ) : null}
                </div>

                <div className="form-group">
                  {/* <label>Telephone number</label> */}
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    {t("LBL_SINGUP_OFFCANVAS_PARENT_OR_STUDENT_TEXT")}?
                  </label>
                  <div className="d-flex flex-wrap ">
                    <div className="form-check align-items-center me-3">
                      <label
                        className="form-check-label  mb-0 me-2"
                        htmlFor="flexCheckDefault1"
                      >
                        {t("LBL_SINGUP_OFFCANVAS_PARENT_TEXT")}
                      </label>
                      <Field
                        type="radio"
                        name="eAccountType"
                        id="flexCheckDefault1"
                        value="Parent"
                        onChange={(e) => {
                          setAccountType("");
                          handleChange(e);
                        }}
                      />
                    </div>
                    <div className="form-check align-items-center">
                      <label
                        className="form-check-label mb-0 me-2"
                        htmlFor="flexCheckDefault2"
                      >
                        {t("LBL_SINGUP_OFFCANVAS_STUDENT_TEXT")}
                      </label>
                      <Field
                        type="radio"
                        name="eAccountType"
                        id="flexCheckDefault2"
                        value="Student"
                        onChange={(e) => {
                          setAccountType(e.target.value);
                          handleChange(e);
                        }}
                      />
                    </div>
                  </div>
                  {errors.eAccountType && touched.eAccountType ? (
                    <div className="text-danger">{errors.eAccountType}</div>
                  ) : null}
                </div>
                {accountType === "Student" ? (
                  <div className="form-group">
                    <label>
                      {t("LBL_SINGUP_OFFCANVAS_PARENT_EMAIL_LABEL")}
                    </label>
                    <Field name="vParentEmail" type="email" />
                    <div className="text-danger">
                      {errors.vParentEmail && touched.vParentEmail ? (
                        <div className="text-danger">{errors.vParentEmail}</div>
                      ) : null}
                    </div>
                  </div>
                ) : (
                  <></>
                )}

                <button type="submit" className="green-btn btn">
                  {t("LBL_SINGUP_OFFCANVAS_BUTTON")}
                  {/* SIGN UP */}
                </button>
              </Form>
            )}
          </Formik>
        </Dialog>
      </div>
    </>
  );
}
