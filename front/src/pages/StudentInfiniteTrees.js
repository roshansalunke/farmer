import React from "react";
import Container from "../components/Custom/Container/Container";
import Lesson from "../components/StudentInfiniteTrees/Lesson";

function StudentInfiniteTrees() {
  return (
    <Container>
      <Lesson />
    </Container>
  );
}

export default StudentInfiniteTrees;
