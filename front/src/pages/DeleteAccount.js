import React from 'react'
import Container from '../components/Custom/Container/Container'
import DeleteUser from '../components/DeleteAccount/DeleteUser'

function DeleteAccount() {
  return (
    <Container>
      <DeleteUser />
    </Container>
  )
}

export default DeleteAccount