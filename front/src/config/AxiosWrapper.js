import axios from "axios";
import { useNavigate } from "react-router-dom";


const AxiosWrapper = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}`, // our API base URL
});

// Request interceptor for adding the bearer token
AxiosWrapper.interceptors.request.use(
  (config) => {
    const token = sessionStorage.getItem("token"); 
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Response interceptor for checking the bearer token
AxiosWrapper.interceptors.response.use(
  (response) => {
    const navigate = useNavigate();
    const headers = response.headers;
    const authToken = headers.get("Authorization");

    if (authToken) {
      // If a token exists, you can save it to local storage or state
      // For example, save it to local storage for later use
      localStorage.setItem("token", authToken);
    } else {
      navigate("/logout");
    }

    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default AxiosWrapper;
