import React from "react";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import CheckoutForm from "./CheckoutForm";
import axios from "axios";
import { useSelector } from "react-redux";

const stripePromise = loadStripe(
  "pk_test_51OWJilHo8SaRmVONhASF34cP1bYns0omxkEk30jwSp1qjDIftoKyStYI0bH0fYAKzQws7Rwnvh4TmsR3I5MuSQg600F1Je4HBt"
);

function Stripe() {
  const [clientSecret, setClientSecret] = React.useState("");
  const [paymentIndentId, setPaymentIndentId] = React.useState();
  const { totalAmount } = useSelector((state) => state.shoppingCart);

  React.useEffect(() => {
    async function createPaymentIntent(amount) {
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/stripe/create-payment-intent`,
        {
          amount: amount,
        }
      );
      // console.log(res.data);
      const { data, error } = res;
      if (error) {
        // console.log(error);
      } else {
        if (data && data.data && data.data.client_secret) {
          setClientSecret(data.data.client_secret);
          setPaymentIndentId(data.data.id);
        }
      }
    }
    if (
      totalAmount &&
      Object.keys(totalAmount).length > 0 &&
      totalAmount.amount
    ) {
      createPaymentIntent(totalAmount.amount);
    }
  }, [totalAmount]);

  // console.log(clientSecret);

  return (
    clientSecret &&
    stripePromise && (
      <Elements stripe={stripePromise} options={{ clientSecret }}>
        <CheckoutForm
          clientSecret={clientSecret}
          paymentIndentId={paymentIndentId}
        />
      </Elements>
    )
  );
}

export default Stripe;
