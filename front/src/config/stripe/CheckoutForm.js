import React from "react";
import {
  useStripe,
  useElements,
  // PaymentElement,
  CardElement,
} from "@stripe/react-stripe-js";
import { Toast } from "primereact/toast";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { SetPayableAmount } from "../../store/action/shoppingCartActions";

const CheckoutForm = ({ clientSecret, subscription, paymentIndentId }) => {
  const stripe = useStripe();
  const dispatch = useDispatch();
  const elements = useElements();
  const { userByTokenData } = useSelector((state) => state.user);
  const toast = React.useRef(null);
  const { totalAmount } = useSelector((state) => state.shoppingCart);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const cardElement = elements.getElement(CardElement);
    if (!stripe || !elements) {
      return <>No data</>;
    }
    const { error, paymentIntent } = await stripe.confirmCardPayment(
      clientSecret,
      {
        payment_method: {
          card: cardElement,
        },
      }
    );

    if (error) {
      toast.current.show({
        severity: "warn",
        detail: "Something went wrong",
        life: 3000,
      });
    }

    if (
      paymentIntent &&
      Object.keys(paymentIntent).length > 0 &&
      userByTokenData &&
      userByTokenData.code === "200" &&
      userByTokenData.data &&
      Object.keys(userByTokenData.data).length > 0 &&
      totalAmount &&
      Object.keys(totalAmount).length > 0 &&
      totalAmount.amount
    ) {
      const values = {
        amount: totalAmount.amount,
        iUserId: userByTokenData.data.iUserId,
        iCreditDetailOfSchool: totalAmount.totalSchools,
        iCreditEductionProgram: totalAmount.educationProgram,
        iTransactionId: paymentIntent.id,
      };
      const resData = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/stripe/credit-create`,
        values,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (
        resData &&
        resData.status === 200 &&
        resData.data &&
        resData.data.message
      ) {
        dispatch(
          SetPayableAmount({
            visible: true,
            data: { ...resData.data, amount: totalAmount.amount },
          })
        );
        toast.current.show({
          severity: "success",
          
          detail: resData.data.message,
          life: 3000,
        });
      }
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <Toast ref={toast} />
      <CardElement />
      <button
        className="yellow-btn btn"
        type="submit"
        disabled={!stripe}
        data-bs-target="#paymentdetails2"
        data-bs-toggle="modal"
      >
        Pay&nbsp;
        {totalAmount &&
        Object.keys(totalAmount).length > 0 &&
        totalAmount.amount
          ? totalAmount.amount
          : ""}
        &nbsp; Kč
      </button>
    </form>
  );
};

export default CheckoutForm;
