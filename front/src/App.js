import React, { Suspense } from "react";
import {
  Route,
  Routes,
  BrowserRouter as Router,
  // useNavigate,
} from "react-router-dom";
import i18n from "i18next";
const Home = React.lazy(() => import("./pages/Home"));
const Logout = React.lazy(() => import("./pages/Logout"));
const DeleteAccount = React.lazy(() => import("./pages/DeleteAccount"));
const KnowledgeVerification = React.lazy(() =>
  import("./pages/KnowledgeVerification")
);
const Test = React.lazy(() => import("./pages/Test"));
const Region = React.lazy(() => import("./pages/Region"));
const ResultScreen = React.lazy(() => import("./pages/ResultScreen"));
const Subdivision = React.lazy(() => import("./pages/Subdivision"));
const ParentSchoolDetails = React.lazy(() =>
  import("./pages/ParentSchoolDetails")
);
const ParentsHome = React.lazy(() => import("./pages/ParentsHome"));
const Settings = React.lazy(() => import("./pages/Settings"));
const ParentStudentDetails = React.lazy(() =>
  import("./pages/ParentStudentDetails")
);
const LineChart = React.lazy(() => import("./pages/LineChart"));
const BarChart = React.lazy(() => import("./pages/BarChart"));
const RangeColumnChart = React.lazy(() => import("./pages/RangeColumnChart"));
const ThankYou = React.lazy(() => import("./pages/ThankYou"));
const Success = React.lazy(() => import("./pages/Success"));
const StudentForceTest = React.lazy(() => import("./pages/StudentForceTest"));
const StudentInfiniteTrees = React.lazy(() =>
  import("./pages/StudentInfiniteTrees")
);
const StudentProgressLadders = React.lazy(() =>
  import("./pages/StudentProgressLadders")
);
const StudentRecap = React.lazy(() => import("./pages/StudentRecap"));
const StudentIntroTopic = React.lazy(() => import("./pages/StudentIntroTopic"));
const StudentLessonDone = React.lazy(() => import("./pages/StudentLessonDone"));
const StudentTest = React.lazy(() => import("./pages/StudentTest"));
const StudentAccepted = React.lazy(() => import("./pages/StudentAccepted"));
const StudentDeclined = React.lazy(() => import("./pages/StudentDeclined"));
const ParentAccepted = React.lazy(() => import("./pages/ParentAccepted"));
const ParentDeclined = React.lazy(() => import("./pages/ParentDeclined"));
const StudentMainAfterLogin = React.lazy(() =>
  import("./pages/StudentMainAfterLogin")
);
const StudentSubjectTest = React.lazy(() => import("./pages/SubjectTest"));
const ResetPassword = React.lazy(() => import("./pages/ResetPassword"));
const ResetPasswordStudent = React.lazy(() =>
  import("./pages/ResetPasswordStudent")
);
const GLoginVerification = React.lazy(() =>
  import("./pages/GLoginVerification")
);
const TermAndCondtion = React.lazy(() => import("./pages/TermAndConditon"));
const PaymentDeclined = React.lazy(() => import("./pages/PaymentDeclined"));
const PaymentSuccess = React.lazy(() => import("./pages/PaymentSuccess"));

// function BackToHomePage() {
//   const nav = useNavigate();
//   React.useEffect(() => {
//     sessionStorage.clear();
//     nav("/");
//   }, [nav]);
// }

function App() {
  React.useEffect(() => {
    if (sessionStorage.getItem("langType")) {
      i18n.changeLanguage(sessionStorage.getItem("langType"));
    }
  }, []);

  return (
    <Router>
      <Suspense fallback={"Loading ..."}>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/:lang" element={<Home />} />
          <Route path="/logout" element={<Logout />} />
          <Route
            path="/delete-account/:vJWTToken"
            element={<DeleteAccount />}
          />
          <Route path="/thank-you" element={<ThankYou />} />
          <Route path="/success" element={<Success />} />
          <Route
            path="/:lang/knowledge-verification/:vJWTToken"
            element={<KnowledgeVerification />}
          />
          <Route
            path="/:lang/knowledge-verification-l"
            element={<KnowledgeVerification />}
          />
          <Route path="/:lang/test" element={<Test />} />
          <Route path="/:lang/student-test" element={<StudentSubjectTest />} />
          <Route path="/:lang/region" element={<Region />} />
          <Route path="/:lang/subdivision" element={<Subdivision />} />
          <Route path="/result-screen" element={<ResultScreen />} />
          <Route
            path="/:lang/parent-school-detail/:vJWTToken"
            element={<ParentSchoolDetails />}
          />
          <Route path="/parents-home" element={<ParentsHome />} />
          <Route path="/:lang/settings/:vJWTToken" element={<Settings />} />
          <Route
            path="/:lang/student-force-test/:vJWTToken"
            element={<StudentForceTest />}
          />
          <Route
            path="/:lang/student-infinite-trees/:vJWTToken"
            element={<StudentInfiniteTrees />}
          />
          <Route
            path="/:lang/payment-declined/:vJWTToken"
            element={<PaymentDeclined />}
          />
          <Route
            path="/:lang/payment-sucess/:vJWTToken"
            element={<PaymentSuccess />}
          />
          <Route
            path="/student-progress-ladders/:vJWTToken"
            element={<StudentProgressLadders />}
          />
          <Route path="/student-recap/:vJWTToken" element={<StudentRecap />} />
          <Route
            path="/:lang/student-intro-topic/:vJWTToken"
            element={<StudentIntroTopic />}
          />
          <Route
            path="/student-lesson-done/:vJWTToken"
            element={<StudentLessonDone />}
          />
          <Route path="/student-Test/:vJWTToken" element={<StudentTest />} />
          <Route
            path="/:lang/student-auth-main/:vJWTToken"
            element={<StudentMainAfterLogin />}
          />
          <Route
            path="/:lang/parents-student-detail/:vJWTToken"
            // path="/parents-student-detail"
            element={<ParentStudentDetails />}
          />
          {/* Temp routes  */}
          <Route path="/line-chart" element={<LineChart />} />
          <Route path="/bar-chart" element={<BarChart />} />
          <Route path="/range-column-chart" element={<RangeColumnChart />} />
          <Route path="/login" element={<GLoginVerification />} />
          <Route
            path="/:lang/student/accepted/:vUserRelationToken"
            element={<StudentAccepted />}
          />
          <Route
            path="/:lang/student/declined/:vUserRelationToken"
            element={<StudentDeclined />}
          />
          <Route
            path="/:lang/parent/accepted/:vUserRelationToken"
            element={<ParentAccepted />}
          />
          <Route
            path="/:lang/parent/declined/:vUserRelationToken"
            element={<ParentDeclined />}
          />
          <Route
            path="/:lang/reset-password/:authToken"
            element={<ResetPassword />}
          />
          <Route
            path="/:lang/reset-password-student/:authToken"
            element={<ResetPasswordStudent />}
          />
          <Route path="/:lang/page/:pageCode" element={<TermAndCondtion />} />
          {/* <Route path="*" element={<BackToHomePage />} /> */}
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
