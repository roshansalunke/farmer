import axios from "axios";
import {
  GET_ALL_MATH_LESSON_REQUEST,
  GET_ALL_MATH_LESSON_SUCCESS,
  GET_ALL_MATH_LESSON_FAILURE,
  GET_ALL_CZ_LESSON_REQUEST,
  GET_ALL_CZ_LESSON_SUCCESS,
  GET_ALL_CZ_LESSON_FAILURE,
  GET_MATH_LESSON_REQUEST,
  GET_MATH_LESSON_SUCCESS,
  GET_MATH_LESSON_FAILURE,
  GET_CZ_LESSON_REQUEST,
  GET_CZ_LESSON_SUCCESS,
  GET_CZ_LESSON_FAILURE,
  RESET_LESSON,
  GET_ALL_VIEW_LESSON_REQUEST,
  GET_ALL_VIEW_LESSON_SUCCESS,
  GET_ALL_VIEW_LESSON_FAILURE,
  GET_LESSON_TEST_ATTEMP_LIST_REQUEST,
  GET_LESSON_TEST_ATTEMP_LIST_FAILURE,
  GET_LESSON_TEST_ATTEMP_LIST_SUCCESS,
} from "../constants/studentTopicLesson";

export const GetAllMathLesson = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_MATH_LESSON_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/lesson_math/get_all_lesson_math_data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_ALL_MATH_LESSON_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_ALL_MATH_LESSON_FAILURE });
  }
};

export const GetAllCZLesson = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_CZ_LESSON_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/lesson_cz/get_all_lesson_cz_data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
          // "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_ALL_CZ_LESSON_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_ALL_CZ_LESSON_FAILURE });
  }
};

export const GetMathLesson = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_MATH_LESSON_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/lesson_teaching_math/get_all_math_introduction_and_defination_data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_MATH_LESSON_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_MATH_LESSON_FAILURE });
  }
};

export const GetCZLesson = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_CZ_LESSON_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/lesson_teaching_cz/get_all_CZ_introduction_and_defination_data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
          // "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_CZ_LESSON_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_CZ_LESSON_FAILURE });
  }
};

export const GetAllAttemptTestView = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_LESSON_TEST_ATTEMP_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user-lesson/get-user-lesson-test-listing-data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_LESSON_TEST_ATTEMP_LIST_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_LESSON_TEST_ATTEMP_LIST_FAILURE });
  }
};

export const GetAllViewLesson = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_VIEW_LESSON_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user-lesson/get-user-lesson-listing-data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_ALL_VIEW_LESSON_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_ALL_VIEW_LESSON_FAILURE });
  }
};

export const ResetTopics = () => async (dispatch) => {
  dispatch({ type: RESET_LESSON });
};
