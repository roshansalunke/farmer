import axios from "axios";
import {
  GET_SOCIAL_ICON_FAILURE,
  GET_SOCIAL_ICON_REQUEST,
  GET_SOCIAL_ICON_SUCCESS,
} from "../constants/socialIcons";

export const GetSocialIcons = () => async (dispatch) => {
  try {
    dispatch({ type: GET_SOCIAL_ICON_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/setting/setting-social`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_SOCIAL_ICON_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_SOCIAL_ICON_FAILURE });
  }
};
