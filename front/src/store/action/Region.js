import axios from "axios";
import {
  GET_ALL_CITY_BY_ID_FAILURE,
  GET_ALL_CITY_BY_ID_REQUEST,
  GET_ALL_CITY_BY_ID_SUCCESS,
  GET_ALL_DISTRICT_BY_ID_LIST_FAILURE,
  GET_ALL_DISTRICT_BY_ID_LIST_REQUEST,
  GET_ALL_DISTRICT_BY_ID_LIST_SUCCESS,
  GET_ALL_DISTRICT_LIST_FAILURE,
  GET_ALL_DISTRICT_LIST_REQUEST,
  GET_ALL_DISTRICT_LIST_SUCCESS,
  GET_ALL_REGION_LIST_FAILURE,
  GET_ALL_REGION_LIST_REQUEST,
  GET_ALL_REGION_LIST_SUCCESS,
  GET_HIGH_SCHOOL_LIST_FAILURE,
  GET_HIGH_SCHOOL_LIST_REQUEST,
  GET_HIGH_SCHOOL_LIST_SUCCESS,
  GET_SCHOOLS_BY_CITY_ID_FAILURE,
  GET_SCHOOLS_BY_CITY_ID_REQUEST,
  GET_SCHOOLS_BY_CITY_ID_SUCCESS,
  STORE_DISTRICT_NAME_DATA,
  STORE_SCHOOL_DATA,
} from "../constants/region";

export const DistrictList = () => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_DISTRICT_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/district/get-all-district`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_ALL_DISTRICT_LIST_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_ALL_DISTRICT_LIST_FAILURE });
  }
};

export const DistrictByIdList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_DISTRICT_BY_ID_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/district/get-by-id`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (resData.data.code === "404") {
      dispatch({
        type: GET_ALL_DISTRICT_BY_ID_LIST_SUCCESS,
        payload: {},
      });
    } else {
      dispatch({
        type: GET_ALL_DISTRICT_BY_ID_LIST_SUCCESS,
        payload: resData.data,
      });
    }
  } catch (err) {
    dispatch({ type: GET_ALL_DISTRICT_BY_ID_LIST_FAILURE });
  }
};

export const CityByIdList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_CITY_BY_ID_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/city/get-by-id`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (resData.data.code === "404") {
      dispatch({
        type: GET_ALL_CITY_BY_ID_SUCCESS,
        payload: {},
      });
    } else {
      dispatch({
        type: GET_ALL_CITY_BY_ID_SUCCESS,
        payload: resData.data,
      });
    }
  } catch (err) {
    dispatch({ type: GET_ALL_CITY_BY_ID_FAILURE });
  }
};

export const RegionList = () => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_REGION_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/region/get-all-region`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_ALL_REGION_LIST_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_ALL_REGION_LIST_FAILURE });
  }
};

export const getSchoolList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_SCHOOLS_BY_CITY_ID_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/school/getSchoolNamesByiCityId`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (resData.data.code === "404") {
      dispatch({ type: GET_SCHOOLS_BY_CITY_ID_SUCCESS, payload: {} });
    } else {
      dispatch({ type: GET_SCHOOLS_BY_CITY_ID_SUCCESS, payload: resData.data });
    }
  } catch (err) {
    dispatch({ type: GET_SCHOOLS_BY_CITY_ID_FAILURE });
  }
};

export const GetHighSchoolList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_HIGH_SCHOOL_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/high-school/get-data-by-city-title`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_HIGH_SCHOOL_LIST_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_HIGH_SCHOOL_LIST_FAILURE });
  }
};

export const StoreSchoolData = (data) => async (dispatch) => {
  dispatch({ type: STORE_SCHOOL_DATA, payload: data });
};

export const StoreDistrictName = (data) => async (dispatch) => {
  dispatch({ type: STORE_DISTRICT_NAME_DATA, payload: data });
};
