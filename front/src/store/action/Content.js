import axios from "axios";
import {
  GET_APPROACH_TEXT_DATA_FAILURE,
  GET_APPROACH_TEXT_DATA_REQUEST,
  GET_APPROACH_TEXT_DATA_SUCCESS,
} from "../constants/content";

export const GetApproachText = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_APPROACH_TEXT_DATA_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/howitworks/get_all_howitworkslang_data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_APPROACH_TEXT_DATA_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_APPROACH_TEXT_DATA_FAILURE });
  }
};
