// import axios from "axios";
import { SET_SCROLLED_FOOTER } from "../constants/otherCSS";

export const SetFooterScrolled = (value) => (dispatch) => {
  dispatch({ type: SET_SCROLLED_FOOTER, payload: value });
};
