import axios from "axios";
import {
  GET_DIFFERENCE_ELEMENTARY_SCHOOL_FAILURE,
  GET_DIFFERENCE_ELEMENTARY_SCHOOL_REQUEST,
  GET_DIFFERENCE_ELEMENTARY_SCHOOL_SUCCESS,
  GET_DIFFERENCE_HIGH_SCHOOL_FAILURE,
  GET_DIFFERENCE_HIGH_SCHOOL_REQUEST,
  GET_DIFFERENCE_HIGH_SCHOOL_SUCCESS,
  GET_ELEMENTARY_AND_HIGH_SCHOOL_LIST_FAILURE,
  GET_ELEMENTARY_AND_HIGH_SCHOOL_LIST_REQEUST,
  GET_ELEMENTARY_AND_HIGH_SCHOOL_LIST_SUCCESS,
  GET_PARENT_SCHOOL_DETAILS_FAILURE,
  GET_PARENT_SCHOOL_DETAILS_REQUEST,
  GET_PARENT_SCHOOL_DETAILS_SUCCESS,
  GET_PARENT_STUDENT_CHILD_LIST_FAILURE,
  GET_PARENT_STUDENT_CHILD_LIST_REQUEST,
  GET_PARENT_STUDENT_CHILD_LIST_SUCCESS,
} from "../constants/parentSchoolDetails";

export const GetParentSchoolDetails = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_PARENT_SCHOOL_DETAILS_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/school/getFullDetailsOfSchool`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_PARENT_SCHOOL_DETAILS_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_PARENT_SCHOOL_DETAILS_FAILURE });
  }
};

export const GetElementaryAndHighSchoolList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ELEMENTARY_AND_HIGH_SCHOOL_LIST_REQEUST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user-school-setting/get-user-school-setting-data-by-id`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_ELEMENTARY_AND_HIGH_SCHOOL_LIST_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_ELEMENTARY_AND_HIGH_SCHOOL_LIST_FAILURE });
  }
};

export const GetParentChildList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_PARENT_STUDENT_CHILD_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user/parent-and-student-data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_PARENT_STUDENT_CHILD_LIST_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_PARENT_STUDENT_CHILD_LIST_FAILURE });
  }
};

export const GetElementarySchoolDifference = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_DIFFERENCE_ELEMENTARY_SCHOOL_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user-school/difference-elemantory-school`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_DIFFERENCE_ELEMENTARY_SCHOOL_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_DIFFERENCE_ELEMENTARY_SCHOOL_FAILURE });
  }
};

export const GetHighSchoolDifference = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_DIFFERENCE_HIGH_SCHOOL_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user-school/difference-high-school`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_DIFFERENCE_HIGH_SCHOOL_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_DIFFERENCE_HIGH_SCHOOL_FAILURE });
  }
};
