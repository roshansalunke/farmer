import axios from "axios";
import {
  GET_BEST_AND_WORST_STAT_FAILURE,
  GET_BEST_AND_WORST_STAT_REQUEST,
  GET_BEST_AND_WORST_STAT_SUCCESS,
  GET_DISTRICT_SCHOOL_AVG_STAT_FAILURE,
  GET_DISTRICT_SCHOOL_AVG_STAT_REQUEST,
  GET_DISTRICT_SCHOOL_AVG_STAT_SUCCESS,
  GET_EASIEST_AND_HARDEST_TOPIC_STAT_FAILURE,
  GET_EASIEST_AND_HARDEST_TOPIC_STAT_REQUEST,
  GET_EASIEST_AND_HARDEST_TOPIC_STAT_SUCCESS,
  GET_SCHOOL_AND_PERCENTAGE_STAT_FAILURE,
  GET_SCHOOL_AND_PERCENTAGE_STAT_REQUEST,
  GET_SCHOOL_AND_PERCENTAGE_STAT_SUCCESS,
  NUMBER_OF_SCHOOL_STAT_REQUEST,
  NUMBER_OF_SCHOOL_STAT_SUCCESS,
  NUMBER_OF_SCHOOL_STAT_FAILURE,
} from "../constants/schoolStatistics";

export const getBestAndWorstSchoolStat = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_BEST_AND_WORST_STAT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/district/getBestAndWorstSchool`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_BEST_AND_WORST_STAT_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_BEST_AND_WORST_STAT_FAILURE });
  }
};

export const getEasiestAndHardStat = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_EASIEST_AND_HARDEST_TOPIC_STAT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/district/getEasiestAndHardestTopic`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_EASIEST_AND_HARDEST_TOPIC_STAT_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_EASIEST_AND_HARDEST_TOPIC_STAT_FAILURE });
  }
};

export const getAllSchoolAndPercentage = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_SCHOOL_AND_PERCENTAGE_STAT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/district/getAllSchoolAndPercent`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_SCHOOL_AND_PERCENTAGE_STAT_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_SCHOOL_AND_PERCENTAGE_STAT_FAILURE });
  }
};

export const getDistrictSchoolAvgStat = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_DISTRICT_SCHOOL_AVG_STAT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/districtStats/getCountryAndDistrictStats`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_DISTRICT_SCHOOL_AVG_STAT_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_DISTRICT_SCHOOL_AVG_STAT_FAILURE });
  }
};

export const GetNumberOfSchoolStats = (values) => async (dispatch) => {
  try {
    dispatch({ type: NUMBER_OF_SCHOOL_STAT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/school/getSchoolCountGroupWise`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: NUMBER_OF_SCHOOL_STAT_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: NUMBER_OF_SCHOOL_STAT_FAILURE });
  }
};