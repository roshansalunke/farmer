import axios from "axios";
import {
  GET_LANGUAGE_LIST_FAILURE,
  GET_LANGUAGE_LIST_REQEUST,
  GET_LANGUAGE_LIST_SUCCESS,
} from "../constants/languageType";

export const GetLanguageList = () => async (dispatch) => {
  try {
    dispatch({ type: GET_LANGUAGE_LIST_REQEUST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/language/get_all_language`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_LANGUAGE_LIST_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_LANGUAGE_LIST_FAILURE });
  }
};
