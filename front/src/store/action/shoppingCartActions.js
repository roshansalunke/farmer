import axios from "axios";
import {
  PAYABLE_AMOUNT,
  CREATE_CREDIT_REQUEST,
  CREATE_CREDIT_SUCCESS,
  CREATE_CREDIT_FAILURE,
  GET_USER_SCHOOL_LIST_REQUEST,
  GET_USER_SCHOOL_LIST_SUCCESS,
  GET_USER_SCHOOL_LIST_FAILURE,
  GET_REMAINING_CREDIT_COUNT_REQUEST,
  GET_REMAINING_CREDIT_COUNT_SUCCESS,
  GET_REMAINING_CREDIT_COUNT_FAILURE,
  GET_PARENT_CHILD_LIST_REQUEST,
  GET_PARENT_CHILD_LIST_SUCCESS,
  GET_PARENT_CHILD_LIST_FAILURE,
  TOTAL_EDU_PRGS,
  TOTAL_SCHOOLS,
} from "../constants/shoppingCart";

export const SetPayableAmount = (value) => (dispatch) => {
  dispatch({ type: PAYABLE_AMOUNT, payload: value });
};

export const SetSchools = (value) => (dispatch) => {
  dispatch({ type: TOTAL_SCHOOLS, payload: value });
};

export const SetEduPrgs = (value) => (dispatch) => {
  dispatch({ type: TOTAL_EDU_PRGS, payload: value });
};

export const CreateCredit = (values) => async (dispatch) => {
  try {
    dispatch({ type: CREATE_CREDIT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/stripe/credit-create`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: CREATE_CREDIT_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: CREATE_CREDIT_FAILURE });
  }
};

export const GetRemainCreditCount = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_REMAINING_CREDIT_COUNT_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/stripe/get-remaining-credit-count-data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_REMAINING_CREDIT_COUNT_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_REMAINING_CREDIT_COUNT_FAILURE });
  }
};

export const GetUserShcoolList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_SCHOOL_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user-school/get-user-school-data-by-id`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_USER_SCHOOL_LIST_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_USER_SCHOOL_LIST_FAILURE });
  }
};

export const GetParentChildList = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_PARENT_CHILD_LIST_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user/findUserChildList`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_PARENT_CHILD_LIST_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_PARENT_CHILD_LIST_FAILURE });
  }
};
