import axios from "axios";
import {
  USER_LOGIN_FAILURE,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_SIGN_UP_FAILURE,
  USER_SIGN_UP_SUCCESS,
  USER_SING_UP_REQUEST,
  GET_USER_BY_COOKIE_REQUEST,
  GET_USER_BY_COOKIE_SUCCESS,
  GET_USER_BY_COOKIE_FAILURE,
  GET_USER_BY_TOKEN_REQUEST,
  GET_USER_BY_TOKEN_SUCCESS,
  GET_USER_BY_TOKEN_FAILURE,
} from "../constants/user";

export const UserSingUp = (values) => async (dispatch) => {
  try {
    dispatch({ type: USER_SING_UP_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user/register`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: USER_SIGN_UP_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: USER_SIGN_UP_FAILURE });
  }
};

export const UserLogin = (values) => async (dispatch) => {
  try {
    dispatch({ type: USER_LOGIN_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user/login`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: USER_LOGIN_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: USER_LOGIN_FAILURE });
  }
};

export const GetUserByCookie = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_BY_COOKIE_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user/getUserKnowledgeDataByCookieId`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_USER_BY_COOKIE_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_USER_BY_COOKIE_FAILURE });
  }
};

export const GetUserByJWTAuthToken = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_BY_TOKEN_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/user/getUserByJwtAuthCode`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_USER_BY_TOKEN_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_USER_BY_TOKEN_FAILURE });
  }
};
