import axios from "axios";
import {
  GET_ALL_SUBJECT_LESSION_QUESTION_FAILURE,
  GET_ALL_SUBJECT_LESSION_QUESTION_REQUEST,
  GET_ALL_SUBJECT_LESSION_QUESTION_SUCCESS,
  GET_TEST_QUESTIONS_FAILURE,
  GET_TEST_QUESTIONS_REQUEST,
  GET_TEST_QUESTIONS_SUCCESS,
} from "../constants/test";

export const GetTestQuestions = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_TEST_QUESTIONS_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/test_cz/getTestCZData`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({ type: GET_TEST_QUESTIONS_SUCCESS, payload: resData.data });
  } catch (err) {
    dispatch({ type: GET_TEST_QUESTIONS_FAILURE });
  }
};

export const GetAllSubjectLessionQuestions = (values) => async (dispatch) => {
  try {
    dispatch({ type: GET_ALL_SUBJECT_LESSION_QUESTION_REQUEST });
    const resData = await axios.post(
      `${process.env.REACT_APP_API_URL}/api/math-lesson-test/get-all-math-lesson-test-data`,
      values,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    dispatch({
      type: GET_ALL_SUBJECT_LESSION_QUESTION_SUCCESS,
      payload: resData.data,
    });
  } catch (err) {
    dispatch({ type: GET_ALL_SUBJECT_LESSION_QUESTION_FAILURE });
  }
};
